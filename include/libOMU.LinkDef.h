/**
 **********************************************
 *
 * \file libOMU.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__
#include "OMU/Config.h"

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ namespace OMU;

#pragma link C++ class  OMU::TConfigParser+;

#ifdef KAFKA_FOUND
#pragma link C++ class  OMU::TKafka+;
#endif

#ifdef HDF5_FOUND
#pragma link C++ class OMU::TFileH5+;
#endif

#pragma link C++ class OMU::TObjectStreamer+;
#pragma link C++ class OMU::TObjectStream+;

#pragma link C++ class OMU::TComplexPlan+;
#pragma link C++ class OMU::TPoleZeroMap+;

#pragma link C++ class      OMU::TComplexFilter+;
#pragma link C++ enum class OMU::TComplexFilter::FilterDomain+;
#pragma link C++ enum class OMU::TComplexFilter::FilterTransform+;
#pragma link C++ class      OMU::TBiquadFilter+;
#pragma link C++ class      OMU::TKalmanFilter+;

#pragma link C++ class      OMU::TKFR+;
#pragma link C++ enum class OMU::TKFR::ComplexPart+;
#pragma link C++ enum class OMU::TKFR::Transform+;
#pragma link C++ enum class OMU::TKFR::Domain+;
#pragma link C++ enum class OMU::TKFR::Filter+;
#pragma link C++ enum class OMU::TKFR::Direction+;
#pragma link C++ enum class OMU::TKFR::Output+;
#pragma link C++ enum class OMU::TKFR::Scale+;
#pragma link C++ enum class OMU::TKFR::Detrend+;
#pragma link C++ enum class OMU::TKFR::Window+;
#pragma link C++ enum class OMU::TKFR::WindowSymmetry+;

#endif
