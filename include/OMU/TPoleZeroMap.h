/**
 * *********************************************
 *
 * \file TPoleZeroMap.h
 * \brief Header of the TPoleZeroMap class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TPoleZeroMap
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 *
 * *********************************************
 */

#ifndef TPoleZeroMap_H
#define TPoleZeroMap_H

#include <Riostream.h>
#include <TMarker.h>
#include <TExec.h>

#include <UIUC/TFactory.h>
#include <OMU/MathUtils.h>
#include <TSpectrum.h>
#include <TPolyMarker.h>
#include <TArrow.h>

#include <TFitResultPtr.h>
#include <TFitResult.h>

#include <kfr/all.hpp>

#include "OMU/MathUtils.h"
#include "OMU/TComplexPlan.h"
#include "OMU/TComplexFilter.h"

namespace OMU
{
        class TPoleZeroMap: public TComplexPlan, public TComplexFilter
        {
                public:

                        using TComplexPlan::TComplexPlan;
                        TPoleZeroMap() {};
                        ~TPoleZeroMap() { };

                        TPoleZeroMap(const char *name,                          const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, 1, B, A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter,           const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, filter,         1, B, A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name,                double k, const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, k, B, A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter, double k, const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr);

                        TPoleZeroMap(const char *name,                          const OMU::TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, 1, tf.first, tf.second, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter,           const OMU::TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, filter,         1, tf.first, tf.second, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name,                double k, const OMU::TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, k, tf.first, tf.second, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter, double k, const OMU::TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, filter,         k, tf.first, tf.second, xmin, xmax, ymin, ymax, opt) { }

                        void Draw(Option_t *option = "", int level = 0);
                        void DrawNyquist(Option_t *option = "", int N = 4096);

                        TGraph *Nyquist(int N = 4096);
                        static void NyquistArrows(TString contourName, int dmin = 25);
                                
                        TPoleZeroMap *Cast(Filter type, double Ts, FilterTransform method = FilterTransform::Bilinear, double alpha = NAN);

                        using TComplexFilter::Print;
                        
                ClassDef(OMU::TPoleZeroMap, 1);
        };
}
#endif
