/**
 * *********************************************
 *
 * \file TComplexFilter.h
 * \brief Header of the TComplexFilter class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TComplexFilter
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 *
 * *********************************************
 */

#ifndef TComplexFilter_H
#define TComplexFilter_H

#include <Riostream.h>
#include <TMarker.h>
#include <TExec.h>
#include <TComplex.h>

#include <OMU/TKFR.h>

namespace OMU
{
        class TComplexFilter
        {
                public:
                        using ComplexPart     = OMU::TKFR::ComplexPart;
                        using Domain          = OMU::TKFR::Domain;
                        using Filter          = OMU::TKFR::Filter;
                        using FilterTransform = OMU::TKFR::FilterTransform;

                protected:

                        ROOT::Math::Polynomial *B = NULL;
                        ROOT::Math::Polynomial *A = NULL;
                        Filter type;

                public:

                        TComplexFilter() {};
                        ~TComplexFilter() {
                                
                                delete this->B;
                                this->B = NULL;
                                
                                delete this->A;
                                this->B = NULL;
                        };

                        TComplexFilter(                       std::vector<double> B, std::vector<double> A): TComplexFilter(Filter::Analog, 1, B, A) { }
                        TComplexFilter(Filter type,           std::vector<double> B, std::vector<double> A): TComplexFilter(type,           1, B, A) { }
                        TComplexFilter(             double k, std::vector<double> B, std::vector<double> A): TComplexFilter(Filter::Analog, k, B, A) { }
                        TComplexFilter(Filter type, double k, std::vector<double> B, std::vector<double> A);

                        TComplexFilter(                       OMU::TF tf): TComplexFilter(tf.first, tf.second) { }
                        TComplexFilter(Filter type,           OMU::TF tf): TComplexFilter(type,    tf.first, tf.second) { }
                        TComplexFilter(             double k, OMU::TF tf): TComplexFilter(      k, tf.first, tf.second) { }
                        TComplexFilter(Filter type, double k, OMU::TF tf): TComplexFilter(type, k, tf.first, tf.second) { }

                        // TComplexFilter(                       OMU::SOS sos): TComplexFilter(sos.first, sos.second) { }
                        // TComplexFilter(Filter type,           OMU::SOS sos): TComplexFilter(type,    sos.first, sos.second) { }
                        // TComplexFilter(             double k, OMU::SOS sos): TComplexFilter(      k, sos.first, sos.second) { }
                        // TComplexFilter(Filter type, double k, OMU::SOS sos): TComplexFilter(type, k, sos.first, sos.second) { }
                        
                        // TComplexFilter(                       OMU::ZPK zpk): TComplexFilter(zpk.first, zpk.second) { }
                        // TComplexFilter(Filter type,           OMU::ZPK zpk): TComplexFilter(type,    zpk.first, zpk.second) { }
                        // TComplexFilter(             double k, OMU::ZPK zpk): TComplexFilter(      k, zpk.first, zpk.second) { }
                        // TComplexFilter(Filter type, double k, OMU::ZPK zpk): TComplexFilter(type, k, zpk.first, zpk.second) { }
                        
                        inline bool IsDigital() const { return type == Filter::Digital; }
                        inline bool IsAnalog () const { return type == Filter::Analog; }

                        inline std::vector<std::complex<double>> GetPoles() { return this->A->FindRoots(); }
                        inline std::vector<std::complex<double>> GetZeros() { return this->B->FindRoots(); }
                        inline double GetGain() { return this->B->NPar() > 0 ? this->B->Parameters()[0] : 0; }
                        void SetGain(double k);

                        inline TF  GetTF() { return GetTransferFunction(); }
                        inline TF  GetTransferFunction() { return OMU::tf(*this->B, *this->A); }
                        inline SOS GetSOS() { return GetSecondOrderSection(); }
                        inline SOS GetSecondOrderSection() { return OMU::sos(*this->B, *this->A); }
                        inline ZPK GetZPK() { return GetZerosPolesGain(); }
                        inline ZPK GetZerosPolesGain() { return OMU::zpk(*this->B, *this->A); }
                        inline SS  GetSS() { return GetStateSystem(); }
                        inline SS  GetStateSystem() { return OMU::ss(*this->B, *this->A); }
        
                        TComplexFilter *Cast(Filter type, double Ts, FilterTransform method = FilterTransform::Bilinear, double alpha = NAN);
                        std::vector<std::complex<double>> Response(std::vector<std::complex<double>> X);
                        std::vector<double> Response(std::vector<double> x);
                        
                        void Print(Option_t *option = "") const;

                ClassDef(TComplexFilter, 1);
        };
}
#endif
