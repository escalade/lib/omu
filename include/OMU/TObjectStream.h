/**
 * *********************************************
 *
 * \file TObjectStream.h
 * \brief Header of the TObjectStream class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TObjectStream
 * \brief Advanced signal processing class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#ifndef TObjectStream_H
#define TObjectStream_H

#include <Riostream.h>
#include <TPSocket.h>
#include <TSSLSocket.h>
#include <TMessage.h>

#include "TPSocket.h"

#include "OMU/TObjectStreamer.h"

#include "UIUC/HandboxMsg.h"
#include "UIUC/HandboxUsage.h"

namespace OMU
{
        class TObjectStream: public TPSocket
        {
                public:

                        const int FAILURE = -1;
                        const int PASSIVE =  0;
                        const int SUCCESS =  1;

                protected:  
                        static void ErrorHandler(int level, Bool_t abort, const char *location, const char *msg);
                      
                        int Initialize(const char *);
                        TMessage *Send(const char *);

                        int connect = PASSIVE;
                        const char *token = "";

                        const static int timeout = 1000;
                        double lastUpdate = 0; // in ms
                        Long_t ticks = 0;

                        const static int maxAuthAttempts = 3;

                        bool Authenticate();

                public:

                        TObjectStream(const char *host, const char *service, const char *token = "", Int_t tcpwindowsize=-1): TPSocket(host, service, tcpwindowsize) { this->Initialize(token); }
                        TObjectStream(const char *host, Int_t port, const char *token = "", Int_t tcpwindowsize=-1): TObjectStream(host, gSystem->GetServiceByPort(port), token, tcpwindowsize) { }
                        ~TObjectStream() { };

                        static TObjectStream* Open(const char *host, const char *service, const char *token = "", Int_t tcpwindowsize=-1);
                        static TObjectStream* Open(const char *host, Int_t port, const char *token = "", Int_t tcpwindowsize=-1);

                        TObjectStream* Open();
                        void Close();
                        bool IsOpen();
                        
                        TMessage* FindObject(const char *objname, const char *cl = "");
                        TMessage* FindObject(const char *objname, TClass *cl);

                        TObject* ReadObject(const char *objname, const char *cl = "");
                        TObject* ReadObject(const char *objname, TClass *cl);
                        TObject* ReadObject(TMessage *message);

                        void Print(Option_t *opt);
                        Long_t LastUpdate();
                        Long_t Ticks();

                ClassDef(OMU::TObjectStream, 1);
        };
}
#endif
