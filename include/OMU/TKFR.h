/**
 * *********************************************
 *
 * \file TKFR.h
 * \brief Header of the TKFR class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TKFR
 * \brief Advanced signal processing class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#ifndef TKFR_H
#define TKFR_H

#include <Riostream.h>
#include <TLorentzVector.h>
#include <UIUC/HandboxMsg.h>
#include <UIUC/TFactory.h>

#include <UIUC/TFactory.h>
#include <OMU/MathUtils.h>

#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TMatrix.h>

#include <TArrow.h>
#include <TExec.h>

#include <thread>

#include <OMU/TKFRHelpers.h>

#include <OMU/MathUtils.h>
#include <Math/Interpolator.h>
#include <Math/Polynomial.h>

namespace OMU
{
       class TKFR
       {
              public:

                     constexpr static double WINDOW_DEFAULT_TUKEY_ALPHA = 0.5;
                     constexpr static double WINDOW_DEFAULT_PLANCK_TAPER_EPSILON = 0.1;
                     
                     static inline void str2enum() { std::invalid_argument("Invalid argument converting string to enum"); }
                     static inline void enum2str() { std::invalid_argument("Invalid argument converting enum to string"); }
                     
                     enum class Domain {Time, Frequency, ComplexFrequency};
                     static inline TString enum2str(Domain domain) {

                            switch(domain) {

                                   case Domain::Time:             return "time";
                                   case Domain::Frequency:        return "freq";
                                   case Domain::ComplexFrequency: return "freqc";

                                   default: enum2str();
                            }
                     }

                     enum class ComplexPart   {Real, Imag, Magnitude, Phase, Decibel, Argument};
                     static inline TString enum2str(ComplexPart type) {

                            switch(type) {

                                   case ComplexPart::Real:      return "re";
                                   case ComplexPart::Imag:      return "im";
                                   case ComplexPart::Magnitude: return "mag";
                                   case ComplexPart::Phase:     return "ph";
                                   case ComplexPart::Decibel:   return "db";
                                   case ComplexPart::Argument:  return "arg";
                                   default: enum2str();
                            }
                     }

                     enum class Filter {Analog = false, Digital = true};
                     static inline TString enum2str(Filter type) {

                            switch(type) {

                                   case Filter::Analog:  return "s";
                                   case Filter::Digital: return "z";
                                   default: enum2str();
                            }
                     }
                     
                     enum class FilterType {None, LowShelf, HighShelf, Notch, Peak, AllPass, LowPass, HighPass, BandPass, BandStop};
                     static inline TString enum2str(FilterType filter) {

                            switch(filter) {

                                   case FilterType::None:      return "";
                                   case FilterType::LowShelf:  return "lshelf";
                                   case FilterType::HighShelf: return "hshelf";
                                   case FilterType::Notch:     return "notch";
                                   case FilterType::Peak:      return "peak";
                                   case FilterType::AllPass:   return "allpass";
                                   case FilterType::LowPass:   return "lpass";
                                   case FilterType::HighPass:  return "hpass";
                                   case FilterType::BandPass:  return "bpass";
                                   case FilterType::BandStop:  return "bstop";
                                   default: enum2str();
                            }
                     }

                     enum class FilterTransform {
                            GeneralizedBilinear, GBT,
                            Bilinear, Tustin,
                            ForwardEuler, Euler, 
                            BackwardEuler, Backward, 
                            ZeroOrderHold, ZOH, 
                            FirstOrderHold, FOH,
                            ImpulseResponse, Impulse
                     };

                     enum class FilterDesign {None, Butterworth, Elliptic, ChebyshevI, ChebyshevII, Bessel};
                     static inline TString enum2str(FilterDesign design) {

                            switch(design) {

                                   case FilterDesign::None:        return "";
                                   case FilterDesign::Butterworth: return "butter";
                                   case FilterDesign::Elliptic:    return "ellip";
                                   case FilterDesign::ChebyshevI:  return "cheby1";
                                   case FilterDesign::ChebyshevII: return "cheby2";
                                   case FilterDesign::Bessel:      return "bessel";
                                   default: enum2str();
                            }
                     }

                     static FilterDesign str2filter(TString str) {

                            str = UIUC::SnakeToCamel(str);
                            if(str == "") return FilterDesign::None;

                            if(str == "butter") return FilterDesign::Butterworth;
                            if(str == "ellip" ) return FilterDesign::Elliptic;
                            if(str == "cheby1") return FilterDesign::ChebyshevI;
                            if(str == "cheby2") return FilterDesign::ChebyshevII;
                            if(str == "bessel") return FilterDesign::Bessel;

                            return FilterDesign::None;
                     }

                     enum class Method { Direct, FFT, Polyphase };
                     static inline TString enum2str(Method m) {

                            switch(m) {

                                   case Method::Direct:     return "direct";
                                   case Method::FFT:        return "fft";
                                   case Method::Polyphase:  return "polyphase";
                                   default: enum2str();
                            }
                     }

                     static Method str2method(TString str) {

                            str = UIUC::SnakeToCamel(str);
                            if(str == "fft") return Method::FFT;
                            if(str == "polyphase")  return Method::Polyphase;

                            str2enum();
                            
                            return Method::FFT; // default, cannot be reached (on purpose) due to exception in str2enum
                     }

                     enum class Trend{None, Constant, Linear, Quadratic, QuadraticConcave, QuadraticConvex, Logarithmic, Hyperbolic};
                     static inline TString enum2str(Trend detrend) {

                            switch(detrend) {

                                   case Trend::None:             return "";
                                   case Trend::Constant:         return "cst";
                                   case Trend::Linear:           return "lin";
                                   case Trend::Quadratic:        return "quad";
                                   case Trend::QuadraticConcave: return "quadv";
                                   case Trend::QuadraticConvex:  return "quadx";
                                   case Trend::Logarithmic:      return "log";
                                   case Trend::Hyperbolic:       return "hyperb";

                                   default: enum2str();
                            }
                     }

                     enum class Average{Mean, Median};
                     static inline TString enum2str(Average average) {

                            switch(average) {

                                   case Average::Mean:   return "mean";
                                   case Average::Median: return "median";

                                   default: enum2str();
                            }
                     }
                     enum class Operator: char { 
                            None,
                            Average     = 'a', Concatenate = 'c',
                            Orthonormal = 'o', Backward    = 'b', Forward = 'f'
                     };

                     static inline TString enum2str(Operator op) {

                            switch(op) {

                                   case Operator::None:        return "";
                                   case Operator::Average:     return "avg";
                                   case Operator::Concatenate: return "concat";
                                   case Operator::Backward:    return "backward";
                                   case Operator::Orthonormal: return "orthonormal";
                                   case Operator::Forward:     return "forward";
                                   default: enum2str();
                            }
                     }

                     enum class Direction {
                            Forward, ForwardBackward, 
                            Backward, BackwardForward, 
                            Both, Orthonormal
                     };
                     static inline TString enum2str(Direction direction) {

                            switch(direction) {

                                   case Direction::Forward:  return "fwd";
                                   case Direction::Backward: return "bwd";

                                   case Direction::Both: [[fallthrough]];
                                   case Direction::Orthonormal: [[fallthrough]];
                                   case Direction::BackwardForward: [[fallthrough]];
                                   case Direction::ForwardBackward: return "";

                                   default: enum2str();
                            }
                     }

                     enum class Estimator {Correlation, Periodogram, ModifiedPeriodogram, Welch, Bartlett, BlackmanTukey, LombScargle};
                     static inline TString enum2str(Estimator estimator) {

                            switch(estimator) {

                                   case Estimator::Correlation:   return "correlation";
                                   case Estimator::BlackmanTukey: return "blackmanTukey";

                                   case Estimator::Periodogram        : [[fallthrough]];
                                   case Estimator::ModifiedPeriodogram: return "periodogram";

                                   case Estimator::Bartlett   : return "bartlett";
                                   case Estimator::LombScargle: return "lombScargle";
                                   case Estimator::Welch      : return "welch";
                                   
                                   default: enum2str();
                            }
                     }

                     enum class EstimatorProperty{Density, Decibel, Spectrum, Bias, Variance, Variability, Resolution, FigureOfMerit};
                     static inline TString enum2str(EstimatorProperty property) {

                            switch(property) {

                                   case EstimatorProperty::Density      : return "";
                                   case EstimatorProperty::Spectrum     : return "spectrum";
                                   case EstimatorProperty::Decibel      : return "decibel";
                                   case EstimatorProperty::Bias         : return "bias";
                                   case EstimatorProperty::Variance     : return "variance";

                                   case EstimatorProperty::Variability  : return "variability";
                                   case EstimatorProperty::Resolution   : return "resolution";
                                   case EstimatorProperty::FigureOfMerit: return "figureOfMerit";

                                   default: enum2str();
                            }
                     }


                     enum class Transform {Standard, Fourier, Laplace, Z, Hilbert};
                     static inline TString enum2str(Transform transform) {

                            switch(transform) {

                                   case Transform::Standard: return "std";
                                   case Transform::Fourier:  return "fourier";
                                   case Transform::Laplace:  return "laplace";
                                   case Transform::Z:        return "z";
                                   case Transform::Hilbert:  return "hilbert";

                                   default: enum2str();
                            }
                     }

                     enum class WindowType {None, Zeros, ZeroPad, Unit, Bartlett, BartlettHann, Blackman, BlackmanHarris, PlanckTaper, PlanckTaperLeft, PlanckTaperRight,
                                   Bohman, Cosine, Sine, FlatTop, Tukey, Gaussian, Hamming, Hann, Kaiser, Sawtooth, Square,
                                   Lanczos, Rectangular, Rectangle, Triangular, Triangle};

                     static WindowType str2window(TString str) {

                            str = UIUC::SnakeToCamel(str);
                            if(str == "") return WindowType::Unit;

                            if(str == "zero") return WindowType::ZeroPad;
                            if(str == "square") return WindowType::Square;
                            if(str == "rectangle") return WindowType::Rectangle;

                            if(str == "sawtooth") return WindowType::Sawtooth;
                            if(str == "triangle") return WindowType::Triangle;

                            if(str == "bartlett") return WindowType::Bartlett;
                            if(str == "bartlettHann") return WindowType::BartlettHann;
                            if(str == "blackman") return WindowType::Blackman;
                            if(str == "blackmanHarris") return WindowType::BlackmanHarris;

                            if(str == "planckTaper") return WindowType::PlanckTaper;
                            if(str == "planckTaperLeft") return WindowType::PlanckTaperLeft;
                            if(str == "planckTaperRight") return WindowType::PlanckTaperRight;

                            if(str == "bohman") return WindowType::Bohman;
                            if(str == "tukey") return WindowType::Tukey;
                            if(str == "cosine") return WindowType::Cosine;
                            if(str == "sine") return WindowType::Sine;
                            if(str == "flat-top") return WindowType::FlatTop; 
                            if(str == "gaus") return WindowType::Gaussian;
                            if(str == "hamming") return WindowType::Hamming;
                            if(str == "hann") return WindowType::Hann;
                            if(str == "kaiser") return WindowType::Kaiser;
                            if(str == "lanczos") return WindowType::Lanczos;

                            return WindowType::None;
                     }

                     static inline TString enum2str(WindowType window) {

                            switch(window) {

                                   case WindowType::None: [[fallthrough]];
                                   case WindowType::Unit: return "";

                                   case WindowType::Zeros  : [[fallthrough]];
                                   case WindowType::ZeroPad: return "zero";

                                   case WindowType::Rectangular: [[fallthrough]];
                                   case WindowType::Rectangle  : return "rectangle";
                                   case WindowType::Square     : return "square";
                                   
                                   case WindowType::Sawtooth  : return "sawtooth";

                                   case WindowType::Triangular: [[fallthrough]];
                                   case WindowType::Triangle  : return "triangle";

                                   case WindowType::Bartlett      : return "bartlett";
                                   case WindowType::BartlettHann  : return "bartlettHann";

                                   case WindowType::Blackman        : return "blackman";
                                   case WindowType::BlackmanHarris  : return "blackmanHarris";
                                   case WindowType::PlanckTaper     : return "planck";
                                   case WindowType::PlanckTaperLeft : return "planck-left";
                                   case WindowType::PlanckTaperRight: return "planck-right";

                                   case WindowType::Bohman        : return "bohman";
                                   case WindowType::Tukey         : return "tukey";
                                   case WindowType::Cosine        : return "cosine";
                                   case WindowType::Sine          : return "sine";
                                   case WindowType::FlatTop       : return "flat-top";
                                   case WindowType::Gaussian      : return "gaus";
                                   case WindowType::Hamming       : return "hamming";
                                   case WindowType::Hann          : return "hann";
                                   case WindowType::Kaiser        : return "kaiser";
                                   case WindowType::Lanczos       : return "lanczos";
                                   default: enum2str();
                            }
                     }

                     using WindowSymmetry = kfr::window_symmetry;
                     static WindowSymmetry str2window_symmetry(TString str) {

                            str = UIUC::SnakeToCamel(str);
                            if(str == "symmetric") return WindowSymmetry::symmetric;
                            if(str == "periodic")  return WindowSymmetry::periodic;

                            str2enum();
                            
                            return WindowSymmetry::symmetric; // default, cannot be reached (on purpose) due to exception in str2enum
                     }

                     static inline TString enum2str(WindowSymmetry symmetry) {

                            switch(symmetry) {

                                   case WindowSymmetry::symmetric  : return "symmetric";
                                   case WindowSymmetry::periodic     : return "periodic";

                                   default: enum2str();
                            }
                     }

              protected:

                     template <typename T>
                     inline static std::vector<T> symmetric_resize(std::vector<T> v, int n)
                     {
                            int n0 = v.size();
                            if (n > n0) {
                     
                                   v.resize(n, 0);
                                   std::rotate(v.begin(), v.begin() + n0 + (n-n0)/2, v.end());

                            } else {

                                   std::rotate(v.begin(), v.end() - (n-n0)/2, v.end());
                                   v.resize(n, 0);
                            }

                            return v;
                     }

                     template <typename T>
                     inline static std::vector<std::complex<T>> symmetric_resize(std::vector<std::complex<T>> v, int n)
                     {
                            if (n > v.size()) {
                     
                                   v.resize(n);
                                   std::rotate(v.begin(), v.end() + n/2, v.end());

                            } else {

                                   std::rotate(v.begin(), v.end() - n/2, v.end());
                                   v.resize(n);
                            }

                            return v;
                     }
                     
              public:
                     static bool IsAlreadyRunningMT;
                     static std::atomic<int> finishedWorkers;

                     TKFR() {};
                     ~TKFR() { OMU::ClearFourierCache(); };

                     // https://research.iaun.ac.ir/pd/naghsh/pdfs/UploadFile_2230.pdf (Oppenheim, 1999, p173)
                     inline static std::vector<double> Interpolate(const std::vector<double> &x0, const std::vector<double> &x, const std::vector<double> &y, ROOT::Math::Interpolation::Type type = ROOT::Math::Interpolation::kLINEAR) { return Interpolate(x0, ROOT::Math::Interpolator(x, y, type)); }  
                            static std::vector<double> Interpolate(                        int N,                               const std::vector<double> &y, ROOT::Math::Interpolation::Type type = ROOT::Math::Interpolation::kLINEAR);
                            static std::vector<double> Interpolate(const std::vector<double> &x0, const ROOT::Math::Interpolator &itp);
                            static TH1D *Interpolate(const std::vector<double> &x0, TH1D *h, ROOT::Math::Interpolation::Type type = ROOT::Math::Interpolation::kLINEAR);

                     inline static std::vector<Complex> Adjust(const std::vector<double>  &x, double amplitude, double phase, double offset, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::Frequency,        Option_t *option = "x") { return Adjust(OMU::Complexify(x), amplitude, phase, offset, fs, domain, option); }
                     static std::vector<Complex>        Adjust(const std::vector<Complex> &x, double amplitude, double phase, double offset, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "x");
                     static std::vector<Complex>        Adjust(                      TH1 *h1, double amplitude, double phase, double offset,                OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "x");
                     static TH1*                        Adjust(ComplexPart part,     TH1 *h1, double amplitude, double phase, double offset,                OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "x");

                     static std::vector<double> Get(ComplexPart part, const std::vector<Complex> &Xn);
                     static std::vector<std::vector<double>> Get(ComplexPart part, const std::vector<std::vector<Complex>> &Xn);

                     //
                     // Handling FFT for std::vectors
                     inline static std::vector<double>   FFT(ComplexPart part, const std::vector<double> &x, OMU::TKFR::Domain domain = OMU::TKFR::Domain::Frequency,        Option_t *option = "", int nFFT = 0) { return Get(part, FFT(OMU::Complexify(x), domain, option, nFFT)); }
                     inline static std::vector<Complex>  FFT(const std::vector<double>  &x, OMU::TKFR::Domain domain = OMU::TKFR::Domain::Frequency,        Option_t *option = "", int nFFT = 0) { return FFT(OMU::Complexify(x), domain, option, nFFT); }
                            static std::vector<Complex>  FFT(std::vector<Complex> x, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0);
                            static TH1*                  FFT(ComplexPart part, TH1 *h, Option_t *option = "x", int nFFT = 0);
                            static std::vector<Complex>  FFT(TH1 *h, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "x", int nFFT = 0);

                     inline static std::vector<double>  iFFT(const std::vector<double> &X, Option_t *option = "", int nFFT = 0) { return OMU::Real(iFFT(OMU::Complexify(X), OMU::TKFR::Domain::Frequency, option, nFFT)); }
                     inline static std::vector<double>  iFFT(ComplexPart part, const std::vector<Complex> &X, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0)  { return Get(part, iFFT(X, domain, option, nFFT)); }
                            static std::vector<Complex> iFFT(std::vector<Complex> X, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0);

                     // To be implemented in PSD calculation
                     inline static std::vector<std::vector<double>>  STFT(ComplexPart part, const std::vector<double>  &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::Frequency, Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Get(part, STFT(OMU::Complexify(x), fs, domain, option, nFFT, noverlap, window, ab, symmetry)); }
                     inline static std::vector<std::vector<Complex>> STFT(                  const std::vector<double>  &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::Frequency, Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return STFT(OMU::Complexify(x), fs, domain, option, nFFT, noverlap, window, ab, symmetry); }
                            static std::vector<std::vector<Complex>> STFT(                  const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static TH2*                              STFT(ComplexPart part, TH2 *h,                                   OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static std::vector<std::vector<Complex>> STFT(                  TH2 *h,                                   OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);

                     inline static std::vector<double>  iSTFT(ComplexPart part, const std::vector<std::vector<Complex>> &X, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric)  { return Get(part, iSTFT(X, fs, domain, option, nFFT, noverlap, window, ab, symmetry)); }
                     inline static std::vector<double>  iSTFT(                  const std::vector<std::vector<double>>  &X, double fs,                                                                 Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return OMU::Real(iSTFT(OMU::Complexify(X), fs, OMU::TKFR::Domain::Frequency, option, nFFT, noverlap, window, ab, symmetry)); }
                            static std::vector<Complex> iSTFT(                  const std::vector<std::vector<Complex>> &X, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0, double noverlap = 0.5, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);

                     bool static NOLA(double nFFT, double noverlap, WindowType window, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric, double tolerance = 1e-10);
                     bool static COLA(double nFFT, double noverlap, WindowType window, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric, double tolerance = 1e-10);

                     //
                     // Handling signal injection
                     inline static std::vector<Complex> Inject(const std::vector<double>  &x, const std::vector<double>  &y,                                                                 Option_t *option = "",  int nFFT = 0) { return Inject(OMU::Complexify(x), OMU::Complexify(y), OMU::TKFR::Domain::Frequency, option, nFFT); }
                            static std::vector<Complex> Inject(const std::vector<Complex> &x, const std::vector<Complex> &y, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0);
                            static TH1*                 Inject(TH1 *h1,                       TH1 *h2,                                                                                       Option_t *option = "x", int nFFT = 0);

                     // Auto correlation
                     inline static std::vector<double>  AutocorrelateFFT(ComplexPart part, const std::vector<double>  &x,                                                                 Option_t *option = "",  int nFFT = 0) { return Get(part, CorrelateFFT(                   x, x,         option, nFFT)); }
                     inline static std::vector<Complex> AutocorrelateFFT(                  const std::vector<double>  &x,                                                                 Option_t *option = "",  int nFFT = 0) { return           CorrelateFFT(                   x, x,         option, nFFT); }
                     inline static std::vector<double>  AutocorrelateFFT(ComplexPart part, const std::vector<Complex> &x, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0) { return Get(part, CorrelateFFT(                   x, x, domain, option, nFFT)); }
                     inline static std::vector<Complex> AutocorrelateFFT(                  const std::vector<Complex> &x, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0) { return           CorrelateFFT(                   x, x, domain, option, nFFT); }
                     inline static TH1*                 AutocorrelateFFT(                  TH1 *h,                                                                                        Option_t *option = "x", int nFFT = 0) { return           CorrelateFFT(ComplexPart::Real, h, h,         option, nFFT); }
                     inline static TH1*                 AutocorrelateFFT(ComplexPart part, TH1 *h,                                                                                        Option_t *option = "x", int nFFT = 0) { return           CorrelateFFT(part,              h, h,         option, nFFT); }

                     // Cross correlation
                     inline static std::vector<double>  CorrelateFFT(ComplexPart part, const std::vector<double>  &x, const std::vector<double>  &y,                                                                 Option_t *option = "",  int nFFT = 0) { return Get(part, CorrelateFFT(OMU::Complexify(x), OMU::Complexify(y), OMU::TKFR::Domain::Frequency, option, nFFT)); }
                     inline static std::vector<Complex> CorrelateFFT(                  const std::vector<double>  &x, const std::vector<double>  &y,                                                                 Option_t *option = "",  int nFFT = 0) { return           CorrelateFFT(OMU::Complexify(x), OMU::Complexify(y), OMU::TKFR::Domain::Frequency, option, nFFT); }
                     inline static std::vector<double>  CorrelateFFT(ComplexPart part,       std::vector<Complex>  x,       std::vector<Complex>  y, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0) { return Get(part, CorrelateFFT(                x ,                 y , domain           , option, nFFT)); }
                            static std::vector<Complex> CorrelateFFT(                        std::vector<Complex>  x,       std::vector<Complex>  y, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0);
                     inline static TH1*                 CorrelateFFT(                  TH1 *h1,                       TH1 *h2,                                                                                       Option_t *option = "x", int nFFT = 0) { return           CorrelateFFT(ComplexPart::Real, h1, h2,         option, nFFT); }
                            static TH1*                 CorrelateFFT(ComplexPart part, TH1 *h1,                       TH1 *h2,                                                                                       Option_t *option = "x", int nFFT = 0);

                     // Convolution
                     inline static std::vector<double>  ConvolveFFT(ComplexPart part, const std::vector<double> &x, const std::vector<double>  &y,                                                                 Option_t *option = "",  int nFFT = 0) { return Get(part, ConvolveFFT(OMU::Complexify(x), OMU::Complexify(y), OMU::TKFR::Domain::Frequency, option, nFFT)); }
                     inline static std::vector<Complex> ConvolveFFT(                  const std::vector<double> &x, const std::vector<double>  &y,                                                                 Option_t *option = "",  int nFFT = 0) { return           ConvolveFFT(OMU::Complexify(x), OMU::Complexify(y), OMU::TKFR::Domain::Frequency, option, nFFT); }
                     inline static std::vector<double>  ConvolveFFT(ComplexPart part, std::vector<Complex>       x,       std::vector<Complex>  y, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0) { return Get(part, ConvolveFFT(                x ,                 y , domain           , option, nFFT)); }
                            static std::vector<Complex> ConvolveFFT(                  std::vector<Complex>       x,       std::vector<Complex>  y, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0);
                     inline static TH1*                 ConvolveFFT(                  TH1 *h1,                            TH1 *h2,                                                                                 Option_t *option = "x", int nFFT = 0) { return           ConvolveFFT(ComplexPart::Real, h1, h2,         option, nFFT); }
                            static TH1*                 ConvolveFFT(ComplexPart part, TH1 *h1,                            TH1 *h2,                                                                                 Option_t *option = "x", int nFFT = 0);

                     // Spectral densities
                     inline static std::vector<double> ASD(EstimatorProperty property, const std::vector<double>  &x, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           ASD(property,                   OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<double> ASD(                            const std::vector<double>  &x, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           ASD(EstimatorProperty::Density, OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<double> ASD(EstimatorProperty property, const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return OMU::Sqrt(CSD(property,                   x, x,               fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
                     inline static std::vector<double> ASD(                            const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return OMU::Sqrt(CSD(EstimatorProperty::Density, x, x,               fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
                     inline static TH1*                ASD(                            TH1 *h,                                                                                                   Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           ASD(EstimatorProperty::Density, h,                                         option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static TH1*                ASD(EstimatorProperty property, TH1 *h,                                                                                                   Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     
                     inline static std::vector<double> PSD(EstimatorProperty property, const std::vector<double>  &x, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           PSD(property,                   OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<double> PSD(                            const std::vector<double>  &x, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           PSD(EstimatorProperty::Density, OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<double> PSD(EstimatorProperty property, const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           CSD(property,                   x, x,               fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<double> PSD(                            const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           CSD(EstimatorProperty::Density, x, x,               fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static TH1*                PSD(                            TH1 *h,                                                                                                   Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return           PSD(EstimatorProperty::Density, h,                                         option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static TH1*                PSD(EstimatorProperty property, TH1 *h,                                                                                                   Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     
                     inline static std::vector<double> CSD(EstimatorProperty property, const std::vector<double>  &x, const std::vector<double>  &y, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return CSD(property,                   OMU::Complexify(x), OMU::Complexify(y), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<double> CSD(ComplexPart part,           const std::vector<double>  &x, const std::vector<double>  &y, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return CSD(part,                       OMU::Complexify(x), OMU::Complexify(y), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<double> CSD(                            const std::vector<double>  &x, const std::vector<double>  &y, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return CSD(EstimatorProperty::Density, OMU::Complexify(x), OMU::Complexify(y), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static std::vector<double> CSD(EstimatorProperty property, const std::vector<Complex> &x, const std::vector<Complex> &y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static std::vector<double> CSD(ComplexPart part,           const std::vector<Complex> &x, const std::vector<Complex> &y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     inline static std::vector<double> CSD(                            const std::vector<Complex> &x, const std::vector<Complex> &y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return CSD(EstimatorProperty::Density, x,                  y,                  fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static TH1*                CSD(                            TH1 *h1,                            TH1 *h2,                                                                                             Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return CSD(EstimatorProperty::Density, h1,                 h2,                                        option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static TH1*                CSD(EstimatorProperty property, TH1 *h1,                            TH1 *h2,                                                                                             Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static TH1*                CSD(ComplexPart part,               TH1 *h1,                            TH1 *h2,                                                                                         Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);

                     inline static std::vector<std::vector<double>> Spectrogram(EstimatorProperty property, const std::vector<double>  &x, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(property,                   OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(ComplexPart part,           const std::vector<double>  &x, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(part,                       OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(                            const std::vector<double>  &x, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(EstimatorProperty::Decibel, OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(EstimatorProperty property, const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(property,                   x, x,               fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(ComplexPart part,           const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(part,                       x, x,               fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(                            const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(EstimatorProperty::Decibel, x, x,               fs, domain,            option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static TH2*                             Spectrogram(                            TH1 *h,                                                                                                   Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(EstimatorProperty::Decibel, h,                                         option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static TH2*                             Spectrogram(EstimatorProperty property, TH1 *h,                                                                                                   Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static TH2*                             Spectrogram(ComplexPart part,           TH1 *h,                                                                                                   Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     
                     inline static std::vector<std::vector<double>> Spectrogram(EstimatorProperty property, const std::vector<double>  &x, const std::vector<double>  &y, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(property,                   OMU::Complexify(x), OMU::Complexify(y), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(ComplexPart part,           const std::vector<double>  &x, const std::vector<double>  &y, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(part,                       OMU::Complexify(x), OMU::Complexify(y), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(                            const std::vector<double>  &x, const std::vector<double>  &y, double fs,                                                                 Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(                            OMU::Complexify(x), OMU::Complexify(y), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     inline static std::vector<std::vector<double>> Spectrogram(                            const std::vector<Complex> &x, const std::vector<Complex> &y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(EstimatorProperty::Decibel, x                 , y                 , fs, domain           , option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static std::vector<std::vector<double>> Spectrogram(EstimatorProperty property, const std::vector<Complex> &x, const std::vector<Complex> &y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static std::vector<std::vector<double>> Spectrogram(ComplexPart part,           const std::vector<Complex> &x, const std::vector<Complex> &y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     inline static TH2*                             Spectrogram(                            TH1 *h1,                            TH1 *h2,                                                                                             Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Spectrogram(EstimatorProperty::Decibel, h1,                 h2,                                        option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static TH2*                             Spectrogram(EstimatorProperty property, TH1 *h1,                            TH1 *h2,                                                                                             Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static TH2*                             Spectrogram(ComplexPart part,           TH1 *h1,                            TH1 *h2,                                                                                             Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);

                     // Coherence (between CSD/PSD)
                     inline static std::vector<double> Coherence(const std::vector<double>  &x, const std::vector<double>  &y, double fs,                                           Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Coherence(OMU::Complexify(x), OMU::Complexify(y), fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                            static std::vector<double> Coherence(const std::vector<Complex> &x, const std::vector<Complex> &y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static TH1*                Coherence(TH1 *h1,                            TH1 *h2,                                                                                 Option_t *option = "x", int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
              
                     inline static std::vector<double>  Impulse(double fs, double duration, const double amplitude,                const double freq               ) { return OMU::Real(Impulse(fs, duration, std::vector<double>({amplitude}), std::vector<double>({freq}), OMU::TKFR::Domain::Time)); };
                     inline static std::vector<Complex> Impulse(double fs, double duration, const double amplitude,                const double freq, OMU::TKFR::Domain domain) { return Impulse(fs,duration, std::vector<double>({amplitude}), std::vector<double>({freq}), domain); };
                     inline static std::vector<double>  Impulse(double fs, double duration, const std::vector<double> &amplitudes, const std::vector<double> &freqs) { return OMU::Real(Impulse(fs, duration, amplitudes, freqs, OMU::TKFR::Domain::Time)); }
                            static std::vector<Complex> Impulse(double fs, double duration, const std::vector<double> &amplitudes, const std::vector<double> &freqs, OMU::TKFR::Domain domain);

                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs,                  OMU::TKFR::Domain domain, Complex dc, double    formulaRe,    double    formulaIm = 0                                                                                     ) { return Get(part, Spectrum(fs,          domain, dc, "[0]",        "[1]",       std::vector<double>({formulaRe, formulaIm}))); }
                     inline static std::vector<Complex> Spectrum(                  double fs,                  OMU::TKFR::Domain domain, Complex dc, double    formulaRe,    double    formulaIm = 0                                                                                     ) { return           Spectrum(fs,          domain, dc, "[0]",        "[1]",       std::vector<double>({formulaRe, formulaIm})); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs, double duration, OMU::TKFR::Domain domain, Complex dc, double    formulaRe,    double    formulaIm = 0                                                                                     ) { return Get(part, Spectrum(fs*duration, domain, dc, formulaRe, formulaIm)); }
                     inline static std::vector<Complex> Spectrum(                  double fs, double duration, OMU::TKFR::Domain domain, Complex dc, double    formulaRe,    double    formulaIm = 0                                                                                     ) { return           Spectrum(fs*duration, domain, dc, formulaRe, formulaIm); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs,                  OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params = {},                                  int iterations = 1) { return Get(part, Spectrum(fs,          domain, dc, formulaReStr, formulaImStr, params, std::vector<double>({}), iterations)); }
                     inline static std::vector<Complex> Spectrum(                  double fs,                  OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params = {},                                  int iterations = 1) { return           Spectrum(fs,          domain, dc, formulaReStr, formulaImStr, params, std::vector<double>({}), iterations); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params = {},                                  int iterations = 1) { return Get(part, Spectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, iterations)); }
                     inline static std::vector<Complex> Spectrum(                  double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params = {},                                  int iterations = 1) { return           Spectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, iterations); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1) { return Get(part, Spectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, paramSigmas, iterations)); }
                     inline static std::vector<Complex> Spectrum(                  double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1) { return           Spectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, paramSigmas, iterations); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs,                  OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1) { return Get(part, Spectrum(fs, domain, dc, formulaReStr, formulaImStr, params, paramSigmas, iterations)); }
                            static std::vector<Complex> Spectrum(                  double fs,                  OMU::TKFR::Domain domain, Complex dc, TString   formulaReStr, TString   formulaImStr, std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1);
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs,                  OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params = {},                                  int iterations = 1) { return Get(part, Spectrum(fs,          domain, dc, formulaRe, formulaIm, params, std::vector<double>({}), iterations)); }
                     inline static std::vector<Complex> Spectrum(                  double fs,                  OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params = {},                                  int iterations = 1) { return           Spectrum(fs,          domain, dc, formulaRe, formulaIm, params, std::vector<double>({}), iterations); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params = {},                                  int iterations = 1) { return Get(part, Spectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, iterations)); }
                     inline static std::vector<Complex> Spectrum(                  double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params = {},                                  int iterations = 1) { return           Spectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, iterations); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1) { return Get(part, Spectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations)); }
                     inline static std::vector<Complex> Spectrum(                  double fs, double duration, OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1) { return           Spectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations); }
                     inline static std::vector<double>  Spectrum(ComplexPart part, double fs,                  OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1) { return Get(part, Spectrum(fs, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations)); }
                            static std::vector<Complex> Spectrum(                  double fs,                  OMU::TKFR::Domain domain, Complex dc, TFormula *formulaRe,    TFormula *formulaIm,    std::vector<double> params,      std::vector<double> paramSigmas, int iterations = 1);

                     static std::vector<Complex> SpectrumCut(const std::vector<Complex> &input, double fs, double fLow = NAN, double fHigh = NAN);

                     inline static std::vector<double> Waveform(double fs, double duration, double  formulaStr                                                                                 ) { return Waveform(fs, duration,      "[0]", std::vector<double>({formulaStr})          ); }
                     inline static std::vector<double> Waveform(double fs, double duration, TString formulaStr                                                                                 ) { return Waveform(fs, duration, formulaStr, std::vector<double>({})                    ); }
                     inline static std::vector<double> Waveform(double fs, double duration, TString formulaStr, std::vector<double> params,                                  int iterations = 1) { return Waveform(fs, duration, formulaStr, params, std::vector<double>({}), iterations); }
                            static std::vector<double> Waveform(double fs, double duration, TString formulaStr, std::vector<double> params, std::vector<double> paramSigmas, int iterations = 1);
                     inline static std::vector<double> Waveform(double fs, double duration, TFormula *formula                                                                                  ) { return Waveform(fs, duration, formula   , std::vector<double>({})                    ); }
                     inline static std::vector<double> Waveform(double fs, double duration, TFormula *formula , std::vector<double> params,                                  int iterations = 1) { return Waveform(fs, duration, formula   , params, std::vector<double>({}), iterations); }
                            static std::vector<double> Waveform(double fs, double duration, TFormula *formula , std::vector<double> params, std::vector<double> paramSigmas, int iterations = 1);
                     inline static std::vector<double> Waveform(double fs, double duration,                   WindowType window, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Waveform(fs, duration, 1.0, window, ab, symmetry); }
                            static std::vector<double> Waveform(double fs, double duration, double dutyCycle, WindowType window, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     
                     inline static std::vector<double> Window(size_t N,                   WindowType window, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Window(N, 1.0, window, ab, symmetry); }
                            static std::vector<double> Window(size_t N, double dutyCycle, WindowType window, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     
                     inline static std::vector<double>  Windowing(const std::vector<double>  &x,                   WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Windowing(x, 1.0,         window, ab, symmetry); }
                     inline static std::vector<double>  Windowing(const std::vector<double>  &x, double dutyCycle, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return OMU::Multiply(x, Window(x.size(), dutyCycle, window, ab, symmetry)); }
                     inline static std::vector<Complex> Windowing(const std::vector<Complex> &x,                   WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Windowing(x, 1.0,         window, ab, symmetry); }
                     inline static std::vector<Complex> Windowing(const std::vector<Complex> &x, double dutyCycle, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return OMU::Multiply(x, OMU::Complexify(Window(x.size(), dutyCycle, window, ab, symmetry))); }
                     inline static TH1*                 Windowing(TH1 *h,                                          WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Windowing(h, "x"   , 1.0      , window, ab, symmetry); }
                     inline static TH1*                 Windowing(TH1 *h, Option_t *option,                        WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Windowing(h, option, 1.0      , window, ab, symmetry); }
                     inline static TH1*                 Windowing(TH1 *h,                        double dutyCycle, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Windowing(h, "x"   , dutyCycle, window, ab, symmetry); }
                            static TH1*                 Windowing(TH1 *h, Option_t *option,      double dutyCycle, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     
                     static std::vector<double>  Detrend(const std::vector<double> &x,  Trend detrend = Trend::None);
                     static std::vector<Complex> Detrend(const std::vector<Complex> &x, Trend detrend = Trend::None);
                     static TH1*                 Detrend(TH1 *h,                        Trend detrend = Trend::None, Option_t *option = "");                

                     // compute random variable based on inverse transform method.
                     static std::vector<double> Noise(int N, TF1 *pdf);

                     static std::vector<double> Noise(                                 int N,                  double amplitude = 1, double mean = 0, double sigma = 1, double alpha = 1, double mean2 = 0, double sigma2 = 1);
                     static TH1*                Noise(TString name, TString title, double fs, double duration, double amplitude = 1, double mean = 0, double sigma = 1, double alpha = 1, double mean2 = 0, double sigma2 = 1);                   

                     //  Finite Impulse Response
                     inline static std::vector<Complex> Filtering(const std::vector<Complex> &x, FilterType filter, const std::vector<double> &cutoff, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return OMU::Complexify(Filtering(OMU::Real(x), filter, cutoff, window, ab, symmetry), Filtering(OMU::Imag(x), filter, cutoff, window, ab, symmetry)); }
                            static std::vector<double>  Filtering(const std::vector<double>  &x, FilterType filter, const std::vector<double> &cutoff, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     inline static TH1*                 Filtering(TH1 *h,                        FilterType filter, const std::vector<double> &cutoff, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return Filtering(h, "x", filter, cutoff, window, ab, symmetry); }
                            static TH1*                 Filtering(TH1 *h,      Option_t *option, FilterType filter, const std::vector<double> &cutoff, WindowType window = WindowType::None, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);

                     // Infinite Impulse Response
                     inline static std::vector<Complex> Filtering(const std::vector<Complex> &x,                       ZPK  zpk, Direction direction = Direction::Both) { return Filtering(x, kfr::to_sos(zpk), direction); }
                     inline static std::vector<double>  Filtering(const std::vector<double>  &x,                       ZPK  zpk, Direction direction = Direction::Both) { return Filtering(x, kfr::to_sos(zpk), direction); }
                     inline static TH1*                 Filtering(TH1 *h,                   TString alias,             ZPK  zpk, Direction direction = Direction::Both) { return Filtering(h,         alias, kfr::to_sos(zpk), direction); }
                     inline static TH1*                 Filtering(TH1 *h, Option_t *option, TString alias,             ZPK  zpk, Direction direction = Direction::Both) { return Filtering(h, option, alias, kfr::to_sos(zpk), direction); }
                     
                     inline static std::vector<Complex> Filtering(const std::vector<Complex> &x,                       SOS  sos, Direction direction = Direction::Both) { return OMU::Complexify(Filtering(OMU::Real(x), std::vector<SOS>({sos}), direction), Filtering(OMU::Imag(x), std::vector<SOS>({sos}), direction)); }
                            static std::vector<Complex> Filtering(const std::vector<Complex> &x,           std::vector<SOS> sos, Direction direction = Direction::Both) { return OMU::Complexify(Filtering(OMU::Real(x),                        sos,   direction), Filtering(OMU::Imag(x),                        sos,   direction)); }
                     inline static std::vector<double>  Filtering(const std::vector<double>  &x,                       SOS  sos, Direction direction = Direction::Both) { return Filtering(x, std::vector<SOS>({sos}), direction); }
                            static std::vector<double>  Filtering(const std::vector<double>  &x,           std::vector<SOS> sos, Direction direction = Direction::Both);
                     inline static TH1*                 Filtering(TH1 *h,                   TString alias,             SOS  sos, Direction direction = Direction::Both) { return Filtering(h,    "x", alias, std::vector<SOS>({sos}), direction); }
                     inline static TH1*                 Filtering(TH1 *h, Option_t *option, TString alias,             SOS  sos, Direction direction = Direction::Both) { return Filtering(h, option, alias, std::vector<SOS>({sos}), direction); }
                     inline static TH1*                 Filtering(TH1 *h,                   TString alias, std::vector<SOS> sos, Direction direction = Direction::Both) { return Filtering(h,    "x", alias,  sos , direction); }
                            static TH1*                 Filtering(TH1 *h, Option_t *option, TString alias, std::vector<SOS> sos, Direction direction = Direction::Both);

                     // Wiener filter
                     inline static std::vector<double>  WienerFiltering(const std::vector<double>  &x, double fs, const std::vector<double> &PSD = std::vector<double>{}, Option_t *option = "", int nFFT = 0) { return OMU::Real(WienerFiltering(OMU::Complexify(x), fs, PSD, OMU::TKFR::Domain::Frequency, option, nFFT)); }
                            static std::vector<Complex> WienerFiltering(const std::vector<Complex> &x, double fs, std::vector<double> PSD = std::vector<double>{}, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0);
                            static TH1*                 WienerFiltering(TH1 *h, TH1 *hNoisePSD, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0);

                     // Matched filter
                     inline static std::vector<double>  MatchedFiltering(const std::vector<double>  &x, double fs, const std::vector<double> &PSD = std::vector<double>{}, Option_t *option = "", int nFFT = 0) { return OMU::Real(MatchedFiltering(OMU::Complexify(x), fs, PSD, OMU::TKFR::Domain::Frequency, option, nFFT)); }
                            static std::vector<Complex> MatchedFiltering(const std::vector<Complex> &x, double fs, std::vector<double> PSD = std::vector<double>{}, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0);
                            static std::vector<Complex> MatchedFiltering(TH1 *h, TH1 *hNoisePSD, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0);

                     static std::vector<double> Axis(const double duration, const double fs) { return OMU::Range(duration*fs+1, 1./fs); }

                     inline static std::vector<double>  FrequencyResponse(                             ComplexPart part, double fs, const             SOS  &sos, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency) { return                      FrequencyResponse(part, fs, std::vector<SOS>({sos}), domain);  }
                     inline static std::vector<double>  FrequencyResponse(                             ComplexPart part, double fs, const std::vector<SOS> &sos, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency) { return OMU::TKFR::Get(part, FrequencyResponse(      fs, std::vector<SOS>({sos}), domain)); }
                     inline static std::vector<Complex> FrequencyResponse(                                               double fs, const             SOS  &sos, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency) { return                      FrequencyResponse(      fs, std::vector<SOS>({sos}), domain);  }
                            static std::vector<Complex> FrequencyResponse(                                               double fs, const std::vector<SOS> &sos, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency);
                     inline static TH1*                 FrequencyResponse(TString name, TString title, ComplexPart part, double fs, const             SOS  &sos, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency) { return FrequencyResponse(name, title, part ,fs, sos, domain); }
                            static TH1*                 FrequencyResponse(TString name, TString title, ComplexPart part, double fs, const std::vector<SOS> &sos, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency);

                     inline static std::vector<double>  MagnitudeResponse(                             double fs, const             SOS  &sos) { return MagnitudeResponse(fs, std::vector<SOS>({sos})); }
                     inline static std::vector<double>  MagnitudeResponse(                             double fs, const std::vector<SOS> &sos) { return FrequencyResponse(ComplexPart::Decibel, fs, std::vector<SOS>({sos}), OMU::TKFR::Domain::Frequency); }
                     inline static TH1*                 MagnitudeResponse(TString name, TString title, double fs, const             SOS  &sos) { return MagnitudeResponse(name, title, fs, sos); }
                            static TH1*                 MagnitudeResponse(TString name, TString title, double fs, const std::vector<SOS> &sos);

                     inline static std::vector<double>  PhaseResponse(                             double fs, const             SOS  &sos) { return PhaseResponse(fs, std::vector<SOS>({sos})); }
                     inline static std::vector<double>  PhaseResponse(                             double fs, const std::vector<SOS> &sos) { return FrequencyResponse(ComplexPart::Phase, fs, std::vector<SOS>({sos}), OMU::TKFR::Domain::Frequency); }
                     inline static TH1*                 PhaseResponse(TString name, TString title, double fs, const             SOS  &sos) { return PhaseResponse(name, title, fs, sos); }
                            static TH1*                 PhaseResponse(TString name, TString title, double fs, const std::vector<SOS> &sos);

                     inline static TCanvas* BodePlot   (TString name, TString title, Option_t *opt, double fs,             SOS  sos) { return BodePlot(name, title, opt, fs, std::vector<SOS>({sos})); }
                            static TCanvas* BodePlot   (TString name, TString title, Option_t *opt, double fs, std::vector<SOS> sos);

                     inline static std::vector<double>  Hilbert(ComplexPart part, const std::vector<double>  &x, Option_t *option = "",  int nFFT = 0) { return Get(part, Hilbert(OMU::Complexify(x), option, nFFT)); }
                     inline static std::vector<double>  Hilbert(ComplexPart part, const std::vector<Complex> &x, Option_t *option = "",  int nFFT = 0) { return Get(part, Hilbert(x , option, nFFT)); }
                     inline static std::vector<Complex> Hilbert(                  const std::vector<double>  &x, Option_t *option = "",  int nFFT = 0) { return Hilbert(OMU::Complexify(x), option, nFFT); }
                            static std::vector<Complex> Hilbert(                        std::vector<Complex>  x, Option_t *option = "",  int nFFT = 0);
                     inline static TH1*                 Hilbert(                  TH1 *h1,                       Option_t *option = "x", int nFFT = 0) { return Hilbert(ComplexPart::Magnitude, h1, option, nFFT); }
                            static TH1*                 Hilbert(ComplexPart part, TH1 *h1,                       Option_t *option = "x", int nFFT = 0);

                     inline static std::vector<double>  Envelope(const std::vector<double>  &x, Option_t *option = "" , int nFFT = 0) { return Hilbert(ComplexPart::Magnitude, x, option, nFFT); }
                     inline static std::vector<double>  Envelope(const std::vector<Complex> &x, Option_t *option = "" , int nFFT = 0) { return Hilbert(ComplexPart::Magnitude, x, option, nFFT); }
                            static TH1*                 Envelope(TH1 *h,                       Option_t *option = "x", int nFFT = 0);

                     static std::vector<double> Gaussian(double fs, const double duration, double mean = 0, double sigma = 1);

                     // NB: https://colab.research.google.com/github/kastnerkyle/kastnerkyle.github.io/blob/master/posts/polyphase-signal-processing/polyphase-signal-processing.ipynb#scrollTo=JbT-73-Z-q83
                     static std::vector<double>           Chirp(double fs, const double duration, double t0, double t1, double f0, double f1, Trend method, double taper = NAN, const double phase = 0);
                     static std::vector<double> SweepPolynomial(double fs, const double duration, double t0, double t1, const std::vector<double> coeff,    double taper = NAN,       double phase = 0);                                  
                     static std::vector<double>    SweepFormula(double fs, const double duration, TF1 *formula,  const std::vector<double> params,          double taper = NAN,       double phase = 0);

                     inline static std::vector<double>  Taper(std::vector<double>  x, double fs, double T0, double T1, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric) { return OMU::Real(Taper(OMU::Complexify(x), fs, T0, T1, ab, symmetry)); }
                            static std::vector<Complex> Taper(std::vector<Complex> x, double fs, double T0, double T1, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                            static TH1*                 Taper(TH1 *h, double T0, double T1, Option_t *opt = "x", double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);

                     // 1D Autoregressif models
                     inline static std::vector<double> AR(double fs, const double duration, double c, std::vector<double> p, std::vector<double> AR0, double sigma = 1     ) { return AR(fs*duration, c, p, AR0, Gaussian(fs*duration, 0, sigma)); }
                     inline static std::vector<double> AR(double fs,                        double c, std::vector<double> p, std::vector<double> AR0, double sigma = 1     ) { return AR(fs,          c, p, AR0, Gaussian(fs,       1, 0, sigma)); }
                     inline static std::vector<double> AR(double fs, const double duration, double c, std::vector<double> p, std::vector<double> AR0, std::vector<double> w) { return AR(fs*duration, c, p, AR0, w); }
                            static std::vector<double> AR(double fs,                        double c, std::vector<double> p, std::vector<double> AR0, std::vector<double> w);
                     
                     inline static std::vector<double> MA(double fs, const double duration, std::vector<double> q, double sigma = 1) { return MA(fs*duration, q, Gaussian(fs, duration, 0, sigma)); }
                     inline static std::vector<double> MA(double fs,                        std::vector<double> q, double sigma = 1) { return MA(fs,          q, Gaussian(fs,        1, 0, sigma)); }
                     inline static std::vector<double> MA(double fs, const double duration, std::vector<double> q, std::vector<double> w) { return MA(fs*duration, q, w); }
                            static std::vector<double> MA(double fs,                        std::vector<double> q, std::vector<double> w);
                     
                     inline static std::vector<double> ARMA(double fs, const double duration, double c, std::vector<double> p, std::vector<double> q, std::vector<double> AR0, double sigma = 1     ) { return ARMA(fs*duration, c, p, q, AR0, Gaussian(fs, duration, 0, sigma)); }
                     inline static std::vector<double> ARMA(double fs,                        double c, std::vector<double> p, std::vector<double> q, std::vector<double> AR0, double sigma = 1     ) { return ARMA(fs,          c, p, q, AR0, Gaussian(fs,        1, 0, sigma)); }
                     inline static std::vector<double> ARMA(double fs, const double duration, double c, std::vector<double> p, std::vector<double> q, std::vector<double> AR0, std::vector<double> w) { return OMU::Add(OMU::TKFR::AR(fs*duration, c, p, AR0, std::vector<double>(fs*duration, 0)), OMU::TKFR::MA(fs*duration, q, w)); }
                     inline static std::vector<double> ARMA(double fs,                        double c, std::vector<double> p, std::vector<double> q, std::vector<double> AR0, std::vector<double> w) { return OMU::Add(OMU::TKFR::AR(fs,          c, p, AR0, std::vector<double>(fs,          0)), OMU::TKFR::MA(fs,          q, w)); }

                     inline static std::vector<double> RandomWalk(double fs, const double duration, double step, double sigma = 1) { return RandomWalk(fs, duration, step, Gaussian(fs, duration, 0, sigma)); }
                            static std::vector<double> RandomWalk(double fs, const double duration, double step, std::vector<double> w) { return AR(fs*duration, step, std::vector<double>({1}), std::vector<double>({0}), w); }
                     
                     // Signal whitening
                     static std::vector<Complex> Whiten(const std::vector<Complex> &x, const std::vector<double> &ASD, double fs, Option_t *option = "",  int nFFT = 0, double timeshift = 0, double phase = 0);
                     static std::vector<Complex> Whiten(const std::vector<Complex> &x,                                 double fs, Option_t *option = "",  int nFFT = 0, double timeshift = 0, double phase = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     static std::vector<double>  Whiten(const std::vector<double>  &x, const std::vector<double> &ASD, double fs, Option_t *option = "",  int nFFT = 0, double timeshift = 0, double phase = 0);
                     static std::vector<double>  Whiten(const std::vector<double>  &x,                                 double fs, Option_t *option = "",  int nFFT = 0, double timeshift = 0, double phase = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     static TH1*                 Whiten(TH1 *h,                             TH1 *hASD,                            Option_t *option = "x", int nFFT = 0, double timeshift = 0, double phase = 0);
                     static TH1*                 Whiten(TH1 *h,                                                                   Option_t *option = "x", int nFFT = 0, double timeshift = 0, double phase = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);

                     static std::vector<Complex> Downsample(const std::vector<Complex> &x, double fs, double fdown, int offset = 0);
                     static std::vector<double>  Downsample(const std::vector<double>  &x, double fs, double fdown, int offset = 0);
                     static TH1 *                Downsample(TH1 *h1, Option_t *option, double fdown, int offset = 0);
                     
                     static std::vector<Complex> Upsample  (const std::vector<Complex> &x, double fs, double fup,   int offset = 0);
                     static std::vector<double>  Upsample  (const std::vector<double>  &x, double fs, double fup,   int offset = 0);
                     static TH1 *                Upsample  (TH1 *h1, Option_t *option, double fup,   int offset = 0);
                     
                     static std::vector<Complex> Decimate(const std::vector<Complex> &x, double fs, double fdown, int offset = 0, Method method = Method::FFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<double, double> &filterRpRs = {});
                     static std::vector<double>  Decimate(const std::vector<double>  &x, double fs, double fdown, int offset = 0, Method method = Method::FFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<double, double> &filterRpRs = {});
                     static TH1*                 Decimate(TH1 *h1, Option_t *option,                double fdown, int offset = 0, Method method = Method::FFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<double, double> &filterRpRs = {});

                     static std::vector<Complex> Interpolate(const std::vector<Complex> &x, double fs, double fup, int offset = 0, Method method = Method::FFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<double, double> &filterRpRs = {});
                     static std::vector<double>  Interpolate(const std::vector<double>  &x, double fs, double fup, int offset = 0, Method method = Method::FFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<double, double> &filterRpRs = {});
                     static TH1*                 Interpolate(TH1 *h1, Option_t *option,                double fup, int offset = 0, Method method = Method::FFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<double, double> &filterRpRs = {});

                     //http://www.ws.binghamton.edu/fowler/fowler%20personal%20page/EE521_files/IV-05%20Polyphase%20FIlters%20Revised.pdf
                     //https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.resample.html
                     inline static std::vector<Complex>  ZoomFFT(const std::vector<double>  &x, double fs, double fc, double bw,                   Method method = Method::FFT) { return Resample(x, fs, fc, bw, method); }
                     inline static std::vector<Complex>  ZoomFFT(const std::vector<Complex> &x, double fs, double fc, double bw,                   Method method = Method::FFT) { return Resample(x, fs, fc, bw, method); };
                     inline static std::vector<Complex> Resample(const std::vector<double>  &x, double fs, double fc, double bw,                   Method method = Method::FFT) { return Resample(OMU::Complexify(x), fs, fc, bw, method); }
                            static std::vector<Complex> Resample(const std::vector<Complex> &x, double fs, double fc, double bw,                   Method method = Method::FFT);
                     inline static std::vector<double>  Resample(const std::vector<double>  &x, double fs, double newFs,                           Method method = Method::FFT) { return OMU::Real(Resample(OMU::Complexify(x), fs, newFs, OMU::TKFR::Domain::Frequency, method)); }
                            static std::vector<Complex> Resample(const std::vector<Complex> &x, double fs, double newFs, OMU::TKFR::Domain domain, Method method = Method::FFT);
                            static TH1*                 Resample(TH1 *h, Option_t *option,                 double newFs,                           Method method = Method::FFT);

                     inline static std::vector<std::vector<double>>  Channelize(int M, const std::vector<double>  &x, double fs,                   Method method = Method::FFT) { return OMU::Real(Channelize(M, OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, method)); }
                            static std::vector<std::vector<Complex>> Channelize(int M, const std::vector<Complex> &x, double fs, OMU::TKFR::Domain domain, Method method = Method::FFT);
                            static TH2*                              Channelize(int M, TH1 *h, Option_t *option,                                   Method method = Method::FFT);

                     // https://jp.mathworks.com/help/dsp/ref/channelsynthesizer.html
                     inline static std::vector<std::vector<double>>  Synthesize(const std::vector<std::vector<double>>  &x, double fs,                           Method method = Method::FFT) { return OMU::Real(Synthesize(OMU::Complexify(x), fs, OMU::TKFR::Domain::Frequency, method)); }
                            static std::vector<std::vector<Complex>> Synthesize(const std::vector<std::vector<Complex>> &x, double fs, OMU::TKFR::Domain domain, Method method = Method::FFT);
                            static TH2*                              Synthesize(TH2 *h, Option_t *option,                                                        Method method = Method::FFT);

                     inline static std::vector<std::vector<double>>  VQT(ComplexPart part, const std::vector<double> &x, Option_t *option = "",  int nFFT = 0) { return Get(part,              VQT(OMU::Complexify(x), option, nFFT)); }
                     inline static std::vector<std::vector<double>>  VQT(                  const std::vector<double> &x, Option_t *option = "",  int nFFT = 0) { return Get(ComplexPart::Real, VQT(OMU::Complexify(x), option, nFFT)); }
                     inline static std::vector<std::vector<double>>  VQT(ComplexPart part,       std::vector<Complex> x, Option_t *option = "",  int nFFT = 0) { return Get(part,              VQT(                x , option, nFFT)); }
                            static std::vector<std::vector<Complex>> VQT(                        std::vector<Complex> x, Option_t *option = "",  int nFFT = 0);
                     inline static TH2*                              VQT(                  TH1 *h,                       Option_t *option = "x", int nFFT = 0) { return VQT(ComplexPart::Real, h, option, nFFT); }
                            static TH2*                              VQT(ComplexPart part, TH1 *h,                       Option_t *option = "x", int nFFT = 0);

                     inline static std::vector<std::vector<double>>  CQT(ComplexPart part, const std::vector<double> &x, Option_t *option = "",  int nFFT = 0) { return Get(part,              CQT(OMU::Complexify(x), option, nFFT)); }
                     inline static std::vector<std::vector<double>>  CQT(                  const std::vector<double> &x, Option_t *option = "",  int nFFT = 0) { return Get(ComplexPart::Real, CQT(OMU::Complexify(x), option, nFFT)); }
                     inline static std::vector<std::vector<double>>  CQT(ComplexPart part,       std::vector<Complex> x, Option_t *option = "",  int nFFT = 0) { return Get(part,              CQT(                x , option, nFFT)); }
                            static std::vector<std::vector<Complex>> CQT(                        std::vector<Complex> x, Option_t *option = "",  int nFFT = 0);
                     inline static TH2*                              CQT(                  TH1 *h,                       Option_t *option = "x", int nFFT = 0) { return CQT(ComplexPart::Real, h, option, nFFT); }
                            static TH2*                              CQT(ComplexPart part, TH1 *h,                       Option_t *option = "x", int nFFT = 0);

                     // Inner product is used in the computation of SNR
                     inline static std::vector<double>  NoiseWeightedInnerProduct(const std::vector<double>  &x, const std::vector<double>  &y, double fs, const std::vector<double> &PSD = std::vector<double>{},                                           Option_t *option = "", int nFFT = 0, double fLow = NAN, double fHigh = NAN) { return OMU::Real(NoiseWeightedInnerProduct(OMU::Complexify(x), OMU::Complexify(y), fs, PSD, OMU::TKFR::Domain::Frequency,        option, nFFT, fLow, fHigh)); }
                     inline static std::vector<Complex> NoiseWeightedInnerProduct(const std::vector<double>  &x, const std::vector<Complex> &y, double fs, const std::vector<double> &PSD = std::vector<double>{},                                           Option_t *option = "", int nFFT = 0, double fLow = NAN, double fHigh = NAN) { return           NoiseWeightedInnerProduct(OMU::Complexify(x),                 y , fs, PSD, OMU::TKFR::Domain::ComplexFrequency, option, nFFT, fLow, fHigh);  }
                     inline static std::vector<Complex> NoiseWeightedInnerProduct(const std::vector<Complex> &x, const std::vector<double>  &y, double fs, const std::vector<double> &PSD = std::vector<double>{},                                           Option_t *option = "", int nFFT = 0, double fLow = NAN, double fHigh = NAN) { return           NoiseWeightedInnerProduct(                x , OMU::Complexify(y), fs, PSD, OMU::TKFR::Domain::ComplexFrequency, option, nFFT, fLow, fHigh);  }
                            static std::vector<Complex> NoiseWeightedInnerProduct(const std::vector<Complex> &x, const std::vector<Complex> &y, double fs,       std::vector<double>  PSD = std::vector<double>{}, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0, double fLow = NAN, double fHigh = NAN);

                     inline static std::vector<double> FrequencyMoment(int n, const std::vector<double>  &h, double fs, const std::vector<double> &PSD = std::vector<double>{},                                           Option_t *option = "", int nFFT = 0, double fLow = NAN, double fHigh = NAN) { return FrequencyMoment(n, OMU::Complexify(h), fs, PSD, OMU::TKFR::Domain::Frequency,        option, nFFT, fLow, fHigh); }
                            static std::vector<double> FrequencyMoment(int n, const std::vector<Complex> &h, double fs,       std::vector<double>  PSD = std::vector<double>{}, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "", int nFFT = 0, double fLow = NAN, double fHigh = NAN);

                     //
                     // NB: About Signal-to-Noise ratio.. 
                     //     If not provided, PSD is computed using the first four seconds of the signal.
                     inline static std::pair<std::vector<Complex>, double> SNR(const std::vector<double>  &x, const std::vector<double>  &y, double fs, const std::vector<double> &PSD = std::vector<double>{},                                           Option_t *option = "",  int nFFT = 0, double fLow = NAN, double fHigh = NAN) { return SNR(OMU::Complexify(x), OMU::Complexify(y), fs, PSD, OMU::TKFR::Domain::Frequency,        option, nFFT, fLow, fHigh); }
                     inline static std::pair<std::vector<Complex>, double> SNR(const std::vector<double>  &x, const std::vector<Complex> &y, double fs, const std::vector<double> &PSD = std::vector<double>{},                                           Option_t *option = "",  int nFFT = 0, double fLow = NAN, double fHigh = NAN) { return SNR(OMU::Complexify(x),                 y , fs, PSD, OMU::TKFR::Domain::ComplexFrequency, option, nFFT, fLow, fHigh); }
                     inline static std::pair<std::vector<Complex>, double> SNR(const std::vector<Complex> &x, const std::vector<double>  &y, double fs, const std::vector<double> &PSD = std::vector<double>{},                                           Option_t *option = "",  int nFFT = 0, double fLow = NAN, double fHigh = NAN) { return SNR(                x , OMU::Complexify(y), fs, PSD, OMU::TKFR::Domain::ComplexFrequency, option, nFFT, fLow, fHigh); }
                            static std::pair<std::vector<Complex>, double> SNR(const std::vector<Complex> &x, const std::vector<Complex> &y, double fs,       std::vector<double>  PSD = std::vector<double>{}, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, double fLow = NAN, double fHigh = NAN);
                            static std::pair<std::vector<Complex>, double> SNR(                      TH1 *h1,                       TH1 *h2,                  TH1 *hPSD = NULL,                                                                           Option_t *option = "x", int nFFT = 0, double fLow = NAN, double fHigh = NAN);
                            static std::pair<TH1*, double>                 SNR(ComplexPart part,     TH1 *h1,                       TH1 *h2,                  TH1 *hPSD = NULL,                                                                           Option_t *option = "x", int nFFT = 0, double fLow = NAN, double fHigh = NAN);

                     //
                     // Some considerations about signal filtering..
                     // N is the filter order
                     // wp/ws is the pass/stop width
                     // rp/rs is the "passband ripple"/"bandstop attenuation"
                     //
                     // NB-1: In case you provide [wp,ws] no need to provide the order.. (The smallest order will be computed)
                     // NB-2: For notching, you can just provide [df and df2] the smallest will be used as bandstop
                     // NB-3: Filters output "SOS" have be used in combination with Filtering() call
                     static SOS Elliptic   (const std::vector<double> &wp, const std::vector<double> &ws, FilterType filterType, const std::pair<double, double> &RpRs, const std::vector<double> &f, double fs);
                     static SOS ChebyshevI (const std::vector<double> &wp, const std::vector<double> &ws, FilterType filterType, const std::pair<double, double> &RpRs, const std::vector<double> &f, double fs);
                     static SOS ChebyshevII(const std::vector<double> &wp, const std::vector<double> &ws, FilterType filterType, const std::pair<double, double> &RpRs, const std::vector<double> &f, double fs);
                     static SOS Butterworth(const std::vector<double> &wp, const std::vector<double> &ws, FilterType filterType, const std::pair<double, double> &RpRs, const std::vector<double> &f, double fs);

                     static SOS Elliptic   (int N, FilterType filterType, double rp, double rs,                  const std::vector<double> &f, double fs);
                     static SOS ChebyshevI (int N, FilterType filterType, double rp,                             const std::vector<double> &f, double fs);
                     static SOS ChebyshevII(int N, FilterType filterType,            double rs,                  const std::vector<double> &f, double fs);
                     static SOS Butterworth(int N, FilterType filterType,                                        const std::vector<double> &f, double fs);
                     static SOS Bessel     (int N, FilterType filterType,                                        const std::vector<double> &f, double fs);

                     static SOS Biquad(FilterType filter, double f, double fs, double Q = 0.5, double gain = 1);
                     static SOS Biquad(int N,                                                               FilterDesign design, const std::pair<double,double> &filterRpRs, FilterType filter, const std::vector<double> &f, double fs);
                     static SOS Biquad(       const std::vector<double> &wp, const std::vector<double> &ws, FilterDesign design, const std::pair<double,double> &filterRpRs, FilterType filter, const std::vector<double> &f, double fs);
                     
                     inline static SOS Notch(double                     f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, double rp, double rs, int N) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs), N); }
                            static SOS Notch(double                     f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, const std::pair<double,double> &filterRpRs, int N);
                     inline static SOS Notch(const std::vector<double> &f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, double rp, double rs, int N) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs), N); }
                            static SOS Notch(const std::vector<double> &f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, const std::pair<double,double> &filterRpRs, int N);
                     inline static SOS Notch(double                     f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, double rp, double rs) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs)); }
                            static SOS Notch(double                     f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, const std::pair<double,double> &filterRpRs);
                     inline static SOS Notch(const std::vector<double> &f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, double rp, double rs) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs)); }
                            static SOS Notch(const std::vector<double> &f, const std::pair<double,double> &df, double fs, FilterDesign filterDesign, const std::pair<double,double> &filterRpRs);
                     inline static SOS Notch(double                     f, const std::pair<double,double> &df, double fs) { return Notch(std::vector<double>({f}), df, fs, OMU::TKFR::FilterDesign::Butterworth, NAN, NAN, 4); };
                     inline static SOS Notch(const std::vector<double> &f, const std::pair<double,double> &df, double fs) { return Notch(f, df, fs, OMU::TKFR::FilterDesign::Butterworth, NAN, NAN, 4); };

                     inline static  TF Analog(const  TF &digital, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN) { return  tf(Analog(ss(digital), Ts, method, alpha)); }
                     inline static ZPK Analog(const ZPK &digital, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN) { return zpk(Analog(ss(digital), Ts, method, alpha)); }
                     inline static SOS Analog(const SOS &digital, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN) { return sos(Analog(ss(digital), Ts, method, alpha)); }
                            static  SS Analog(const  SS &digital, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN);

                     inline static  TF Digital(const  TF &analog, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN) { return  tf(Digital(ss(analog), Ts, method, alpha)); }
                     inline static ZPK Digital(const ZPK &analog, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN) { return zpk(Digital(ss(analog), Ts, method, alpha)); }
                     inline static SOS Digital(const SOS &analog, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN) { return sos(Digital(ss(analog), Ts, method, alpha)); }
                            static  SS Digital(const  SS &analog, double Ts, FilterTransform method = FilterTransform::Tustin, double alpha = NAN);
                     
              private:
                     //NB: xi and yi are copied for mt purpose
                     static void SpectrogramSlice(int i, int K, int N, std::vector<double> &slice, ComplexPart part, const std::vector<Complex> x, const std::vector<Complex> y, double fs, OMU::TKFR::Domain domain = OMU::TKFR::Domain::ComplexFrequency, Option_t *option = "",  int nFFT = 0, Estimator estimator = Estimator::Welch, double noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::None, WindowType window = WindowType::Hann, double ab = NAN, WindowSymmetry symmetry = OMU::TKFR::WindowSymmetry::symmetric);
                     
                ClassDef(OMU::TKFR,1);
        };
}
#endif
