#ifndef OMU_TKFRHelpers_H
#define OMU_TKFRHelpers_H

#include <OMU/MathUtils.h>
#include <Math/Polynomial.h>
#include <TComplex.h>
#include <TMatrixDEigen.h>
#include <TDecompLU.h>
#include <kfr/all.hpp>

namespace OMU
{
       using Complex = std::complex<double>;
       
       using TQuad = std::tuple<double, double, double>;
       using TBiquad = kfr::biquad_params<double>;

       template <typename T>
       using _ZPK = kfr::zpk<T>;
       using  ZPK = _ZPK<double>;

       template <typename T>
       using _SOS = std::vector<kfr::biquad_params<T>>;
       using  SOS = _SOS<double>;

       template <typename T>
       using _TF = std::pair<std::vector<T>,std::vector<T>>; /* (B,A) */
       using  TF = _TF<double>;

       template <typename T>
       using _SS = std::tuple<TMatrixT<T>,TMatrixT<T>,TMatrixT<T>,TMatrixT<T>>; /* (A,B,C,D) */
       using  SS = _SS<double>;
       
       template <typename T>
       inline kfr::univector<std::complex<T>> kfrvector(const std::vector<std::complex<T>> &u)
       {
              kfr::univector<std::complex<T>> v(u.size());
              for(int i = 0, N = u.size(); i < N; i++)
                     v[i] = u[i];

              return v;
       }

       template <typename T>
       inline std::vector<T> stdvector(const kfr::univector<T> &u)
       {
              std::vector<T> v;
              for(int i = 0, N = u.size(); i < N; i++)
                     v.push_back(u[i]);

              return v;
       }

       template <typename T>
       inline std::vector<T> stdvector(const TVectorT<T> &v)
       {
              return std::vector<T>(v.GetMatrixArray(), v.GetMatrixArray() + v.GetNrows());
       }

       template <typename T>
       inline TVectorT<T> rootvector(const std::vector<const T> &v)
       {
              TVectorT<T> rootv(v.size());
              for(int i = 0, N = v.size(); i < N; i++)
                     rootv(i) = v[i];

              return rootv;
       }

       template <typename T>
       inline TVectorT<T> rootvector(const std::vector<T> &v)
       {
              TVectorT<T> rootv(v.size());
              for(int i = 0, N = v.size(); i < N; i++)
                     rootv(i) = v[i];

              return rootv;
       }

       template <typename T>
       inline std::vector<std::complex<T>> stdvector(const kfr::univector<std::complex<T>> &u)
       {
              std::vector<std::complex<T>> v;
              for(int i = 0, N = u.size(); i < N; i++)
                     v.push_back(u[i]);

              return v;
       }

       template <typename T>
       inline kfr::univector<T> kfrvector(const std::vector<T> &u)
       {
              kfr::univector<T> v(u.size());
              for(int i = 0, N = u.size(); i < N; i++)
                     v[i] = u[i];

              return v;
       }

       template <typename T>
       inline T Real(const std::complex<T>& z) { return z.real(); }
       template <typename T>
       inline std::vector<T> Real(const std::vector<std::complex<T>>& z)
       {
              std::vector<T> v;
              for(int i = 0, N = z.size(); i < N; i++)
                     v.push_back(z[i].real());

              return v;
       }
       template <typename T>
       inline std::vector<std::vector<T>> Real(const std::vector<std::vector<std::complex<T>>>& z)
       {
              std::vector<std::vector<T>> v;
              for(int i = 0, N = z.size(); i < N; i++)
                     v.push_back(OMU::Real(z[i]));

              return v;
       }

       template <typename T>
       inline T Imag(const std::complex<T>& z) { return z.imag(); }
       template <typename T>
       inline std::vector<T> Imag(const std::vector<std::complex<T>>& z)
       {
              std::vector<T> v;
              for(int i = 0, N = z.size(); i < N; i++)
                     v.push_back(z[i].imag());

              return v;
       }
       template <typename T>
       inline std::vector<std::vector<T>> Imag(const std::vector<std::vector<std::complex<T>>>& z)
       {
              std::vector<std::vector<T>> v;
              for(int i = 0, N = z.size(); i < N; i++)
                     v.push_back(OMU::Imag(z[i]));

              return v;
       }

       template <typename T>
       inline std::complex<T> Complexify(const T &a, const T &b = 0) { return std::complex<T>(a, b); }
       template <typename T>
       inline std::vector<std::complex<T>> Complexify(const std::vector<T>& a, const std::vector<T>& b = std::vector<T>())
       {
              std::vector<std::complex<T>> v;
              for(unsigned int i = 0, N = TMath::Max(a.size(), b.size()); i < N; i++)
                     v.push_back(OMU::Complexify(a.size() > i ? a[i] : 0, b.size() > i ? b[i] : 0));

              return v;
       }
       template <typename T>
       inline std::vector<std::vector<std::complex<T>>> Complexify(const std::vector<std::vector<T>>& a, const std::vector<std::vector<T>>& b = std::vector<std::vector<T>>())
       {
              std::vector<std::vector<std::complex<T>>> v;
              std::vector<T> e;
              for(unsigned int i = 0, N = TMath::Max(a.size(), b.size()); i < N; i++) {
                     v.push_back(OMU::Complexify(i < a.size() ? a[i] : e, i < b.size() ? b[i] : e));
              }

              return v;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Minus(const std::vector<std::complex<T>>& a) { return Transform(a, [](const std::complex<T> &a) { return -a; }); }
       template <typename T>
       inline std::vector<T> Minus(const std::vector<T>& a) { return Transform(a, [](const T &a) { return -a; }); }
       
       template <typename T>
       inline std::vector<std::complex<T>> Negate(const std::vector<std::complex<T>>& x) { return OMU::Minus(x); }
       template <typename T>
       inline std::vector<T> Negate(const std::vector<T>& v) { return OMU::Minus(v); }

       template <typename T>
       inline std::vector<std::complex<T>> Power(const std::vector<std::complex<T>>& X, double y) { return Transform(X, [&y](const std::complex<T> &x) { return pow(x, y); }); }
       template <typename T>
       inline std::vector<T> Power(const std::vector<T>& X, double y) { return Transform(X, [&y](const T &x) { return TMath::Power(x, y); }); }
       template <typename T>
       inline std::complex<T> Power(const std::complex<T>& x, double y) { return pow(x, y); }
       template <typename T>
       inline T Power(const T& x, double y) { return TMath::Power(x, y); }

       template <typename T>
       inline std::vector<std::complex<T>> Sqrt(const std::vector<std::complex<T>>& X) { return OMU::Power(X, 1/2.); }
       template <typename T>
       inline std::vector<T> Sqrt(const std::vector<T>& X) { return OMU::Power(X, 1/2.); }
       template <typename T>
       inline T Sqrt(const T& x) { return OMU::Power(x, 1/2.); }
       template <typename T>
       inline std::complex<T> Sqrt(const std::complex<T>& X) { return OMU::Power(X, 1/2.); }

       template <typename T>
       inline std::complex<T> CConj( const std::complex<T>& z ) { return kfr::cconj(z); }
       template <typename T>
       inline std::vector<std::complex<T>> CConj( const std::vector<std::complex<T>>& z ) { return Transform(z, [](const std::complex<T> &z) { return kfr::cconj(z); }); }

       template <typename T>
       inline std::vector<T> Mag2(const std::vector<std::complex<T>>& z) { return Transform(z, [](const std::complex<T> &z) { return (z.real()*z.real() + z.imag()*z.imag()); }); }
       template <typename T>
       inline std::vector<T> Mag (const std::vector<std::complex<T>>& z) { return Transform(z, [](const std::complex<T> &z) { return sqrt(z.real()*z.real() + z.imag()*z.imag()); }); }
       template <typename T>
       inline std::vector<T> Abs (const std::vector<std::complex<T>>& z) { return Mag(z); }

       template <typename T>
       inline T Mag2(const std::complex<T>& z) { return z.real()*z.real() + z.imag()*z.imag(); }
       template <typename T>
       inline T Mag(const std::complex<T>& z) { return Sqrt(Mag2(z)); }
       template <typename T>
       inline T Abs(const std::complex<T>& z) { return Mag(z); }

       template <typename T>
       inline std::vector<T> Mag2(const std::vector<T>& x) { return Transform(x, [](const T &x) { return x*x; }); }
       template <typename T>
       inline std::vector<T> Mag (const std::vector<T>& x) { return Transform(x, [](const T &x) { return sqrt(x*x); }); }
       template <typename T>
       inline std::vector<T> Abs (const std::vector<T>& x) { return Mag(x); }

       template <typename T>
       inline T Mag2(const T& z) { return z*z; }
       template <typename T>
       inline T Mag(const T& z) { return Sqrt(Mag2(z)); }
       template <typename T>
       inline T Abs(const T& z) { return Mag(z); }

       template <typename T>
       inline std::vector<T> Mag2dB(const std::vector<T>& mag, double mag0 = 1, bool powerQuantity = true)
       {
              double powerFactor = 10 * (powerQuantity ? 1 : 2);
              
              std::vector<T> v;
              for(int i = 0, N = mag.size(); i < N; i++)
                     v.push_back(powerFactor * TMath::Log10(mag[i] / mag0));

              return v;
       }

       template <typename T>
       inline std::vector<T> dB(const std::vector<T>& z, double mag0 = 1, bool powerQuantity = true) { return OMU::Mag2dB(OMU::Mag(z), mag0, powerQuantity); }
       template <typename T>
       inline std::vector<T> dB(const std::vector<std::complex<T>>& z, double mag0 = 1, bool powerQuantity = true) { return OMU::Mag2dB(OMU::Mag(z), mag0, powerQuantity); }
       template <typename T>
       inline std::vector<T> dB2Mag(const std::vector<T>& dB, double mag0 = 1, bool powerQuantity = true)
       {
              double powerFactor = 10 * (powerQuantity ? 1 : 2);
              
              std::vector<T> v;
              for(int i = 0, N = dB.size(); i < N; i++)
                     v.push_back(mag0 * TMath::Power(10, dB / powerFactor));

              return v;
       }

       template <typename T>
       inline T Mag2dB(const T& mag, double mag0 = 1, bool powerQuantity = true)
       {
              double powerFactor = 10 * (powerQuantity ? 1 : 2);
              return powerFactor * TMath::Log10(mag / mag0);
       }

       template <typename T>
       inline T dB(const T& z, double mag0 = 1, bool powerQuantity = true) { return OMU::Mag2dB(OMU::Mag(z), mag0, powerQuantity); }
       template <typename T>
       inline T dB(const std::complex<T>& z, double mag0 = 1, bool powerQuantity = true) { return OMU::Mag2dB(OMU::Mag(z), mag0, powerQuantity); }
       template <typename T>
       inline T dB2Mag(const T& dB, double mag0 = 1, bool powerQuantity = true)
       {
              double powerFactor = 10 * (powerQuantity ? 1 : 2);
              return mag0 * TMath::Power(10, dB / powerFactor);
       }

       template <typename T>
       inline T Argument(const std::complex<T>& z, T offset = 0, bool extend = true) { return (extend ? TMath::ATan2(z.imag(), z.real()) : TMath::ATan(z.imag()/z.real())) + UIUC::MathMod(offset, 2*TMath::Pi()); }
       template <typename T>
       inline std::vector<T> Argument(const std::vector<std::complex<T>>& z, T offset = 0, bool extend = true)
       {
              std::vector<T> v;
              for(int i = 0, N = z.size(); i < N; i++) {
                     v.push_back(Argument(z[i], offset, extend));
              }

              return v;
       }

       template <typename T>
       inline std::vector<T> Unwrap(std::vector<T> v, double discount = NAN, double period = NAN) 
       { 
              if(UIUC::IsNaN(period)) period = 2*TMath::Pi();
              if(UIUC::IsNaN(discount)) discount = period/2;

              for(int i = 1, N = v.size(); i < N; i++) {
                     
                     double p0 = v[i-1];
                     double p  = v[i];

                     double dd = fmod((p - p0) + period/2., period);
                     
                     if (dd < 0) dd += period;
                     v[i] = p0 + (dd - period/2.);
              }

              return v;
       }

       template <typename T>
       inline T Phase(const std::complex<T>& z, T offset = 0, bool extend = true) { return Argument(z, offset, extend); }
       template <typename T>
       inline std::vector<T> Phase(const std::vector<std::complex<T>>& z, T offset = 0, bool extend = true, bool unwrap = true) { return unwrap ? Unwrap(Argument(z, offset, extend)) : Argument(z, offset, extend); }
       template <typename T>
       inline std::vector<T> Phase(const std::vector<T>& v, T offset = 0, bool unwrap = true) { return (unwrap ? Unwrap(v) : v) + UIUC::MathMod(offset, 2*TMath::Pi()); }

       template <typename T>
       inline std::vector<T> Add( const std::vector<T>& a, const std::vector<T>& b )
       {
              if(a.size() != b.size())
                     throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

              std::vector<T> result;
              std::transform( std::begin(a), std::begin(a)+a.size(), std::begin(b),
                            std::back_inserter(result), std::plus<>{});

              return result;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Add( const std::vector<std::complex<T>>& z1, const std::vector<T>& z2 )
       {       
              if(z1.size() != z2.size())
                     throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

              std::vector<std::complex<T>> result;
              for(int i = 0, N = z1.size(); i < N; i++)
                     result.push_back(std::complex<T>(z1[i].real() + z2[i], z1[i].imag()));

              return result;

       }

       template <typename T>
       inline std::vector<T> Add( const std::vector<T>& A, const T& b )
       {
              return Add(A, std::vector<T>(A.size(), b));
       }
       template <typename T>
       inline std::vector<std::complex<T>> Add( const std::vector<std::complex<T>>& z1, const T& z2 )
       {
              return Add(z1, std::vector<T>(z1.size(), z2));
       }
       template <typename T>
       inline std::vector<std::complex<T>> Add( const T& z2, const std::vector<std::complex<T>>& z1 ) { return Add(z1, z2); }
       template <typename T>
       inline std::vector<std::complex<T>> Add( const std::vector<std::complex<T>>& z1, const std::complex<T>& z2 ) { return Add(z1, std::vector<std::complex<T>>(z1.size(), z2)); }
       template <typename T>
       inline std::vector<std::complex<T>> Add( const std::complex<T>& z2, const std::vector<std::complex<T>>& z1 ) { return Add(z1, z2); }

       template <typename T>
       inline std::vector<std::complex<T>> Add( const std::vector<std::complex<T>>& z1, const std::vector<std::complex<T>>& z2 )
       {
              if(z1.size() != z2.size())
                     throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

              std::vector<std::complex<T>> result;
              for(int i = 0, N = z1.size(); i < N; i++)
                     result.push_back(std::complex<T>(z1[i].real() + z2[i].real(), z1[i].imag() + z2[i].imag()));

              return result;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Subtract( const std::vector<std::complex<T>>& z1, const std::vector<std::complex<T>>& z2 )
       {
              if(z1.size() != z2.size())
                     throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

              std::vector<std::complex<T>> result;
              for(int i = 0, N = z1.size(); i < N; i++)
                     result.push_back(std::complex<T>(z1[i].real() - z2[i].real(), z1[i].imag() - z2[i].imag()));

              return result;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Divide( const std::vector<std::complex<T>>& z1, const std::vector<std::complex<T>>& z2, bool discardNaN = false)
       {
              if(z1.size() != z2.size())
                     throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

              std::vector<std::complex<T>> result;
              for(int i = 0, N = z1.size(); i < N; i++)
              {
                     T a = z1[i].real(); T b = z1[i].imag();
                     T c = z2[i].real(); T d = z2[i].imag();

                     double  real = (a*c+b*d)/(c*c+d*d);
                            real = discardNaN && UIUC::IsNaN(real) ? 0 : real;

                     double  imag = (b*c-a*d)/(c*c+d*d);
                            imag = discardNaN && UIUC::IsNaN(imag) ? 0 : imag;

                     result.push_back(std::complex<T>(real,imag));
              }

              return result;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Divide( const std::vector<std::complex<T>>& z, const std::vector<T> &x, bool discardNaN = false) { return Divide(z, OMU::Complexify(x), discardNaN); }
       template <typename T>
       inline std::vector<std::complex<T>> Divide( const std::vector<std::complex<T>>& z, const T k, bool discardNaN = false) { return Divide(z, std::vector<std::complex<T>>(z.size(), std::complex<T>(k,0)), discardNaN); }

       template <typename T>
       inline std::vector<std::complex<T>> Multiply( const std::vector<std::complex<T>>& z1, const std::vector<std::complex<T>>& z2 )
       {
              if(z1.size() != z2.size())
                     throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

              std::vector<std::complex<T>> result;
              for(int i = 0, N = z1.size(); i < N; i++) {
              
                     T a = z1[i].real(); T b = z1[i].imag();
                     T c = z2[i].real(); T d = z2[i].imag();

                     result.push_back(std::complex<T>(a*c-b*d,a*d+b*c));
              }

              return result;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Multiply( const std::vector<std::complex<T>>& z, const std::vector<T> &x) { return Multiply(z, OMU::Complexify(x)); }
       template <typename T>
       inline std::vector<std::complex<T>> Multiply( const std::vector<T> &x, const std::vector<std::complex<T>>& z) { return Multiply(z, OMU::Complexify(x)); }
       template <typename T>
       inline std::vector<std::complex<T>> Multiply( const std::vector<std::complex<T>>& z, const T k ) { return OMU::Multiply(z, std::vector<std::complex<T>>(z.size(), std::complex<T>(k,0))); }
       template <typename T>
       inline std::vector<std::complex<T>> Multiply( const T k, const std::vector<std::complex<T>>& z ) { return OMU::Multiply(z, std::vector<std::complex<T>>(z.size(), std::complex<T>(k,0))); }

       template <typename T>
       inline T Sum(const std::vector<T>& v) { 
              
              T d = 0;
              for(int i = 0, N = v.size(); i < N; i++)
                     d += v[i];

              return d;
       }

       template <typename T>
       inline std::complex<T> Sum(const std::vector<std::complex<T>>& v) { 
              
              std::complex<T> d(0,0);
              for(int i = 0, N = v.size(); i < N; i++)
                     d = std::complex<T>(d.real()+v[i].real(), d.imag()+v[i].imag());

              return d;
       }


       template <typename T>
       inline std::vector<T> Subtract( const std::vector<T>& a, const std::vector<T>& b )
       {
              if(a.size() != b.size())
                     throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

              std::vector<T> result ;
              std::transform( std::begin(a), std::begin(a)+a.size(), std::begin(b),
                            std::back_inserter(result), std::minus<>{} ) ;

              return result;
       }
       template <typename T>
       inline std::vector<T> Subtract( const std::vector<T>& a, const T& b )
       {
              std::vector<T> v(a.size(),b);
              return Subtract(a, v);
       }

       template <typename T>
       inline std::vector<std::complex<T>> Subtract( const std::vector<std::complex<T>>& a, const std::complex<T>& b )
       {
              std::vector<std::complex<T>> v(a.size(),b);
              return Subtract(a, v);
       }
       template <typename T>
       inline std::vector<std::complex<T>> Subtract( const std::vector<std::complex<T>>& a, const T& b )
       {
              return Subtract(a, std::complex(b,0));
       }
       template <typename T>
       inline std::vector<std::complex<T>> Subtract( const T& b, const std::vector<std::complex<T>>& a)
       {
              return Subtract(std::complex(b,0), a);
       }
       template <typename T>
       inline std::vector<std::complex<T>> Subtract( const std::complex<T>& b, const std::vector<std::complex<T>>& a)
       {
              std::vector<std::complex<T>> v(a.size(),b);
              return Subtract(v, a);
       }
       template <typename T>
       inline std::vector<std::complex<T>> Subtract( const std::vector<T>& a, const std::complex<T>& b )
       {
              std::vector<std::complex<T>> v(a.size(),b);
              return Subtract(a, v);
       }

       template <typename T>
       inline std::vector<T> Divide( const std::vector<T>& x, const std::vector<T> y, bool discardNaN = false) { return OMU::Real(Divide(OMU::Complexify(x), OMU::Complexify(y), discardNaN)); }

       template <typename T>
       inline std::vector<T> Divide( const std::vector<T>& a, const T k, bool discardNaN = false ) { return Divide(a, std::vector<T>(a.size(), k), discardNaN); }

       template <typename T>
       inline std::vector<T> Multiply( const std::vector<T>& a, const std::vector<T>& b )
       {
              if(a.size() != b.size())
                     throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

              std::vector<T> result;
              std::transform( std::begin(a), std::begin(a)+a.size(), std::begin(b),
                            std::back_inserter(result), std::multiplies<>{} ) ;

              return result;
       }

       template <typename T>
       inline std::vector<T> Multiply( const std::vector<T>& a, const T k ) { return OMU::Multiply(a, std::vector<T>(a.size(), k)); }
       template <typename T>
       inline std::vector<T> Multiply( const T k, const std::vector<T>& a ) { return OMU::Multiply(a, k); }

       template <typename T>
       inline std::vector<T> Mean( const std::vector<std::vector<T>>& V )
       {
              if(V.size() == 0) 
                     return std::vector<T>({});

              std::vector<T> Vn(V[0].size(), 0);
              for(int i = 0, N = V.size(); i < N; i++) {

                     if(Vn.size() != V[i].size())
                            throw std::invalid_argument("Vectors are not same size: Vn("+TString::Itoa(Vn.size(),10)+") != V["+TString::Itoa(i,10)+"]("+TString::Itoa(V[i].size(),10)+")");

                     Vn = OMU::Add(Vn, V[i]);
              }

              return OMU::Multiply(Vn, 1./V.size());
       }


       template <typename T>
       inline T Mean( const std::vector<T>& V )
       {
              T mean = 0;

              if(V.size() == 0) return mean;
              for(int i = 0, N = V.size(); i < N; i++)
                     mean += V[i];

              return mean / ((double) V.size());
       }

       template <typename T>
       inline std::complex<T> Mean( const std::vector<std::complex<T>>& V )
       {
              std::complex<T> mean = 0;

              if(V.size() == 0) return mean;
              for(int i = 0, N = V.size(); i < N; i++)
                     mean = mean + V[i];

              return mean / ((double) V.size());
       }


       template <typename T>
       inline std::vector<std::complex<T>> Mean( const std::vector<std::vector<std::complex<T>>>& V )
       {
              if(V.size() == 0) 
                     return std::vector<std::complex<T>>({});

              std::vector<std::complex<T>> Vn(V[0].size(), 0);
              for(int i = 0, N = V.size(); i < N; i++) {

                     if(Vn.size() != V[i].size())
                            throw std::invalid_argument("Vectors are not same size: Vn("+TString::Itoa(Vn.size(),10)+") != V["+TString::Itoa(i,10)+"]("+TString::Itoa(V[i].size(),10)+")");

                     OMU::Add(Vn, V[i]);
              }

              return OMU::Multiply(Vn, 1./V.size());
       }
       template <typename T>
       inline std::vector<std::complex<T>> Scale   ( const std::vector<std::complex<T>>& z, const T k ) 
       { 
              std::complex<T> mean = OMU::Mean(z);
              return OMU::Add(OMU::Multiply(OMU::Subtract(z, mean), std::vector<std::complex<T>>(z.size(), std::complex<T>(k,0))), mean);
       }
       template <typename T>
       inline std::vector<std::complex<T>> Scale   ( const T k, const std::vector<std::complex<T>>& z ) 
       { 
              return OMU::Scale(z, std::vector<std::complex<T>>(z.size(), std::complex<T>(k,0)));
       }

       template <typename T>
       inline std::vector<T> Scale( const std::vector<T>& a, const T k ) 
       { 
              T mean = OMU::Mean(a);
              return OMU::Add(OMU::Multiply(OMU::Subtract(a, mean), k), mean); 
       }

       template <typename T>
       inline std::vector<T> Scale( const T k, const std::vector<T>& a ) { return OMU::Scale(a,k); }

       template <typename T>
       inline T Radian(const std::complex<T>& z) { return Argument(z); }
       template <typename T>
       inline std::vector<T> Radian(const std::vector<std::complex<T>>& z) { return Argument(z); }
       template <typename T>
       inline std::vector<T> Radian(const std::vector<T>& v) { return Argument(v); }

       template <typename T>
       inline T Degree(const std::complex<T>& z) { return Radian(z)*180/TMath::Pi() + 180; }
       template <typename T>
       inline std::vector<T> Degree(const std::vector<std::complex<T>>& z) { return OMU::Add(OMU::Multiply(Radian(z), 180/TMath::Pi()), std::vector<T>(z.size(), 180)); }
       template <typename T>
       inline std::vector<T> Degree(const std::vector<T>& v) { return OMU::Add(OMU::Multiply(Radian(v), 180/TMath::Pi()), 180); }

       template <typename T>
       inline T StdDev( const std::vector<T>& V )
       {
              T stdDev = 0;

              if(V.size() == 0) return stdDev;

              T mean = OMU::Mean(V);
              for(int i = 0, N = V.size(); i < N; i++)
                     stdDev += TMath::Power(V[i]-mean, 2);

              return OMU::Sqrt(mean/V.size());
       }

       template <typename T>
       inline std::complex<T> StdDev( const std::vector<std::complex<T>>& V )
       {
              std::complex<T> stdDev = 0;

              if(V.size() == 0) return stdDev;

              std::complex<T> mean = OMU::Mean(V);
              for(int i = 0, N = V.size(); i < N; i++)
                     stdDev = stdDev + OMU::Power(V[i]-mean, 2);

              return OMU::Sqrt(stdDev/V.size());
       }

       template <typename T>
       inline std::vector<std::complex<T>> StdDev( const std::vector<std::vector<std::complex<T>>>& V )
       {
              if(V.size() == 0) 
                     return std::vector<std::complex<T>>({});

              std::vector<std::complex<T>> mean = OMU::Mean(V);
              std::vector<std::complex<T>> Vn(V[0].size(), 0);
              for(int i = 0, N = V.size(); i < N; i++) {

                     if(Vn.size() != V[i].size())
                            throw std::invalid_argument("Vectors are not same size: Vn("+TString::Itoa(Vn.size(),10)+") != V["+TString::Itoa(i,10)+"]("+TString::Itoa(V[i].size(),10)+")");

                     OMU::Add(Vn, OMU::Power(V[i]-mean, 2));
              }

              return OMU::Sqrt(OMU::Multiply(Vn, 1./V.size()));
       }

       template <typename T>
       inline T Median( std::vector<T> V)
       {
              if(V.size() == 0) 
                     return 0;

              T Vi = V.size();
              sort(V.begin(), V.end());

              if (UIUC::MathMod(Vi, 2) == 0) return (V[Vi / 2 - 1] + V[Vi / 2]) / 2;
              return V[Vi / 2];
       }

       template <typename T>
       inline T Max( std::vector<T> V)
       {
              if(V.size() == 0) 
                     return 0;

              T max = V[0];
              for(int i = 1, N = V.size(); i < N; i++) {
                     max = TMath::Max(max, V[i]);
              }

              return max;
       }

       template <typename T>
       inline T Min( std::vector<T> V)
       {
              if(V.size() == 0) 
                     return 0;

              T min = V[0];
              for(int i = 1, N = V.size(); i < N; i++) {
                     min = TMath::Min(min, V[i]);
              }

              return min;
       }


       template <typename T>
       inline std::vector<std::vector<T> > Transpose(const std::vector<std::vector<std::complex<T>> > &v)
       {
              if (v.size() == 0)
                     return {};

              std::vector<std::vector<std::complex<T>> > vT(v[0].size(), std::vector<std::complex<T>>());

              for (int i = 0; i < v.size(); i++)
                     for (int j = 0; j < v[i].size(); j++)
                            vT[j].push_back(v[i][j]);

              return vT;
       }

       template <typename T>
       inline std::vector<std::vector<T> > Transpose(const std::vector<std::vector<T> > &v)
       {
              if (v.size() == 0)
                     return {};

              std::vector<std::vector<T> > vT(v[0].size(), std::vector<T>());

              for (int i = 0; i < v.size(); i++)
                     for (int j = 0; j < v[i].size(); j++)
                            vT[j].push_back(v[i][j]);

              return vT;
       }

       template <typename T>
       inline T Median( std::vector<std::complex<T>> V)
       {
              if(V.size() == 0) 
                     return std::vector<T>({});

              T Vi = V.size();
              sort(V.begin(), V.end());

              if (UIUC::MathMod(Vi,2) == 0) return (V[Vi / 2 - 1] + V[Vi / 2]) / 2;
              return V[Vi / 2];
       }

       template <typename T>
       inline std::vector<T> Median( std::vector<std::vector<T>> V, bool transpose = true)
       {
              if(V.size() == 0) 
                     return std::vector<T>({});

              if(transpose) V = Transpose(V);

              std::vector<T> Vn(V.size(), 0);
              for(int i = 0, N = V.size(); i < N; i++) {

                     int Vi = V[i].size();
                     sort(V[i].begin(), V[i].end());

                     if (Vi % 2 == 0) Vn[i] = (V[i][Vi / 2 - 1] + V[i][Vi / 2]) / 2;
                     else Vn[i] = V[i][Vi / 2];
              }

              return Vn;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Median( std::vector<std::vector<std::complex<T>>> V, bool transpose = true)
       {
              if(V.size() == 0) 
                     return std::vector<std::complex<T>>({});

              if(transpose) V = Transpose(V);

              std::vector<std::complex<T>> Vn(V.size(), 0);
              for(int i = 0, N = V.size(); i < N; i++) {

                     int Vi = V[i].size();
                     sort(V[i].begin(), V[i].end());

                     if (Vi % 2 == 0) Vn[i] = (V[i][Vi / 2 - 1] + V[i][Vi / 2]) / 2;
                     else Vn[i] = V[i][Vi / 2];
              }

              return Vn;
       }

       template <typename T>
       inline T Mode( std::vector<T> V)
       {
              int index = 0;
              T highest = 0;

              for (int a = 0; a < V.size(); a++) {

                     int count = 1;
                     T position = V.at(a);
                     for (int b = a + 1; b < V.size(); b++) {

                            if (UIUC::EpsilonEqualTo(V.at(b), position)) count++;
                     }
                     
                     if (count >= index) {

                            index = count;
                            highest = position;
                     }
              }

              return highest;
       }

       template <typename T>
       inline T Mode( std::vector<std::complex<T>> V)
       {
              int index = 0;
              std::complex<T> highest = 0;

              for (int a = 0; a < V.size(); a++) {

                     int count = 1;
                     std::complex<T> position = V.at(a);
                     for (int b = a + 1; b < V.size(); b++) {

                            if (UIUC::EpsilonEqualTo(V.at(b), position)) count++;
                     }
                     
                     if (count >= index) {

                            index = count;
                            highest = position;
                     }
              }

              return highest;
              
       }

       template<class Element>
       const std::vector<std::complex<Element>> EigenValues(const TMatrixT<Element> &matrix)
       {
              if(!matrix.GetNrows()) return {};
              if(!matrix.GetNcols()) return {};

              TMatrixDEigen eigenValues(matrix);
              return OMU::Complexify(stdvector(eigenValues.GetEigenValuesRe()), stdvector(eigenValues.GetEigenValuesIm()));
       }

       template<class Element>
       const std::vector<std::complex<Element>> EigenVector(const TMatrixT<Element> &matrix)
       {
              return OMU::EigenValues(matrix);
       }

       template <typename T>
       inline TMatrixT<T> GetVerticalBlockMatrix(const std::vector<TMatrixT<T>> &M) 
       {
              if(!M.size()) return TMatrixT<T>();

              TMatrixT<T> S = M[0];
              for(int k = 1, K = M.size(); k < K; k++) {

                     if(S.GetNcols() != M[k].GetNcols()) throw std::invalid_argument("Unexpected column dimension provided for M["+TString::Itoa(k,10)+"] matrix");
                     
                     S.ResizeTo(S.GetNrows()+M[k].GetNrows(), S.GetNcols());

                     for(int i = 0, I = M[k].GetNrows(); i < I; i++) {

                            for(int j = 0, J = M[k].GetNcols(); j < J; j++) {

                                   S(S.GetNrows()-1+i, j) = M[k](i,j);
                            }              
                     }
              }

              return S;
       }

       template <typename T>
       inline TVectorT<T> Concatenate(const TVectorT<T> &A, const TVectorT<T> &B) { 
       
              TVectorT<T> C = A;
                          C.insert(C.end(), B.begin(), B.end());
              
              return C;
       }

       template <typename T>
       inline TMatrixT<T> GetHorizontalBlockMatrix(const std::vector<TMatrixT<T>> &M) 
       {
              if(!M.size()) return TMatrixT<T>();

              TMatrixT<T> S = M[0];
              
              for(int k = 1, K = M.size(); k < K; k++) {

                     if(S.GetNrows() != M[k].GetNrows()) throw std::invalid_argument("Unexpected row dimension provided for M["+TString::Itoa(k,10)+"] matrix");
                     
                     int Scols = S.GetNcols();
                     S.ResizeTo(S.GetNrows(), S.GetNcols() + M[k].GetNcols());

                     for(int i = 0, I = M[k].GetNrows(); i < I; i++) {

                            for(int j = 0, J = M[k].GetNcols(); j < J; j++) {

                                   S(i, Scols+j) = M[k](i,j);
                            }
                     }
              }

              return S;
       }

       template <typename T>
       inline TMatrixT<T> GetVerticalBlockMatrix(const TMatrixT<T> &A, const TMatrixT<T> &B) { return GetVerticalBlockMatrix(std::vector<TMatrixT<T>>({A,B})); }
       template <typename T>
       inline TMatrixT<T> GetHorizontalBlockMatrix(const TMatrixT<T> &A, const TMatrixT<T> &B) { return GetHorizontalBlockMatrix(std::vector<TMatrixT<T>>({A,B})); }
       template <typename T>
       inline TMatrixT<T> GetBlockMatrix(const TMatrixT<T> &A, const TMatrixT<T> &B) { return GetHorizontalBlockMatrix(A,B); }
       template <typename T>
       inline TMatrixT<T> GetBlockMatrix(const std::vector<std::vector<TMatrixT<T>>> &M) 
       {
              std::vector<TMatrixT<T>> S;
              for(int i = 0, N = M.size(); i < N; i++)
                     S.push_back(OMU::GetHorizontalBlockMatrix(M[i]));

              return OMU::GetVerticalBlockMatrix(S);
       }

       template <typename T>
       inline TMatrixT<T> GetDiagonalBlockMatrix(const std::vector<TMatrixT<T>> &M) 
       {
              if(!M.size()) return TMatrixT<T>();

              TMatrixT<T> S = M[0];
              for(int k = 1, K = M.size(); k < K; k++) {

                     S.ResizeTo(S.GetNrows()+M[k].GetNrows(), S.GetNcols()+M[k].GetNcols());
                     
                     for(int i = 0, I = M[k].GetNrows(); i < I; i++) {

                            for(int j = 0, J = M[k].GetNcols(); j < J; j++) {

                                   S(S.GetNrows()-M[k].GetNrows()+i, S.GetNcols()-M[k].GetNrows()+j) = M[k](i,j);
                            }              
                     }
              }

              return S;
       }
       template <typename T>
       inline TMatrixT<T> GetDiagonalBlockMatrix(const TMatrixT<T> &A, const TMatrixT<T> &B) { return GetDiagonalBlockMatrix(std::vector<TMatrixT<T>>({A,B})); }
       
       template <typename Element>
       inline std::vector<const Element> GetMatrixRow(const TMatrixT<Element> &M, int i)
       {
              int J = M.GetNcols();
              if(i >= M.GetNrows()) return {};

              const Element *array = M.GetMatrixArray();
              return std::vector<const Element>(&array[i*J], &array[i*J] + J);
       }

       template <typename Element>
       inline std::vector<const Element> GetMatrixCol(const TMatrixT<Element> &M, int j)
       {
              std::vector<const Element> col;
              if(j >= M.GetNcols()) return col;

              const Double_t* array = M.GetMatrixArray();
              for (int i = 0; i < M.GetNrows(); i++)
                     col.push_back(array[i * M.GetNcols() + j]);

              return col;
       }

       template <typename T>
       inline void Print(kfr::univector<T> u, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(u.size() == 0) std::cout << "-- Empty vector" << std::endl;
              else std::cout << "-- ";

              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              for(int i = 0, N = u.size(); i < N; i++) {
                     
                     double d = u[i];
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*odd, ' ');
                     std::cout << std::setprecision(epsilon_exp) << "`" << d << "`" ;
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*even, ' ');

                          if((i+1)   == N) std::cout << std::endl;
                     else if((i+1)%n == 0) std::cout << std::endl << "-- ";
              }
              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename Tx, typename Ty>
       inline void Print(std::map<Tx, Ty> m)
       {
              if(m.size() == 0) std::cout << "-- Empty map" << std::endl;
              else std::cout << "-- {" << std::endl;
              
              for(typename std::map<Tx, Ty>::const_iterator it = m.begin(); it != m.end(); ++it)
                     std::cout << "--      `" << it->first << "` : `" << it->second << "`\n";
              
              std::cout << "-- }" << std::endl;
       }

       template <typename Tx, typename Ty>
       inline void Print(std::vector<std::map<Tx, Ty>> vm)
       {
              if(vm.size() == 0) std::cout << "-- Empty map" << std::endl;
              for(int i = 0, N = vm.size(); i < N; i++) OMU::Print(vm[i]);
       }
       
       template <typename T>
       inline void Print(kfr::univector<std::complex<T>> u, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(u.size() == 0) std::cout << "-- Empty vector" << std::endl;
              else std::cout << "-- ";

              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              for(int i = 0, N = u.size(); i < N; i++) {

                     double real = u[i].real();
                     double imag = u[i].imag();
                     
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*odd, ' ');
                     std::cout << std::setprecision(epsilon_exp) << "`" << real << (imag < 0 ? "" : "+")  << imag << "i` " ;
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*even, ' ');

                     if((i+1)   == N) std::cout << std::endl;
                     else if((i+1)%n == 0) std::cout << std::endl << "-- ";
              }
              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename T, kfr::univector_tag Tag>
       inline void Print(kfr::univector<T, Tag> u, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(u.size() == 0) std::cout << "-- Empty vector" << std::endl;
              else std::cout << "-- ";

              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              for(int i = 0, N = u.size(); i < N; i++) {

                     double d = u[i];
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*odd, ' ');
                     std::cout << std::setprecision(epsilon_exp) << "`" << d  << "` ";
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*even, ' ');

                            if((i+1)   == N) std::cout << std::endl;
                     else if((i+1)%n == 0) std::cout << std::endl << "-- ";
              }
              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename T, kfr::univector_tag Tag>
       inline void Print(kfr::univector<std::complex<T>, Tag> u, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(u.size() == 0) std::cout << "-- Empty vector" << std::endl;
              else std::cout << "-- ";

              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              for(int i = 0, N = u.size(); i < N; i++) {
                     
                     double real = u[i].real();
                     double imag = u[i].imag();
              
                     std::cout << epsilon_exp << std::endl;
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*odd, ' ');
                     std::cout << std::setprecision(epsilon_exp) << "`" << real << (imag < 0 ? "" : "+")  << imag << "i" << "` " ;
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon+8)*even, ' ');

                            if((i+1)   == N) std::cout << std::endl;
                     else if((i+1)%n == 0) std::cout << std::endl << "-- ";
              }
              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename T>
       inline void Print(const T &real, double epsilon = 1e-20)
       {
              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              std::cout << std::setprecision(epsilon_exp)  << "`" << real << "`" ;
              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename T>
       inline void Print(const std::complex<T> &z, double epsilon = 1e-20)
       {
              double real = z.real();
              double imag = z.imag();

              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              std::cout << std::setprecision(epsilon_exp)  << "`" << real << (imag < 0 ? "" : "+") << imag << "i" << "` " ;
              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename T>
       inline void Print(const std::vector<T> &v, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
              else std::cout << "-- ";

              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              for(int i = 0, N = v.size(); i < N; i++) {

                     T d = v[i];
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon_exp+8)*odd, ' ');
                     std::cout << std::setprecision(epsilon_exp) << "`" << d << "` ";
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon_exp+8)*even, ' ');

                            if((i+1)   == N) std::cout << std::endl;
                     else if((i+1)%n == 0) std::cout << std::endl << "-- ";
              }

              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename T>
       inline void Print(const std::vector<std::vector<T>> &v, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
              for(int i = 0, N = v.size(); i < N; i++) 
                     OMU::Print(v[i], epsilon, n, even, odd);
       }

       template <typename T>
       inline void Print(const std::vector<std::complex<T>> &v, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
              else std::cout << "-- ";

              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
              for(int i = 0, N = v.size(); i < N; i++) {
                     
                     double real = v[i].real();
                     double imag = v[i].imag();
                     
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon_exp+8)*odd, ' ');
                     std::cout << std::setprecision(epsilon_exp) << "`" << real << (imag < 0 ? "" : "+")  << imag << "i` " ;
                     std::cout << UIUC::HandboxMsg::Spacer((2*epsilon_exp+8)*even, ' ');

                            if((i+1)   == N) std::cout << std::endl;
                     else if((i+1)%n == 0) std::cout << std::endl << "-- ";
              }
              std::cout << std::setprecision(epsilon_init) << std::flush;
       }

       template <typename T>
       inline void Print(const std::vector<std::vector<std::complex<T>>> &v, double epsilon = 1e-20, int n = 32, int even = 0, int odd = 0)
       {
              if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
              for(int i = 0, N = v.size(); i < N; i++) 
                     OMU::Print(v[i], epsilon, n, even, odd);
       }

       template <typename Element>
       inline void Print(const TVectorT<Element> &V, double epsilon = 1e-5, int n = 32, int even = 0, int odd = 0)
       {
              const Element *array = V.GetMatrixArray();
              std::vector<const Element> v(&array[0], &array[0] + V.GetNoElements());
              
              OMU::Print(v, epsilon, n, even, odd);
       }

       template <typename Element>
       inline void Print(const TMatrixT<Element> &M, double epsilon = 1e-5, int n = 32, int even = 0, int odd = 0)
       {
              std::vector<std::vector<const Element>> m(M.GetNrows());
              if(m.size() == 0) std::cout << "-- Rank 0 matrix" << std::endl;

              for(int i = 0, I = M.GetNrows(); i < I; i++)
                     m[i] = GetMatrixRow(M, i);

              OMU::Print(m, epsilon, n, even, odd);
       }

       template <typename T>
       inline void Print(_ZPK<T> zpk)
       {
              std::cout << "-- Zero-Pole-Gain model (z,p,k):" << std::endl;
              
              std::cout << Form("-- z = [");
              for(int i = 0, I = zpk.z.size(); i < I; i++) {
              
                     double real = zpk.z[i].real();
                     double imag = zpk.z[i].imag();

                     if(i > 0) std::cout << ", ";
                     std::cout << Form("%.4e + %.4ei", real, imag);
              }
              std::cout << "]" << std::endl;

              std::cout << Form("-- p = [");
              for(int i = 0, I = zpk.p.size(); i < I; i++) {
              
                     double real = zpk.p[i].real();
                     double imag = zpk.p[i].imag();

                     if(i > 0) std::cout << ", ";
                     std::cout << Form("%.4e + %.4ei", real, imag);
              }
              std::cout << "]" << std::endl;

              std::cout << Form("-- k = `%.4e`", zpk.k) << std::endl;
       }

       template <typename T>
       inline void Print(_SS<T> ss, TString prefix = "")
       {
              std::cout << "-- State-system representation (A,B,C,D):" << std::endl;
              std::cout << prefix << "-- x'(t) = A*x(t) + B*u(t)" << std::endl;
              std::cout << prefix << "--  y(t) = C*x(t) + D*u(t)" << std::endl << std::endl;
              
              std::cout << prefix << "-- Matrix A:" << std::endl;
              OMU::Print(std::get<0>(ss));
              std::cout << prefix << "-- Matrix B:" << std::endl;
              OMU::Print(std::get<1>(ss));
              std::cout << prefix << "-- Matrix C:" << std::endl;
              OMU::Print(std::get<2>(ss));
              std::cout << prefix << "-- Matrix D:" << std::endl;
              OMU::Print(std::get<3>(ss));
       }

       template <typename T>
       inline void Print(kfr::biquad_params<T> biquad, TString prefix = "")
       {
              if(!prefix.EqualTo("")) prefix = prefix + " ";
              std::cout << prefix << "-- Biquad parameters (b,a):" << std::endl;
              std::cout << prefix << Form("-- b0 = `%.4e`, b1 = `%.4e`, b2 = `%.4e`", biquad.b0, biquad.b1, biquad.b2) << std::endl;
              std::cout << prefix << Form("-- a0 = `%.4e`, a1 = `%.4e`, a2 = `%.4e`", biquad.a0, biquad.a1, biquad.a2) << std::endl;
       }

       template <typename T>
       inline void Print(_SOS<T> sos)
       {
              if(sos.size() == 0) std::cout << "-- Empty SOS representation" << std::endl;
              else {

                     std::cout << "-- SOS representation ("<< sos.size() << " sections)" << std::endl;
                     for(int i = 0, n_sections = sos.size(); i < n_sections; i++)
                            Print(sos[i], "-- ");
              }
       }

       template <typename T>
       inline void Print(std::vector<_SOS<T>> sos)
       {
              if(sos.size() == 0) std::cout << "-- Empty SOS representation" << std::endl;
              else {

                     for(int i = 0, N = sos.size(); i < N; i++)
                            Print(sos[i]);
              }
       }
       
       template <typename T>
       inline void Print(std::vector<_TF<T>> tf)
       {
              if(tf.size() == 0) std::cout << "-- Empty TF representation" << std::endl;
              else {

                     for(int i = 0, N = tf.size(); i < N; i++)
                            Print(tf[i]);
              }
       }
       
       template <typename T>
       inline void Print(std::vector<_SS<T>> ss)
       {
              if(ss.size() == 0) std::cout << "-- Empty SS representation" << std::endl;
              else {

                     for(int i = 0, N = ss.size(); i < N; i++)
                            Print(ss[i]);
              }
       }
       
       template <typename T>
       inline void Print(std::vector<_ZPK<T>> zpk)
       {
              if(zpk.size() == 0) std::cout << "-- Empty ZPK representation" << std::endl;
              else {

                     for(int i = 0, N = zpk.size(); i < N; i++)
                            Print(zpk[i]);
              }
       }

       template <typename T>
       inline void Print(_TF<T> TF)
       {
              std::cout << "-- Transfer function (B/A):" << std::endl;
              std::cout << Form("-- B = [");
              for(int i = 0, I = TF.first.size(); i < I; i++) {
              
                     if(i > 0) std::cout << ", ";
                     std::cout << Form("%.4e", TF.first[i]);
              }
              std::cout << "]" << std::endl;

              std::cout << Form("-- A = [");
              for(int i = 0, I = TF.second.size(); i < I; i++) {
              
                     if(i > 0) std::cout << ", ";
                     std::cout << Form("%.4e", TF.second[i]);
              }
              std::cout << "]" << std::endl;

              // std::cout << Form("-- b0 = `%.4e`, b1 = `%.4e`, b2 = `%.4e`", biquad.b0, biquad.b1, biquad.b2) << std::endl;
              // std::cout << Form("-- a0 = `%.4e`, a1 = `%.4e`, a2 = `%.4e`", biquad.a0, biquad.a1, biquad.a2) << std::endl;
       }

       template <typename T>
       inline void PrintDebug(const T a, double epsilon = 1e-2) 
       { 
              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;

              std::cout << std::setprecision(epsilon_exp) << a << std::setprecision(epsilon_init) << std::endl;
       }
       
       template <typename T>
       inline void PrintDebug(const std::complex<T> a, double epsilon = 1e-2)
       {
              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;

              std::cout << "std::complex number: " << std::setprecision(epsilon_exp) << a.real() << " + " << a.imag() << "i" << std::endl;
              std::cout << ">>> abs   = " << OMU::Mag(a) << std::endl;
              std::cout << ">>> angle = " << OMU::Argument(a) << std::endl;
              std::cout << ">>> phase = " << OMU::Phase(a) << std::endl;
              
              std::cout << std::setprecision(epsilon_init) << std::endl << std::endl;
       }

       template <typename T>
       inline void PrintDebug(const std::vector<T> a, int nDisplay = 6, double epsilon = 1e-2) 
       { 
              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;

              int N = a.size();
              std::cout << "std::vector: N="+TString::Itoa(N, 10)  << std::setprecision(epsilon_exp) << std::endl;

              std::cout << ">>>  V  = [";
              for(int i = 0; i < TMath::Min(N, nDisplay/2); i++) 
                     std::cout << a[i] << ", ";

              std::cout << "..";

              for(int i = N-nDisplay/2; i < N; i++) 
                     std::cout << ", "<< a[i];

              std::cout << "]" << std::endl;

              std::complex<T> mean = OMU::Mean(a);
              std::cout << ">>> <V> = " << mean.real() << " + " << mean.imag();
              std::cout << std::setprecision(epsilon_init) << std::endl << std::endl;
       }

       template <typename T>
       inline void PrintDebug(const std::vector<std::complex<T>> a, int nDisplay = 6, double epsilon = 1e-2) 
       {
              int epsilon_init = std::cout.precision();
              int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;

              int N = a.size();
              std::cout << "std::complex vector: N="+TString::Itoa(N, 10) << std::setprecision(epsilon_exp) << std::endl;

              std::cout << ">>>  V  = [";
              for(int i = 0; i < TMath::Min(N, nDisplay/2); i++) 
                     std::cout << a[i].real() <<  "+" << a[i].imag() << "i, ";

              std::cout << "..";

              for(int i = 0; i < TMath::Min(N, nDisplay/2); i++) 
                     std::cout << ", " << a[N-1-i].real() <<  "+" << a[N-1-i].imag() << "i";

              std::cout << "]" << std::endl;

              std::complex<T> mean = OMU::Mean(a);
              std::cout << ">>> <V> = " << mean.real() << " + " << mean.imag() << "i" << std::endl;
              std::complex<T> meanabs = OMU::Mean(OMU::Mag(a));
              std::cout << ">>> <abs(V)> = " << meanabs.real() << " + " << meanabs.imag() << "i";
              std::cout << std::setprecision(epsilon_init) << std::endl << std::endl;
       }

       inline bool  IsPowerOf(int n, int base) { return TMath::Ceil(TMath::Log(n)/TMath::Log(base)) == TMath::Floor(TMath::Log(n)/TMath::Log(base)); }
       inline int NextPowerOf(int n, int base) { return IsPowerOf(n, base) ? n*base : TMath::Power(2, TMath::Ceil(TMath::Log(n)/TMath::Log(base))); }

       template <typename T>
       inline bool ValidSizeFFT(const std::vector<std::complex<T>> &input)
       {
              return IsPowerOf(input.size(), 2) || IsPowerOf(2*(input.size()-1), 2);
       }

       template <typename T>
       inline bool IsOneSidedFFT(const std::vector<T> &input)
       {                
              if(IsPowerOf(   input.size()   , 2)) return false;
              if(IsPowerOf(2*(input.size()-1), 2)) return true;

              throw std::invalid_argument("One-sided check requires a power-of-two length (two-sided) input.");
       }
       
       template <typename T>
       inline bool IsOneSidedFFT(const std::vector<std::complex<T>> &input)
       {
              if(input.size() < 3) return false;

              if(IsPowerOf(   input.size()   , 2)) return false;
              if(IsPowerOf(2*(input.size()-1), 2)) return true;

              throw std::invalid_argument("One-sided check requires a power-of-two length (two-sided) input.");
       }
       
       template <typename T>
       inline std::vector<std::complex<T>> DFT(const std::vector<std::complex<T>> &input)
       {
              if(input.size() == 0) return {};
              if(input.size() == 1) return input;

              kfr::dft_plan_ptr<T> dft = kfr::dft_cache::instance().get(kfr::ctype_t<T>(), input.size());
              std::vector<std::complex<T>> output(input.size(), std::numeric_limits<T>::quiet_NaN());
              std::vector<kfr::u8> temp(dft->temp_size);

              dft->execute(&output[0], &input[0], &temp[0]);
              return output;
       }

       template <typename T>
       inline std::vector<std::complex<T>> DFT(const std::vector<T> &input)
       {
              std::vector<std::complex<T>> complex_input = OMU::Complexify(input);
              return DFT(complex_input);
       }

       template <typename T>
       inline std::vector<std::complex<T>> iDFT(const std::vector<std::complex<T>> &input)
       {
              if(input.size() == 0) return {};
              if(input.size() == 1) return input;

              kfr::dft_plan_ptr<T> dft = kfr::dft_cache::instance().get(kfr::ctype_t<T>(), input.size());
              std::vector<std::complex<T>> output(input.size(), std::numeric_limits<T>::quiet_NaN());
              std::vector<kfr::u8> temp(dft->temp_size);

              dft->execute(&output[0], &input[0], &temp[0], kfr::ctrue);
              return output;
       }

       template <typename T>
       inline std::vector<std::complex<T>> RealDFT(const std::vector<T> &input)
       {
              if(input.size() < 4) {

                     std::vector<std::complex<T>> output = DFT(input);
                     output.resize(input.size()/2+1);

                     return output;
              }
              
              kfr::dft_plan_real_ptr<T> dft = kfr::dft_cache::instance().getreal(kfr::ctype_t<T>(), input.size());
              std::vector<std::complex<T>> output(input.size(), std::numeric_limits<T>::quiet_NaN());
              std::vector<kfr::u8> temp(dft->temp_size);

              dft->execute(&output[0], &input[0], &temp[0]);

              output.resize(input.size() / 2 + 1);
              return output;
       }

       template <typename T>
       inline std::vector<T> iRealDFT(const std::vector<std::complex<T>> &input)
       {
              if(input.size() < 3) {

                     std::vector<std::complex<T>> buff = input;
                     std::vector<std::complex<T>> output = iDFT(buff);
                     output.resize(2*(input.size()-1));

                     return OMU::Real(output);
              }
              
              kfr::dft_plan_real_ptr<T> dft = kfr::dft_cache::instance().getreal(kfr::ctype_t<T>(), (input.size() - 1) * 2);
              std::vector<T> output((input.size() - 1) * 2, std::numeric_limits<T>::quiet_NaN());
              std::vector<kfr::u8> temp(dft->temp_size);
              
              dft->execute(&output[0], &input[0], &temp[0]);
              return output;
       }

       template <typename T>
       inline std::vector<std::complex<T>> FFT(const std::vector<std::complex<T>> &input)
       {
              if(!IsPowerOf(input.size(), 2))
                     throw std::invalid_argument("Radix-2 FFT requires a power-of-two length input (received: "+TString::Itoa(input.size(),10)+")");

              return DFT(input);                
       }

       template <typename T>
       inline std::vector<std::complex<T>> RealFFT(const std::vector<T> &input)
       {
              if(!IsPowerOf(input.size(), 2))
                     throw std::invalid_argument("Real Radix-2 FFT requires a power-of-two length input (received: "+TString::Itoa(input.size(),10)+")");

              return RealDFT(input);
       }

       template <typename T>
       inline std::vector<T> iRealFFT(const std::vector<std::complex<T>> &input)
       {
              if(!IsPowerOf((input.size()-1)/2, 2))
                     throw std::invalid_argument("Inverse Real Radix-2 FFT requires a power-of-two length input (received: "+TString::Itoa(input.size(),10)+")");
              if(!IsOneSidedFFT(input)) 
                     throw std::invalid_argument("Inverse Real Radix-2 FFT requires a one sided input");

              return iRealDFT(input);
       }

       template <typename T>
       inline std::vector<std::complex<T>> FFT(const std::vector<T> &input)
       {
              if(!IsPowerOf(input.size(), 2))
                     throw std::invalid_argument("Rafix-2 FFT requires a power-of-two length input (received: "+TString::Itoa(input.size(),10)+")");

              return RealDFT(input);
       }

       template <typename T>
       inline std::vector<std::complex<T>> iFFT(const std::vector<std::complex<T>> &input)
       {
              if(IsOneSidedFFT(input)) return OMU::Complexify(iRealDFT(input));
              if(!IsPowerOf(input.size(), 2))
                     throw std::invalid_argument("Inverse Rafix-2 FFT requires a power-of-two length input (received: "+TString::Itoa(input.size(),10)+")");

              return iDFT(input);
       }

       inline void ClearFourierCache() { kfr::dft_cache::instance().clear(); }

       template <typename T>
       inline std::vector<T> Rotate(std::vector<T> v, int n)
       {
              std::rotate(v.begin(), v.begin() + UIUC::MathMod(n, v.size()), v.end());
              return v;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Rotate(std::vector<std::complex<T>> v, int n)
       {
              std::rotate(v.begin(), v.begin() + UIUC::MathMod(n, v.size()), v.end());
              return v;
       }

       template <typename T>
       inline std::vector<T> Shift(std::vector<T> v, int n)
       {
              if(v.size() < 1) return std::vector<T>({});
              std::rotate(v.begin(), v.begin() + UIUC::MathMod(n, v.size()), v.end());              

              if(n > 0) std::fill(v.end() - n, v.end(), 0); 
              else if(n < 0) std::fill(v.begin(), v.begin() - n, 0); 

              return v;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Shift(std::vector<std::complex<T>> v, int n)
       {
              std::rotate(v.begin(), v.begin() + UIUC::MathMod(n, v.size()), v.end());
              if(n > 0) std::fill(v.end() - n, v.end(), 0); 
              else if(n < 0) std::fill(v.begin(), v.begin() - n, 0); 

              return v;
       }

       template <typename T>
       inline std::vector<T> Repeat(const std::vector<T> &v, double length)
       {
              std::vector<T> vN(v.size() * length);
              for (std::size_t i = 0; i < (std::size_t) length; ++i)
                     std::copy(v.begin(), v.end(), vN.begin() + i * v.size());

              if(!UIUC::IsInteger(length)) {

                     int rest = v.size() + (length - (int) length);
                     for(int i = 0; i < rest; i++)
                            vN[v.size() * ((int) length) + i] = v[i];
              }

              return vN;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Repeat(const std::vector<std::complex<T>> &v, double length)
       {
              std::vector<std::complex<T>> vN(v.size() * length);
              for (std::size_t i = 0; i < (std::size_t) length; ++i)
                     std::copy(v.begin(), v.end(), vN.begin() + i * v.size());

              if(!UIUC::IsInteger(length)) {

                     int rest = v.size() + (length - (int) length);
                     for(int i = 0; i < rest; i++)
                            vN[v.size() * ((int) length) + i] = v[i];
              }
              
              return vN;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Mirroring(const std::vector<std::complex<T>> &input, unsigned int offset = 0, unsigned int foldingOffset = 0, int conjugate = true)
       {
              int N = 2*(input.size()-offset-foldingOffset)+foldingOffset;
              std::vector<std::complex<T>> output = input;
              
              if(offset > input.size()) return output;
              output.resize(offset+N, 0);

              for(unsigned int i = offset; i < input.size()-foldingOffset; i++) {
                     output[offset+output.size()-i-1] = std::complex<T>(input[i].real(), (conjugate ? -1 : 1) * input[i].imag());
              }

              return output;
       }
       template <typename T>
       inline std::vector<T> Mirroring(const std::vector<T> &input, unsigned int offset = 0, unsigned int foldingOffset = 0)
       {
              int N = 2*(input.size()-offset-foldingOffset)+foldingOffset;
              std::vector<T> output = input;
              
              if(offset > input.size()) return output;
              output.resize(offset+N, 0);

              for(unsigned int i = offset; i < input.size()-foldingOffset; i++) {
                     output[offset+output.size()-i-1] = input[i];
              }

       
              return output;
       }

       template <typename T>
       inline std::vector<std::complex<T>> MirroringFFT(const std::vector<std::complex<T>> &input) 
       { 
              if(!IsOneSidedFFT(input)) 
                     throw std::invalid_argument("Mirroring is only allowed for one sided FFT");
              
              std::vector<std::complex<T>> output = OMU::Mirroring(input, 1, UIUC::MathMod(input.size(), 2) ? 1 : 0);
              return output;
       }

       template <typename T>
       inline std::vector<T> MirroringFFT(const std::vector<T> &input) 
       { 
              if(!IsOneSidedFFT(input)) 
                     throw std::invalid_argument("Mirroring is only allowed for one sided FFT");
              
              return OMU::Mirroring(input, 1, UIUC::MathMod(input.size(), 2) ? 1 : 0);
       }
       
       template <typename T>
       inline std::vector<T> MirroringPSD(const std::vector<T> &input) 
       { 
              if(!IsOneSidedFFT(input)) 
                     throw std::invalid_argument("Mirroring is only allowed for one sided FFT");
              
              std::vector<T> output = OMU::Mirroring(input, 1, UIUC::MathMod(input.size(), 2) ? 1 : 0);
                             output = OMU::Multiply(1/2., output);

              if(output.size()) output[0] = 2*output[0];
              if(output.size() > 1) output[output.size()/2+1] = 2*output[output.size()/2+1];
              return output;
       }

       template <typename T>
       inline bool EpsilonEqualTo( const T& a, const T& b, double epsilon = std::numeric_limits<T>::epsilon() ) { return UIUC::EpsilonEqualTo(a, b, epsilon); }
       template <typename T>
       inline bool EpsilonEqualTo( const std::complex<T>& z1, const std::complex<T>& z2, double epsilon = std::numeric_limits<T>::epsilon() )
       {
              return OMU::EpsilonEqualTo(z1.real(), z2.real(), epsilon) &&
                     OMU::EpsilonEqualTo(z1.imag(), z2.imag(), epsilon);
       }

       template <typename T>
       inline bool EpsilonEqualTo( const std::complex<T>& z, const T& r, double epsilon = std::numeric_limits<T>::epsilon() )
       { 
              return OMU::EpsilonEqualTo(z.real(), r, epsilon) &&
                     OMU::EpsilonEqualTo(z.imag(), 0., epsilon);
       }

       template <typename T>
       inline bool EpsilonEqualTo( const std::vector<T>& a, const std::vector<T>& b, double epsilon = std::numeric_limits<T>::epsilon() )
       {
              if(a.size() != b.size())
                     return false;
                     
              for(int i = 0, N = a.size(); i < N; i++)
                     if(!OMU::EpsilonEqualTo(a[i], b[i], epsilon)) return false;

              return true;
       }

       template <typename T>
       inline bool EpsilonEqualTo( const std::vector<std::complex<T>>& z1, const std::vector<std::complex<T>>& z2, double epsilon = std::numeric_limits<T>::epsilon() )
       {
              if(z1.size() != z2.size())
                     return false;
                     
              for(int i = 0, N = z1.size(); i < N; i++) {

                     if(!OMU::EpsilonEqualTo(z1[i].real(), z2[i].real(), epsilon)) return false;
                     if(!OMU::EpsilonEqualTo(z1[i].imag(), z2[i].imag(), epsilon)) return false;
              }

              return true;
       }

       template <typename T>
       inline bool EpsilonEqualTo( const std::vector<std::complex<T>>& z, const std::vector<T>& r, double epsilon = std::numeric_limits<T>::epsilon() )
       {
              if(z.size() != r.size())
                     return false;
                     
              for(int i = 0, N = z.size(); i < N; i++) {

                     if(!OMU::EpsilonEqualTo(z[i].real(), r[i], epsilon)) return false;
                     if(!OMU::EpsilonEqualTo(z[i].imag(), 0   , epsilon)) return false;
              }

              return true;
       }

       template <typename T>
       inline T StripNaN( const T& a, T replacement = 0 ) { return UIUC::StripNaN(a, replacement); }
       template <typename T>
       inline std::vector<T> StripNaN( const std::vector<T>& v, T replacement = 0 ) {  return UIUC::StripNaN(v, replacement); }
       template <typename T>
       inline std::vector<std::complex<T>> StripNaN( const std::vector<std::complex<T>>& z, T replacement = 0)
       {
              std::vector<std::complex<T>> result;
              for(int i = 0, N = z.size(); i < N; i++)
                     result.push_back(std::complex<T>((UIUC::IsNaN(z[i].real()) || UIUC::IsNaN(z[i].imag())) ? replacement : z[i]));

              return result;
       }

       template <typename T>
       inline T StripInf( const T& a, T replacement = 0 ) { return UIUC::StripInf(a, replacement); }
       template <typename T>
       inline std::vector<T> StripInf( const std::vector<T>& v, T replacement = 0 ) { return UIUC::StripInf(v, replacement); }
       template <typename T>
       inline std::vector<std::complex<T>> StripInf( const std::vector<std::complex<T>>& z, T replacement = 0)
       {
              std::vector<std::complex<T>> result;
              for(int i = 0, N = z.size(); i < N; i++)
                     result.push_back(std::complex<T>((UIUC::IsInf(z[i].real()) || UIUC::IsInf(z[i].imag())) ? replacement : z[i]));

              return result;
       }

       template <typename T>
       bool sort_complex(const std::complex<T> &a, const std::complex<T> &b) {
              return a.real() == b.real() ? a.imag() < b.imag() : a.real() < b.real();
       }

       template <typename T>
       inline std::vector<T> Slice(const std::vector<T>& u, int start, int length = -1)
       {
              if(length < 0) length = u.size()-start;

              std::vector<T> v(u.begin() + start, u.begin() + start + TMath::Min(length, (int) u.size()-start));
                            v.resize(length);

              return v;
       }
       
       template <typename T>
       inline std::vector<std::complex<T>> Slice(const std::vector<std::complex<T>>& u, int start, int length = -1)
       {
              if(length < 0) length = u.size()-start;

              std::vector<std::complex<T>> v(u.begin() + start, u.begin() + start + TMath::Min(length, (int) u.size()-start));
                                          v.resize(length);

              return v;
       }

       template <typename T>
       inline std::vector<T> Span(const std::vector<T> &v, T i0 = -1, T i1 = -1)
       {
              if(v.size() == 0) return v;
              int first = i0 < 0 ? v.front() : i0;
              int last  = i1 < 0 ? v.back() : i1;

              if(i0 > i1)
                     throw std::invalid_argument( "Wrong span pair provided (i0, i1): i0 > i1");

              for(int i = 0, N = v.size(); i < N; i++) {

                     if(i0 <= v[i]) first = v[i];
                     if(i1 >= v[i]) last  = v[i];
              }

              std::vector<T> u(v.begin() + first, v.begin() + last);
              return u;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Span(const std::vector<std::complex<T>> &v, T i0 = -1, T i1 = -1)
       {
              if(v.size() == 0) return v;
              int first = i0 < 0 ? v.front() : i0;
              int last  = i1 < 0 ? v.back() : i1;

              if(i0 > i1)
                     throw std::invalid_argument( "Wrong span pair provided (i0, i1): i0 > i1");

              for(int i = 0, N = v.size(); i < N; i++) {

                     if(i0 <= v[i]) first = v[i];
                     if(i1 >= v[i]) last  = v[i];
              }

              std::vector<std::complex<T>> u(v.begin() + first, v.begin() + last);
              return u;
       }

       template <typename T> // Y = X[a*k+b], b = [0..a-1]
       inline std::vector<std::complex<T>> SliceXn(const std::vector<std::complex<T>>& Xn, int a, int b = 0)
       {
              b = UIUC::MathMod(b, a);

              std::vector<std::complex<T>> Y(Xn.size()/a);
              for(int k = 0, N = Y.size(); k < N; k++)
                     Y[k] = Xn[a*k+b];

              return Y;
       }

       template <typename T> // Y = X[a*k+b], b = [0..a-1]
       inline std::vector<T> SliceXn(const std::vector<T>& Xn, int a, int b = 0)
       {
              b = UIUC::MathMod(b, a);

              std::vector<T> Yn(Xn.size()/a);
              for(int k = 0, N = Yn.size(); k < N; k++)
                     Yn[k] = Xn[a*k+b];
                     
              return Yn;
       }

       template <typename T> // Y[a*k+b] = X[k], b = [0..a-1]
       inline std::vector<std::complex<T>> InsetXn(const std::vector<std::complex<T>>& Xn, int a, int b = 0, const std::complex<T>& v = std::complex<T>(0,0))
       {
              b = UIUC::MathMod(b, a);

              std::vector<std::complex<T>> Y(a*Xn.size(), v);
              for(int k = 0, N = Xn.size(); k < N; k++)
                     Y[a*k+b] = Xn[k];

              return Y;
       }

       template <typename T> // Y[a*k+b] = X[k], b = [0..a-1]
       inline std::vector<T> InsetXn(const std::vector<T>& Xn, int a, int b = 0, const T& v = 0)
       {
              b = UIUC::MathMod(b, a);

              std::vector<T> Y(a*Xn.size(), v);
              for(int k = 0, N = Xn.size(); k < N; k++)
                     Y[a*k+b] = Xn[k];
                     
              return Y;
       }


       template <typename T>
       inline int Index(int index, const std::vector<T> &input) {
              return UIUC::MathMod(index, input.size());
       }

       template <typename T>
       inline std::vector<T> Resize(const std::vector<T> &input, int N, int offset = 0)
       {
              if(input.size() == 0) return {};
              
              std::vector<T> output = input;
              output.resize(UIUC::MathMod(offset, output.size()+1) + N, 0);
              return output;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Resize(const std::vector<std::complex<T>> &input, unsigned int N, int offset = 0)
       {
              if(input.size() == 0) return {};

              std::vector<std::complex<T>> output = input;
                                          output.resize(UIUC::MathMod(offset, output.size()+1) + N, 0);
              
              return output;
       }

       template <typename T>
       inline std::vector<std::complex<T>> ResizeDFT(unsigned int inputSize, const std::vector<std::complex<T>> &inputDFT, unsigned int N, bool isOneSided = false)
       {
              if(inputDFT.size() == 0) return {};

              unsigned int outputSize = TMath::Max((int) (isOneSided ? N/2+1 : N) - 1, (int) 0);
              unsigned int nFFT = TMath::Min(inputSize, N);
              if(inputSize == N) return inputDFT;
                                   
              // Create minimal vector and handle positive frequency
              int iN = nFFT/2+1; // Nyquist frequency candidate
              std::vector<std::complex<T>> outputDFT(inputDFT.begin(), inputDFT.begin() + TMath::Min(iN, (int) inputDFT.size()));
              outputDFT.resize(outputSize+1, 0);
       
              // Handle negative frequencies
              for(int i = 1; nFFT > 2 && !isOneSided && i < iN; i++) {
                     outputDFT[OMU::Index(outputDFT.size()-i, outputDFT)] = inputDFT[OMU::Index(inputDFT.size()-i, inputDFT)];
              }

              // Adjust nyquist frequency, if found
              if( UIUC::MathMod(nFFT, 2) == 0 && N > 0)
              {
                     if(inputSize < N) outputDFT[iN-1] = 0.5*outputDFT[iN-1]; // Upsample
                     else { // Downsample
                     
                            if(isOneSided) outputDFT[iN-1] = 2.0 * outputDFT[iN-1];
                            else {
                                   if(iN == 2) outputDFT[iN-1] = 0; // Special termination case 
                                   else outputDFT[iN-1] = OMU::CConj(outputDFT[iN-1]);

                                   outputDFT[iN-1] = outputDFT[iN-1] + inputDFT[OMU::Index(inputDFT.size()-iN+1, inputDFT)];
                            }
                     }

                     if(!isOneSided) outputDFT[OMU::Index(outputDFT.size()-iN+1, outputDFT)] = outputDFT[iN-1]; // Unfold nyquist frequency onto negative freq
              }

              return OMU::Multiply(outputDFT, ((double) N) / inputSize);
       }

       template <typename T> // Assumes double sided FFT
       inline std::vector<std::complex<T>> ResizeFFT(const std::vector<std::complex<T>> &inputFFT, unsigned int N)
       {
              unsigned int isOneSided = IsOneSidedFFT(inputFFT);
              unsigned int inputSize = isOneSided ? 2*(inputFFT.size()-1) : inputFFT.size();
              return ResizeDFT(inputSize, inputFFT, N, isOneSided);
       }

       template <typename T>
       inline double Length(const std::vector<T> &v) { return v[v.size()-1] - v[0]; }

       template <typename T>
       inline std::vector<std::complex<T>> TimeShift(const std::vector<std::complex<T>> &Xn, T fs, T time) 
       {
              std::vector<std::complex<T>> Yn(Xn.size());
              for(int f = 0, N = Xn.size(); f < N; f++) {
                     T x = 2*TMath::Pi() * (f/fs) * time;
                     Yn[f] = Xn[f] * std::complex<T>(TMath::Cos(x), TMath::Sin(x));
              }

              return Yn;
       }

       template <typename T>
       inline std::vector<std::complex<T>> PhaseShift(const std::vector<std::complex<T>> &Xn, T phase) 
       { 
              std::vector<std::complex<T>> Yn(Xn.size());
              for(int f = 0, N = Xn.size(); f < N; f++) {
                     T x = phase;
                     Yn[f] = Xn[f] * std::complex<T>(TMath::Cos(x), TMath::Sin(x));
              }

              return Yn;
       }

       template <typename T>
       inline std::vector<T> Roll(const std::vector<T> &Xn, int nbins) 
       {
              std::vector<T> Yn(Xn.size());
              for(int i = 0, N = Xn.size(); i < N; i++)
                     Yn[i] = Xn[UIUC::MathMod(i+nbins, Xn.size())];

             
              return Yn;
       }
       
       template <typename T>
       inline std::vector<std::complex<T>> Roll(const std::vector<std::complex<T>> &Xn, int nbins) 
       { 
              std::vector<std::complex<T>> Yn(Xn.size());
              for(int i = 0, N = Xn.size(); i < N; i++)
                     Yn[i] = Xn[UIUC::MathMod(i+nbins, Xn.size())];

              return Yn;
       }

       template <typename T> // Frequency shift in timeserie
       inline std::vector<std::complex<T>> FrequencyShift(const std::vector<T> &x, T fs, T fc) 
       {
              std::vector<std::complex<T>> y(x.size());
              for(int k = 0, N = x.size(); k < N; k++) {
                     T z = 2*TMath::Pi() * (fc/fs) * k;
                     y[k] = x[k] * std::complex<T>(TMath::Cos(z), TMath::Sin(z));
              }

              return y;
       }

       template <typename T> // Frequency shift in timeserie
       inline std::vector<std::complex<T>> FrequencyShift(const std::vector<std::complex<T>> &x, T fs, T fc) 
       {
              std::vector<std::complex<T>> y(x.size());
              for(int k = 0, N = x.size(); k < N; k++) {
                     T z  = 2*TMath::Pi() * (fc/fs) * k;
                     y[k] = x[k] * std::complex<T>(TMath::Cos(z), TMath::Sin(z));
              }

              return y;
       }

       template <typename T>
       inline std::vector<T> Flip(std::vector<T> v)
       {
              std::reverse(v.begin(), v.end());
              return v;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Flip(std::vector<std::complex<T>> v)
       {
              std::reverse(v.begin(), v.end());
              return v;
       }

       template <typename T>
       inline bool Same( const std::vector<std::complex<T>>& a, const std::vector<T>& b )
       {
              if( a.size() != b.size() ) return false;
              if( !UIUC::EpsilonEqualTo(OMU::Mag(b), 0) ) return false;
              
              return Same(OMU::Real(a), b);
       }

       template <typename T>
       inline bool Contains( const T &a, const std::vector<T>& b )
       {
              if(b.size() == 0) return false;
              return b[0] <= a && a <= b[b.size() - 1];
       }
       
       template <typename T>
       inline bool Same( const std::vector<T>& a, const std::vector<std::complex<T>>& b ) { return Same(b, a); }
       template <typename T>
       inline bool Same( const std::vector<T>& a, const std::vector<T>& b )
       {
              if(a.size() != b.size()) return false;
              for(int i = 0, N = a.size(); i < N; i++) {
                     
                     if(!UIUC::EpsilonEqualTo(a[i], b[i])) return false;
              }

              return true;
       }


       template <typename T>
       inline std::vector<T> Filter(std::vector<T> _v, std::vector<int> indexes)
       {
              std::vector<T> v;
              for(int i = 0, N = indexes.size(); i < N; i++)
                     v.push_back(_v[indexes[i]]);

              return v;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Filter(std::vector<std::complex<T>> _v, std::vector<int> indexes)
       {
              std::vector<std::complex<T>> v;
              for(unsigned int i = 0, N = indexes.size(); i < N; i++)
                     v.push_back(_v[indexes[i]]);

              return v;
       }

       template <typename T>
       inline std::vector<T> Range(unsigned int N, T dx)
       {
              std::vector<T> v;
              for(unsigned int i = 0; i < N; i++)
                     v.push_back(i*dx);

              return v;
       }

       template <typename T>
       inline std::vector<T> Range(unsigned int N, T min, T max)
       {
              if(N == 0) return {};
              if(N == 1) return {min, max};

              std::vector<T> v;
              T dx = (max-min)/(N-1);
              for(unsigned int i = 0; i < N; i++)
                     v.push_back(min + i*dx);

              return v;
       }
              
       template <typename T>
       inline std::vector<std::complex<T>> Convolve(std::vector<T> x, std::vector<T> y)
       {
              int N = x.size();
              int M = y.size();

              std::vector<T> z(N + M - 1, 0);
              for (int i = 0; i < N; i++) {
                     for (int j = 0; j < M; j++) {
                            z[i+j] += x[i] * y[j];
                     }
              }

              return z;
       }
       
       template <typename T>
       inline std::vector<std::complex<T>> Convolve(std::vector<std::complex<T>> x, std::vector<std::complex<T>> y)
       {
              int N = x.size();
              int M = y.size();

              std::vector<std::complex<T>> z(N + M - 1, 0);
              for (int i = 0; i < N; i++) {
                     for (int j = 0; j < M; j++) {
                            z[i+j] = z[i+j] + x[i] * y[j];
                     }
              }

              return z;
       }

       template <typename T>
       inline std::vector<std::complex<T>> Correlate(std::vector<std::complex<T>> x, const std::vector<std::complex<T>> &y)
       {
              reverse(x.begin(), x.end());
              return Convolve(x,y);
       }

       template <typename T>
       inline std::vector<std::complex<T>> Correlate(std::vector<T> x, const std::vector<T> &y)
       {
              reverse(x.begin(), x.end());
              return Convolve(x,y);
       }

       template <typename T>
       inline std::vector<std::complex<T>> Autocorrelate(const std::vector<std::complex<T>> & x) { return Correlate(x,x); }

       template <typename T>
       inline std::vector<std::complex<T>> Autocorrelate(const std::vector<T> & x) { return Correlate(x,x); }

       template <typename T>
       inline T Distance(const std::vector<T> a, const std::vector<T> b) 
       {
              if(a.size() != b.size())
                     throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

              T distance = 0;
              for(int i = 0, N = a.size(); i < N; i++)
                     distance += TMath::Power(b[i] - a[i], 2);

              return TMath::Sqrt(distance); 
       }

       template <typename T>
       inline std::pair<T,T> Regression(std::vector<T> x, std::vector<T> y)
       {
              assert(x.size() == y.size());

              T N     = x.size();
              T sumX  = OMU::Sum(x);
              T sumY  = OMU::Sum(y); 
              T sumXY = OMU::Sum(OMU::Multiply(x, y)); 
              T sumX2 = OMU::Sum(OMU::Multiply(x, x));

              T a = (   N*sumXY - sumX*sumY ) / (N*sumX2 - sumX*sumX);
              T b = (sumY*sumX2 - sumX*sumXY) / (N*sumX2 - sumX*sumX);
              
              return std::make_pair(UIUC::IsNaN(a) ? 0 : a, UIUC::IsNaN(b) ? 0 : b);
       }

       template <typename T>
       inline std::pair<T,T> Regression(std::vector<T> x)
       {
              std::vector<T> y = OMU::Range(x.size(), 1.0);
              return Regression(x,y);
       }

       template <typename T>
       inline std::vector<T> polymulti(const std::vector<T> &A, const std::vector<T> &B) {

              std::vector<T> C(A.size()+B.size()-1, 0);                
              for (int i = 0, I = A.size(); i < I; i++) {

                     for (int j = 0, J = B.size(); j < J; j++) {

                            C[i+j] += A[i]*B[j];
                     }
              }
              
              return C;
       }

       template <typename T>
       inline std::vector<T> polycoeff(const std::vector<T>& roots)
       {
              int degree = roots.size();
              std::vector<T> coeffs(degree + 1, 0.0);

              // Initialize the coefficient of x^0 (constant term)
              coeffs[0] = 1.0;

              for (int i = 0; i < degree; i++) {
                     for (int j = degree; j >= 1; j--) {
                            coeffs[j] = coeffs[j] + coeffs[j - 1] * (-roots[i]);
                     }
              }

              return coeffs;
       }

       inline std::vector<double> polvector(const ROOT::Math::Polynomial &P) { return std::vector<double>(P.Parameters(), P.Parameters()+P.NPar()); }
       inline ROOT::Math::Polynomial polynomial(const std::vector<double> &coeffs)
       {
              ROOT::Math::Polynomial P = ROOT::Math::Polynomial(coeffs.size()-1);
                                     P.SetParameters(&coeffs[0]);

              return P;
       }

       inline std::vector<std::complex<double>> polysolve(ROOT::Math::Polynomial &P) { return P.FindRoots(); }
       inline std::vector<std::complex<double>> polysolve(const std::vector<double> &coeffs) 
       { 
              ROOT::Math::Polynomial P = OMU::polynomial(coeffs);
              return OMU::polysolve(P);
       }

       template <typename T>
       inline std::vector<std::complex<T>> polysolve(const TMatrixT<T> &M) 
       {
              std::vector<std::complex<T>> eigenValues = OMU::EigenValues(M);
              std::vector<std::complex<T>> v = {1};
              for(int i = 0, N = M.GetNrows(); i < N; i++) {

                     std::vector<std::complex<T>> w = {1, -eigenValues[i]};
                     v = OMU::Convolve(v, w);
              }

              return v;
       }

       inline TComplex polz(const Double_t *x, const Double_t *par)
       {
              TComplex polz = 0;
              for(int i = 0, i0 = 1, N = par[0]; i < N; i++) {
                     polz += par[i+i0] * TComplex::Power(TComplex(x[0],x[1]), i);
              }

              return polz;
       }

       inline TComplex tfuncc(const Double_t *xx, const Double_t *par)
       {
              int Bn = par[0];
              int An = par[1];
              int i0 = 2; // parameter offset

              std::vector<double> par1(Bn+i0-1);
              par1[0] = Bn;
              for(int i = 0; i < Bn; i++)
                     par1[i+i0-1] = par[i+i0];

              std::vector<double> par2(An+i0-1);
              par2[0] = An;
              for(int i = 0; i < An; i++)
                     par2[i+i0-1] = par[i+i0+Bn];

              return polz(xx, &par1[0]) / polz(xx, &par2[0]);
       }

       inline TComplex tfuncc(const TComplex x, const Double_t *par) 
       { 
              std::vector<double> xx = {x.Re(), x.Im()};
              return tfuncc(&xx[0], par);
       }

       inline Double_t tfunc(const Double_t *xx, const Double_t *par)
       {
              return TComplex::Abs(tfuncc(xx,par));
       }

       
       template <typename T>
       inline _ZPK<T> zpk(kfr::biquad_params<T> params) { return zpk(tf(params)); }
       template <typename T>
       inline _SOS<T> sos(kfr::biquad_params<T> params) { return sos(tf(params)); }
       template <typename T>
       inline _SS<T>   ss(kfr::biquad_params<T> params) { return ss(tf(params)); }
       template <typename T>
       inline _TF<T>   tf(kfr::biquad_params<T> params)
       {
              _TF<T> tf;
                     tf.first  = { params.b2, params.b1, params.b0 };
                     tf.second = { params.a2, params.a1, params.a0 };

              return tf;
       }
       
       template <typename T>
       inline _TF<T>  tf(_SOS<T>  sos)
       {
              _TF<T> tf;

              int n_sections = sos.size();
              tf.first.resize(1, 1);
              tf.second.resize(1, 1);

              for(int i = 0; i < n_sections; i++) {

                     std::vector<T> b;
                            b.push_back(sos[i].b2);
                            b.push_back(sos[i].b1);
                            b.push_back(sos[i].b0);
              
                     std::vector<T> a;
                            a.push_back(sos[i].a2);
                            a.push_back(sos[i].a1);
                            a.push_back(sos[i].a0);

                     tf.first  = polymulti(tf.first, b);
                     tf.second = polymulti(tf.second, a);
              }

              return tf;
       }

       template <typename T>
       inline _TF<T> tf(_TF<T> tf)
       {
              if(!tf.second.size() || !tf.first.size()) return tf;
              tf.first  = OMU::RemoveTrailingZeros(tf.first);
              tf.second = OMU::RemoveTrailingZeros(tf.second);

              for(int i = 0, I = tf.first.size(); i < I; i++)
                     tf.first[i] /= tf.second[tf.second.size()-1];
              for(int i = 0, I = tf.second.size(); i < I; i++)
                     tf.second[i] /= tf.second[tf.second.size()-1];

              return tf;
       }

       template <typename T>
       inline _TF<T> tf(_ZPK<T> zpk)
       {
              _TF<T> tf;
                     tf.first = Multiply(Real(polycoeff(stdvector(zpk.z))), zpk.k);
                     tf.second = Real(polycoeff(stdvector(zpk.p)));

              // TF definition : to be checked
              std::reverse(tf.first.begin(), tf.first.end());
              std::reverse(tf.second.begin(), tf.second.end());

              return tf;
       }

       template <typename T>
       inline _TF<T> tf(_SS<T> ss)
       {
              TMatrixT<T> &A = std::get<0>(ss);
              TMatrixT<T> &B = std::get<1>(ss);
              TMatrixT<T> &C = std::get<2>(ss);
              TMatrixT<T> &D = std::get<3>(ss);

              _TF<T> tf;
              if(!A.GetNrows()) return tf;

              if(A.GetNcols() != A.GetNrows()) throw std::invalid_argument("Unexpected dimension provided for A matrix");
              if(A.GetNcols() != B.GetNrows()) throw std::invalid_argument("Unexpected dimension provided for B matrix");
              if(A.GetNcols() != C.GetNcols()) throw std::invalid_argument("Unexpected dimension provided for C matrix");
              if(B.GetNcols() != D.GetNcols()) throw std::invalid_argument("Unexpected col dimension provided for D matrix");
              if(C.GetNrows() != D.GetNrows()) throw std::invalid_argument("Unexpected row dimension provided for D matrix");

              int N = A.GetNrows();

              tf.second.resize(N+1,0);
              tf.second = OMU::Real(OMU::polysolve(A));
              tf.second = OMU::RemoveLeadingZeros(tf.second);

              TVectorT<T> V1 = rootvector(tf.second);
              TVectorT<T> V2 = rootvector(OMU::Real(OMU::polysolve(A - B * C)));
              TVectorT<T> V3 = V2 + (D(0,0) - 1) * V1;

              tf.first.resize(N+1,0);
              tf.first = stdvector(V2 + (D(0,0) - 1) * V1);
              tf.first  = OMU::RemoveLeadingZeros(tf.first);

              std::reverse(tf.first.begin(), tf.first.end());
              std::reverse(tf.second.begin(), tf.second.end());
              
              return tf;
       }

       template <typename T>
       inline std::vector<_TF<T>> tf(const std::vector<_ZPK<T>> &zpk) { return Transform(zpk, [](const _ZPK<T> &x) { return tf(x); }); }
       template <typename T>
       inline std::vector<_TF<T>> tf(const std::vector<_SOS<T>> &sos) { return Transform(sos, [](const _SOS<T> &x) { return tf(x); }); }
       template <typename T>
       inline std::vector<_TF<T>> tf(const std::vector<_SS<T>>   &ss) { return Transform(ss, [](const _SS<T>   &x) { return tf(x); }); }
       template <typename T>
       inline std::vector<_TF<T>> tf(const std::vector<_TF<T>>   &tf) { return Transform(tf, [](const _TF<T>   &x) { return OMU::tf(x); }); }

       template <typename T>
       inline _SS<T> ss(const _TF<T> &tf) 
       {
              _SS<T> ss;
              TMatrixT<T> &A = std::get<0>(ss);
              TMatrixT<T> &B = std::get<1>(ss);
              TMatrixT<T> &C = std::get<2>(ss);
              TMatrixT<T> &D = std::get<3>(ss);

              int N = tf.first.size() - 1;
              int M = tf.second.size() - 1;
              if(N < 0 || M < 0 || M < N)
                     return ss;

              A.ResizeTo(M, M);
              A = OMU::Eye<T>(M, -1);
              for (int i = 0; i < M; i++) {
                     A(0, i) = -tf.second[M - (i + 1)] / tf.second[M];
              }

              B.ResizeTo(M, 1);
              B = OMU::Identity<T>(M, 1);

              C.ResizeTo(1, M);
              for (int i = 0; i < M; i++) {

                     T K = (M != N) ? 0 : tf.first[tf.first.size()-1];
                     C(0, M-1-i) = (i <= N ? tf.first[i] - K*tf.second[i] : 0);
              }
              
              D.ResizeTo(1,1);
              D(0, 0) = N < M ? 0 : tf.first[tf.first.size()-1];
              return ss; 
       }

       template <typename T>
       inline _SS<T> ss(_ZPK<T> zpk) { return ss(tf(zpk)); }
       template <typename T>
       inline _SS<T> ss(_SOS<T> sos) { return ss(tf(sos)); }

       template <typename T>
       inline _ZPK<T> zpk(_TF<T> tf)
       {
              tf = OMU::tf(tf);
              std::vector<T> B = tf.first;
              std::vector<T> A = tf.second;

              ZPK _zpk;
              if(!A.size() || !B.size()) return _zpk;
              
              _zpk.k = B[0];
              for(int i = 0, N = B.size(); i < N; i++)
                     B[i] /= B[B.size()-1];

              ROOT::Math::Polynomial zeros = ROOT::Math::Polynomial(B.size()-1);
                                     zeros.SetParameters(&B[0]);
              
              ROOT::Math::Polynomial poles = ROOT::Math::Polynomial(A.size()-1);
                                     poles.SetParameters(&A[0]);

              _zpk.z = kfrvector(zeros.FindRoots());
              _zpk.p = kfrvector(poles.FindRoots());

              return _zpk;
       }

       template <typename T>
       inline _ZPK<T> zpk(const _SOS<T> &sos)
       {
              int n_sections = sos.size();

              _TF<T>  tf;
              _ZPK<T> zpk;
                      zpk.z.resize(2*n_sections);
                      zpk.p.resize(2*n_sections);
                      zpk.k = 1;

              for(int i = 0; i < n_sections; i++) {

                     _TF<T> _tf = OMU::tf(sos[i]);
                     _ZPK<T> _zpk = OMU::zpk(_tf);

                     for(int j = 0, J = _zpk.z.size(); j < J; j++)
                            zpk.z[j+i*n_sections] = _zpk.z[j];
                     for(int j = 0, J = _zpk.p.size(); j < J; j++)
                            zpk.p[j+i*n_sections] = _zpk.p[j];

                     zpk.k *= _zpk.k;
              }

              return zpk;
       }
       template <typename T>
       inline _ZPK<T> zpk(const _SS<T> &ss) { return zpk(tf(ss)); }

       template <typename T>
       inline _SOS<T> sos(_ZPK<T> zpk) { return kfr::to_sos(zpk);     }
       template <typename T>
       inline _SOS<T> sos(_TF<T>  tf)  { return sos(zpk(tf)); }
       template <typename T>
       inline _SOS<T> sos(_SS<T>  ss)  { return sos(zpk(ss)); }

       template <typename T>
       inline std::vector<_ZPK<T>> zpk(const std::vector<_TF<T>>   &tf) { return Transform(tf,  [](const _TF<T>  &x) { return zpk(x); }); }
       template <typename T>
       inline std::vector<_ZPK<T>> zpk(const std::vector<_SOS<T>> &sos) { return Transform(sos, [](const _SOS<T> &x) { return zpk(x); }); }
       template <typename T>
       inline std::vector<_ZPK<T>> zpk(const std::vector<_SS<T>>   &ss) { return Transform(ss,  [](const _SS<T>  &x) { return zpk(x); }); }

       template <typename T>
       inline _TF<T>  tf (const std::vector<T> &B, const std::vector<T> &A) { return  tf(make_pair(B,A));  }
       template <typename T>
       inline _ZPK<T> zpk(const std::vector<T> &B, const std::vector<T> &A) { return zpk(make_pair(B,A)); }
       template <typename T>
       inline _SOS<T> sos(const std::vector<T> &B, const std::vector<T> &A) { return sos(make_pair(B,A)); }
       template <typename T>
       inline _SS<T>  ss (const std::vector<T> &B, const std::vector<T> &A) { return  ss(make_pair(B,A)); }
       
       template <typename T>
       inline _TF<T>  tf (const TMatrixT<T> &A, const TMatrixT<T> &B, const TMatrixT<T> &C, const TMatrixT<T> &D) { return  tf(make_tuple(A,B,C,D));  }
       template <typename T>
       inline _ZPK<T> zpk(const TMatrixT<T> &A, const TMatrixT<T> &B, const TMatrixT<T> &C, const TMatrixT<T> &D) { return zpk(make_tuple(A,B,C,D)); }
       template <typename T>
       inline _SOS<T> sos(const TMatrixT<T> &A, const TMatrixT<T> &B, const TMatrixT<T> &C, const TMatrixT<T> &D) { return sos(make_tuple(A,B,C,D)); }
       template <typename T>
       inline _SS<T>  ss (const TMatrixT<T> &A, const TMatrixT<T> &B, const TMatrixT<T> &C, const TMatrixT<T> &D) { return     make_tuple(A,B,C,D); }

       inline TF  tf (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return  tf(polvector(B),polvector(A)); }       
       inline ZPK zpk(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return zpk(polvector(B),polvector(A)); }
       inline SOS sos(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return sos(polvector(B),polvector(A)); }
       inline SS  ss (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return  ss(polvector(B),polvector(A)); }

       template <typename T>
       inline std::vector<_SOS<T>> sos(const std::vector<_SS<T>>   &ss) { return Transform(ss, [](const _SS<T>   &x) { return sos(x); }); }
       template <typename T>
       inline std::vector<_SOS<T>> sos(const std::vector<_ZPK<T>> &zpk) { return Transform(zpk, [](const _ZPK<T> &x) { return sos(x); }); }
       template <typename T>
       inline std::vector<_SOS<T>> sos(const std::vector<_TF<T>>   &tf) { return Transform(tf, [](const _TF<T>   &x) { return sos(x); }); }
}

#endif
