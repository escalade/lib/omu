/**
 * *********************************************
 *
 * \file TFileH5.h
 * \brief Header of the TFileH5 class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TFileH5
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#ifndef TFileH5_H
#define TFileH5_H

#include <Riostream.h>
#include <TObject.h>
#include <TString.h>
#include <TRandom.h>
#include <TFormula.h>

#include <TParameter.h>

#include "H5Cpp.h"

#include "UIUC/MathUtils.h"
#include "UIUC/TFactory.h"
#include "UIUC/TFileReader.h"
#include "UIUC/HandboxMsg.h"

#include <exception>

// /!\ SEQUENTIAL HDF5 IS UNTESTED
// I didn't have sequential files..
// So please.. send me an email (marco.meyer@cern.ch) with a sample if this breaks..

namespace OMU
{
        class TFileH5: public TObject
        {
                protected:

                        H5::H5File *h5 = NULL;

                        const char *fname;
                        unsigned int flags;
                        int verbosity;
                        Bool_t sequential;

                        void ReversePtr(hsize_t* dim, int rank);
                        void Payload(H5::Group *group, bool bWrite = false);

                        char GetType(const H5::AbstractDs &d);
                        TObject *CreateScalar(TString objName, const H5::DataSet &dataSet);
                        TObject *CreateAttribute(TString objName, const H5::Attribute *attr);
                        TObject *CreateObject(TString objName, const H5::DataSet &dataSet);

                public:

                       ~TFileH5() { Close(); };
                        TFileH5(const char *fname, Option_t *option = "", Bool_t _sequential = false, int _verbosity = 0);

                        static TFileH5 *Open(const char *fname, Option_t *option = "", Bool_t sequential = false, int verbosity = 0) { return new TFileH5(fname, option, sequential, verbosity); }

                        TObject *Read(TString path);

                        void GetListOfKeys();

                        void Print(Option_t *option = "");
                        void Save(Option_t *option = "CREATE", TString = "");
                        void Close(Option_t *option="");

                ClassDef(OMU::TFileH5,1);
        };
}
#endif
