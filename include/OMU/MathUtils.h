#ifndef OMU_MathUtils_H
#define OMU_MathUtils_H

#include <tuple>

#include <Riostream.h>
#include <vector>
#include <algorithm> //std::for_each

#include <UIUC/HandboxMsg.h>
#include <UIUC/HandboxUsage.h>
#include <UIUC/MathUtils.h>

#include <numeric>
#include <cstdio>
#include <string>
#include <sstream>
#include <iostream>
#include <execinfo.h>
#include <stdio.h>
#include <unistd.h>

#include <time.h>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>

#include <TComplex.h>
#include <TVectorT.h>
#include <TMatrixT.h>
#include <gsl/gsl_linalg.h>

namespace OMU {


        // https://www.timeanddate.com/time/leapseconds.html
        // https://aviation.stackexchange.com/questions/90839/what-are-satellite-time-gps-time-and-utc-time
        const static int unixTimeGPS = 315964800;
        const static std::vector<int> leapSeconds = {
                46828800,   // 1981-06-30 23:59:60 UTC
                78364801,   // 1982-06-30 23:59:60 UTC
                109900802,  // 1983-06-30 23:59:60 UTC
                173059203,  // 1985-06-30 23:59:60 UTC
                252028804,  // 1987-12-31 23:59:60 UTC
                315187205,  // 1989-12-31 23:59:60 UTC
                346723206,  // 1990-12-31 23:59:60 UTC
                393984007,  // 1992-06-30 23:59:60 UTC
                425520008,  // 1993-06-30 23:59:60 UTC
                457056009,  // 1994-06-30 23:59:60 UTC
                504489610,  // 1995-12-31 23:59:60 UTC
                551750411,  // 1997-06-30 23:59:60 UTC
                599184012,  // 1998-12-31 23:59:60 UTC
                820108813,  // 2005-12-31 23:59:60 UTC
                914803214,  // 2008-12-31 23:59:60 UTC
                1025136015, // 2012-06-30 23:59:60 UTC
                1119744016, // 2015-06-30 23:59:60 UTC
                1167264017  // 2016-12-31 23:59:60 UTC
                        // ...
        };

        inline static TString UTC(int GTimeS, int GTimeN, const char *format = "%Y-%m-%d %H:%M:%S UTC")
        {
                int isLeapSecond = false;
                int nLeapSeconds = 0;
                for(int i = 0, N = OMU::leapSeconds.size(); i < N; i++) {
                        
                        if(GTimeS > OMU::leapSeconds[i]) nLeapSeconds++;
                        isLeapSecond |= (GTimeS == OMU::leapSeconds[i]);
                }

                struct timespec ts;
                ts.tv_sec = GTimeS + OMU::unixTimeGPS - nLeapSeconds - (isLeapSecond ? 1 : 0);
                ts.tv_nsec = GTimeN;

                struct tm stm;
                gmtime_r(&ts.tv_sec, &stm);
                if(isLeapSecond) stm.tm_sec = 60;
                
                char utc[128];
                strftime(utc, sizeof(utc), format, &stm);

                return TString(utc);
        }

        inline static TString UTC(double GPS, const char *format = "%Y-%m-%d %H:%M:%S UTC")
        {
                int GTimeS = (int) GPS;
                int GTimeN = 1e9 * (GPS - GTimeS);
                
                return UTC(GTimeS, GTimeS, format);
        }

        inline static double GPS(const char *utc, const char *format = "%Y-%m-%d %H:%M:%S")
        {
                std::tm tm;
                const char* result = strptime(utc, format, &tm);
                
                char buff[128];
                if (result == NULL || *result != '\0' || strftime (buff, sizeof(buff), format, &tm) == 0) {
                        
                        std::cerr << "Failed to parse \"" << utc << "\" using format \"" << format << "\"" << std::endl;
                        return NAN;
                }
                
                std::time_t timestamp = timegm(&tm); // Might be some ambiguity 

                double GTimeS = timestamp - unixTimeGPS;
                int nLeapSeconds = 0;

                for(int i = 0, N = OMU::leapSeconds.size(); i < N; i++) {

                        if(GTimeS+nLeapSeconds > OMU::leapSeconds[i]) nLeapSeconds++;
                        if(UIUC::EpsilonEqualTo(GTimeS+nLeapSeconds, OMU::leapSeconds[i]) && UTC(GTimeS + nLeapSeconds + 1, format).EqualTo(utc)) nLeapSeconds++;
                }

                return GTimeS + nLeapSeconds;
        }

        inline static double GPS(int GTimeS, int GTimeN) { return GTimeS + GTimeN/1e9; }
        inline static double GPS() { return OMU::GPS(UIUC::HandboxUsage::GetCurrentTime("%Y-%m-%d %H:%M:%S")); }
        
        template <class T, class UnaryOperator>
        auto Transform(const T& input, UnaryOperator op)
        {
                std::vector<std::decay_t<decltype(op(*input.begin()))>> output;
                std::transform(input.begin(), input.end(),
                                std::back_inserter(output), op);

                return output;
        }

        template<typename Element>                    
        inline TMatrixT<Element> Exp(const TMatrixT<Element>& a) {

                const size_t Nx = a.GetNrows();
                gsl_matrix_const_view va = gsl_matrix_const_view_array(a.GetMatrixArray(), Nx, Nx);

                TMatrixT<Element> b(Nx, Nx);
                gsl_matrix_view vb = gsl_matrix_view_array(b.GetMatrixArray(), Nx, Nx);

                gsl_linalg_exponential_ss((const gsl_matrix*)&va, (gsl_matrix*)&vb, 0);

                return b;
        }

        template<typename Element>
        inline TVectorT<Element> Zeros(const TVectorT<Element> &V)
        {
                TVectorT<Element> Vc(V.GetNoElements());
                for(int i = 0, N = V.GetNoElements(); i < N; i++) {
                        Vc(i) = 0;
                }

                return Vc;
        }

        template<typename Element>
        inline TMatrixT<Element> Zeros(TMatrixT<Element> &M)
        {
                for(int i = 0, I = M.GetNrows(); i < I; i++) {
                        for(int j = 0, J = M.GetNcols(); j < J; j++) {
                                M(i,j) = 0;
                        }
                }

                return M;
        }

        template<typename Element>                    
        inline TMatrixT<Element> Eye(int n, int offset = 0)
        {
                TMatrixT<Element> M(n,n); 
                for(int i = 0, I = TMath::Max(0, M.GetNrows()-TMath::Abs(offset)); i < I; i++) {

                        if(offset < 0) M(i + TMath::Abs(offset),i) = 1;
                        else if(offset > 0) M(i,i + TMath::Abs(offset)) = 1;
                        else M(i,i) = 1;
                }

                return M;
        }

        template<typename Element>
        inline TMatrixT<Element> Identity(int n, int m) { 
                
                TMatrixT<Element> M(n,m);
                for(int i = 0; i < TMath::Min(n,m); i++)
                        M(i,i) = 1;

                return M;
        }

        template<typename Element>
        inline TMatrixT<Element> Identity(int n) { return OMU::Identity<Element>(n,n); }

        template<typename T>
        std::vector<T> Flatten(std::vector<std::vector<T>> const &vec)
        {
                std::vector<T> flattened;
                for (auto const &v: vec)
                        flattened.insert(flattened.end(), v.begin(), v.end());

                return flattened;
        }

        template<typename T>
        int Closest(std::vector<T> v, T value)
        {
                for (int i = 0, N = v.size(); i < N; i++)
                        v[i] = TMath::Abs(v[i] - value);

                return min_element(v.begin(), v.end()) - v.begin();
        }

        template<typename T>
        std::vector<T> Remove(std::vector<T> v, const T &element)
        {
                v.erase(std::remove_if(v.begin(), v.end(), [element](T value) { return element == value; }), v.end());
                return v;
        }

        template<typename T>
        std::vector<T> RemoveLeading(std::vector<T> v, const T &element)
        {
                for (auto it = v.begin(); it != v.end(); ) {

                        if (!UIUC::EpsilonEqualTo(*it, element)) break;
                        it = v.erase(it);
                }

                return v;
        }

        template<typename T>
        std::vector<T> RemoveLeadingZeros(std::vector<T> v) { return OMU::RemoveLeading(v, 0.0); }

        template<typename T>
        std::vector<T> RemoveTrailing(std::vector<T> v, const T &element)
        {
                while (!v.empty()) {

                        if (!UIUC::EpsilonEqualTo(v.back(), element)) break;
                        v.pop_back();
                }

                return v;
        }

        template<typename T>
        std::vector<T> RemoveTrailingZeros(std::vector<T> v) { return OMU::RemoveTrailing(v, 0.0); }

        template<typename T>
        std::vector<std::vector<T>> Inflatten(std::vector<T> const &vec, int length)
        {
                int N = vec.length();
                if (N % length != 0)
                        throw std::invalid_argument("Length must be a multiple of the total length of the flatten vector");

                std::vector<std::vector<T>> inflattened;
                for (int i = 0, I = N/length; i < I; i++)
                        inflattened.push_back(std::vector<T>(vec.begin()+i*length, vec.begin()+(i+1)*length));

                return inflattened;
        }

        template <typename T, typename Ti>
        inline void Reorder(std::vector<T>& v, std::vector<Ti> indexes)  
        {   
                assert(v.size() == indexes.size());

                // for all elements to put in place
                for( int i = 0; i < v.size() - 1; ++i )
                { 
                        // while the element i is not yet in place 
                        while( i != indexes[i] )
                        {
                                // swap it with the element at its final place
                                Ti alt = indexes[i];
                                std::swap( v[i], v[alt] );
                                std::swap( indexes[i], indexes[alt] );
                        }
                }
        }

        template <typename T>
        inline T Q1( std::vector<T> v) 
        {
                std::sort(v.begin(), v.end()); 
                int i = 1/4. * (v.size() + 1); 
                return v[i];
        }
        template <typename T>
        inline T Q2( std::vector<T> v) 
        { 
                std::sort(v.begin(), v.end()); 
                int i = 2/4. * (v.size() + 1); 
                return v[i];
        }
        template <typename T>
        inline T Q3( std::vector<T> v) 
        {
                std::sort(v.begin(), v.end()); 
                int i = 3/4. * (v.size() + 1);
                return v[i]; 
        }

        template <typename T>
        inline T IQR( std::vector<T> v) 
        {
                std::sort(v.begin(), v.end()); 

                int i1 = 1/4. * (v.size() + 1); 
                T Q1 = v[i1];

                int i3 = 3/4. * (v.size() + 1); 
                T Q3 = v[i3];

                return Q3 - Q1;
        }
}

#endif
