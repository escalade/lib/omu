/**
 * *********************************************
 *
 * \file TConfigParser.h
 * \brief Header of the TConfigParser class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TConfigParser
 * \brief Advanced signal processing class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#ifndef TConfigParser_H
#define TConfigParser_H

#include <Riostream.h>

#include <TObject.h>
#include <TUrl.h>
#include <TSystem.h>
#include <TDOMParser.h>
#include <TXMLDocument.h>
#include <TXMLNode.h>
#include <TXMLAttr.h>

#include <UIUC/HandboxMsg.h>
#include <UIUC/TFileReader.h>
#include <UIUC/MathUtils.h>
#include <OMU/MathUtils.h>

#include "Config.h"
#include <math.h>

#ifdef CURL_FOUND
#include <curl/curl.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#ifndef WIN32
#include <unistd.h>
#endif

#include <nlohmann/json.hpp>
#include <sys/types.h>
#include <sys/stat.h>

#include <nlohmann/json.hpp>
using TJsonFile    = nlohmann::json;
using TJsonIter    = nlohmann::detail::iter_impl<nlohmann::json>;

namespace OMU {

        inline void Print(TJsonFile json, bool bValues = false)
        {
                if(json.size() == 0) {
                        std::cout << "-- Empty json" << std::endl;
                        return;
                }

                std::cout << "-- JSON contains: ";
                std::vector<TString> jsonKeys;
                for (auto it = json.begin(); it != json.end(); ++it){
                        jsonKeys.push_back(it.key().c_str());
                }


                if(bValues) std::cout << json << std::endl;
                else {
                        
                        std::vector<TString> jsonKeys;
                        for (auto it = json.begin(); it != json.end(); ++it)
                                jsonKeys.push_back(it.key().c_str());

                        std::cout << "{`" << UIUC::Implode("`: `[..]`, `", jsonKeys) << "`: `[..]`}" << std::endl; 
                }
        }

        class TConfigParser: public TObject
        {
                private:

                bool bNoCache = false;
                int httpResponseCode = 0;

                TUrl url;
                int lifetime;
                TString output;
                
                // Transient variables
                TString contents;
                TString contentType;
                
                TString formatOutput(TString output) {

                    struct stat info;

                    if (output.Length() == 0) output = TString(this->url.GetUrl()).ReplaceAll('/', '_').ReplaceAll(':', '_');
                    else if( stat( output, &info ) == 0 || info.st_mode & S_IFDIR )
                        output = (TString) output + "/" + gSystem->BaseName(url.GetFile());

                    if (output[0] != '/') {

                        TString tmp  = "/tmp/api.";
                                tmp += output.Data();

                        output = tmp;
                    }

                    return output;
                }

                public:

                static char    _curl_download_chr;
                static int     _curl_download_width;
                static TString _curl_download_prefix;
                static TString _curl_download_suffix;
                
                static TConfigParser *Open(const char *url, int lifetime = -1, const char *output = "")
                {   
                    return new TConfigParser(url, lifetime, output);
                }

                TConfigParser(const char *url, int lifetime = -1, const char *output = "")
                {
                    this->url = TUrl(url);
                    this->lifetime = lifetime;
                    this->output = this->formatOutput(output);
                    this->bNoCache = false;
                }

                TConfigParser(TUrl url, int lifetime = -1, const char *output = "")
                {
                    this->url = url;
                    this->lifetime = lifetime;
                    this->output = this->formatOutput(output);
                    this->bNoCache = false;
                };

                ~TConfigParser() {};

                void DisableCache() { bNoCache = true; };
                void EnableCache() { bNoCache = false; };

                TString GetCache();
                TString GetCacheFile();

                void ClearCache();
                bool IsLocalFile();
                bool IsCached();

                inline TString GetContents() { return this->contents; }

                TJsonFile GetJSON();
                void PrintJSON();

                // TJsonFile GetYAML();
                // void PrintYAML();

                TXMLDocument *GetXML(bool checkValidation = true);
                void PrintXML(bool checkValidation = true);
                static void PrintXMLNode(TXMLNode *node, int level = 0);

                inline static OMU::TConfigParser *Fetch(const char *url, int lifetime = -1, TString output = "", bool showProgress = true) { return OMU::TConfigParser::Fetch(TUrl(url), lifetime, output, showProgress); }
                inline static OMU::TConfigParser *Fetch(TString     url, int lifetime = -1, TString output = "", bool showProgress = true) { return OMU::TConfigParser::Fetch(url.Data(), lifetime, output, showProgress); }
                inline static OMU::TConfigParser *Fetch(TUrl        url, int lifetime = -1, TString output = "", bool showProgress = true) { return (new TConfigParser(url, lifetime, output))->Fetch(showProgress); }
                              OMU::TConfigParser *Fetch(bool showProgress = false);

                inline static TString Download(const char *url, bool showProgress = true, int lifetime = -1, TString output = "") { return OMU::TConfigParser::Download(TUrl(url) , showProgress, lifetime, output); }
                inline static TString Download(TString     url, bool showProgress = true, int lifetime = -1, TString output = "") { return OMU::TConfigParser::Download(url.Data(), showProgress, lifetime, output); }
                inline static TString Download(TUrl        url, bool showProgress = true, int lifetime = -1, TString output = "") { return (new TConfigParser(url, lifetime, output))->Download(showProgress); }
                              TString Download(bool showProgress = true) { return this->Fetch(showProgress)->output; }

                TUrl GetUrl();
                inline OMU::TConfigParser *SetUrl(TString url) { return this->SetUrl(TUrl(url)); };
                OMU::TConfigParser *SetUrl(TUrl);

                TString GetOutput();
                OMU::TConfigParser *SetOutput(TString);

                int GetLifetime();
                OMU::TConfigParser *SetLifetime(int);

                ClassDef(OMU::TConfigParser,1);
        };
}
#endif
