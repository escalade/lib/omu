# Add path to module list
list(PREPEND CMAKE_MODULE_PATH 
    ${CMAKE_CURRENT_LIST_DIR}/../cmake/Inc/ 
    ${CMAKE_CURRENT_LIST_DIR}/../cmake/Req/ 
    ${CMAKE_CURRENT_LIST_DIR}/../cmake/Modules/
)

# Include custom standard package
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
  NAMES hdf5 hdf5_cpp hdf5_hl hdf5_hl_cpp 
  HEADERS "hdf5.h"
  PATHS $ENV{HDF5}
)