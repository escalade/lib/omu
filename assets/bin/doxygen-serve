#!/usr/bin/env python

import os
import sys
import subprocess
import re
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import threading
import argparse

class Color:
    """Terminal color codes."""
    RED = '\033[91m'
    ORANGE = '\033[33m'
    GREEN = '\033[92m'
    ENDC = '\033[0m'

class Doxygen:
    def __init__(self, doxyfile="Doxyfile"):
        self.doxyfile = self.find_doxyfile(doxyfile)
        self.input_files, self.output_dir = self.extract()

    def find_doxyfile(self, doxyfile):
        # Check if the specified Doxyfile exists
        if os.path.isfile(doxyfile):
            return os.path.realpath(doxyfile)

        # Check the current directory for the default Doxyfile
        default_doxyfile = os.path.realpath("Doxyfile")
        if os.path.isfile(default_doxyfile):
            return default_doxyfile

        print(f"{Color.RED}Doxyfile not found!{Color.ENDC}")
        sys.exit(1)

    def generate(self):
        """Regenerate Doxygen documentation."""
        try: 
            doxyfile_dir = os.path.dirname(os.path.abspath(self.doxyfile))
            os.chdir(doxyfile_dir)  # Change directory to the Doxygen configuration file location        

            # Extracting the output directory's path
            output_directory = os.path.abspath(self.output_dir)

            # Creating the output directory if it does not exist
            os.makedirs(output_directory, exist_ok=True)
            os.chdir(doxyfile_dir)

            subprocess.run(["doxygen", "-u"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            subprocess.run(["doxygen", self.doxyfile], stdout=subprocess.DEVNULL)

            return True
        
        except Exception as e:
            print(f"{Color.RED}Error generating Doxygen documentation: {e}{Color.ENDC}")
            return False

    def extract(self):
        """Extracts INPUT files and HTML_OUTPUT from the Doxygen configuration file."""
        input_files = []
        output_dir = ""
        input_pattern = re.compile(r'^\s*INPUT(\s|\+)*=')
        output_pattern = re.compile(r'^\s*HTML_OUTPUT(\s)*=')
        continuation_pattern = re.compile(r'\\\s*$')  # Matches lines ending with '\'

        doxyfile_dir = os.path.dirname(os.path.abspath(self.doxyfile))
        os.chdir(doxyfile_dir)  # Change directory to the Doxygen configuration file location
            
        with open(self.doxyfile, 'r') as file:
            in_input_section = False

            for line in file:
                if input_pattern.match(line):
                    in_input_section = True
                    line = line.split("=")[1].strip()
                    input_files.extend(line.split())

                elif in_input_section:
                    line = line.strip()
                    if continuation_pattern.search(line):
                        input_files.extend(line.split('\\'))
                    else:
                        in_input_section = False
                        input_files.extend(line.split())

                if output_pattern.match(line):
                    output_dir = line.split("=")[1].strip()

        input_files = [os.path.abspath(path.replace('\\', '').strip()) for path in input_files if os.path.exists(path.replace('\\', '').strip())]
        return list(set(input_files)), os.path.abspath(output_dir)

    def print_info(self):
        """Print details of the extracted INPUT information and HTML_OUTPUT."""
        print(f"{Color.GREEN}Doxyfile configuration used...{Color.ENDC} {self.doxyfile}")
        print(f"{Color.GREEN}Monitored file lists:{Color.ENDC}")
        for index, file_path in enumerate(self.input_files, start=1):
            print(f"\tFile #{index}: {file_path}")
        print(f"{Color.GREEN}Output directory:{Color.ENDC} {self.output_dir}")

    def html_enabled(self):
        """Check if HTML generation is enabled in the Doxygen configuration file."""
        html_enabled = False
        with open(self.doxyfile, 'r') as file:
            for line in file:
                if line.startswith("GENERATE_HTML"):
                    if "YES" in line:
                        html_enabled = True
                    break
        return html_enabled

class DoxygenServer:
    def __init__(self, doxygen, hostname, port):
        self.doxygen = doxygen
        self.hostname = hostname
        self.port = port

    def serve(self):
        if self.doxygen.html_enabled():
            os.chdir(self.doxygen.output_dir)
        else:
            print(f"{Color.RED}HTML generation is not enabled in the Doxygen configuration. Serving disabled.{Color.ENDC}")

        """Serve the HTML output using Python's built-in HTTP server."""
        subprocess.run(["python", "-m", "http.server", str(self.port)])
        
    def watch(self):
        """Monitor the files specified in INPUT for changes."""
        event_handler = FileSystemEventHandler()

        def on_any_event(event):
            # Get the current date/time
            current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

            # Format the event type with the first letter capitalized
            event_type = event.event_type.capitalize()

            # Print the event type and the path of the file that triggered the event in orange color
            print(f'\033[33m{current_time} - {event_type} - {event.src_path}\033[0m')

            # Trigger Doxygen upon any modification of the files specified in the INPUT variable
            if event.event_type == 'modified':
                self.doxygen.generate()
                return

        event_handler.on_any_event = on_any_event

        # Get the directory path of the Doxyfile
        doxyfile_directory = os.path.dirname(os.path.abspath(self.doxygen.doxyfile))
        
        observer = Observer()
        # Add each file specified in INPUT for monitoring
        for file_path in self.doxygen.input_files:
            observer.schedule(event_handler, os.path.join(doxyfile_directory, file_path), recursive=False)

        if self.doxygen.generate():
            print(f"{Color.GREEN}Initial doxygen documentation generated successfully...{Color.ENDC} ")

        print(f"Now watching for file changes.")
        print(f"\n{Color.ORANGE}Press Ctrl+C to stop monitoring.{Color.ENDC}\n")
        
        try:
            observer.start()
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            print(f"{Color.ORANGE}Stop using Ctrl+C{Color.ENDC}")
            observer.stop()
            observer.join()

    def start(self):
        # Run serving and watching in separate threads
        if self.doxygen.html_enabled():
            serve_thread = threading.Thread(target=self.serve)
            serve_thread.start()
    
        watch_thread = threading.Thread(target=self.watch)
        watch_thread.start()

        if self.doxygen.html_enabled():
            serve_thread.join()
        
        watch_thread.join()

class DoxygenUsage:
    @staticmethod
    def extract_params(args):
        """Extract parameters from command-line arguments."""
        parser = argparse.ArgumentParser(description="Monitor Doxygen configuration and regenerate documentation on file changes.")
        parser.add_argument("doxyfile_or_dir", nargs='?', metavar="Doxyfile (or directory containing Doxyfile)", type=str, default="Doxyfile",
                            help="Path to the Doxygen configuration file or directory containing the file")
        parser.add_argument("-H", "--hostname", dest="hostname", metavar="hostname", type=str, default="localhost",
                            help="Hostname to serve the HTML output (default is 'localhost')")
        parser.add_argument("-p", "--port", dest="port", metavar="port", type=int, default=8000,
                            help="Port number to serve the HTML output (default is '8000')")
        
        parsed_args = parser.parse_args(args)

        doxyfile_or_directory = parsed_args.doxyfile_or_dir
        if os.path.isfile(doxyfile_or_directory):  # Check if it's a file
            return os.path.realpath(doxyfile_or_directory), parsed_args.hostname, parsed_args.port
        elif os.path.isdir(doxyfile_or_directory):  # Check if it's a directory
            # Search for Doxyfile inside the directory
            doxyfile = os.path.join(doxyfile_or_directory, "Doxyfile")
            if os.path.isfile(doxyfile):
                return os.path.realpath(doxyfile), parsed_args.hostname, parsed_args.port

        print(f"{Color.RED}Doxyfile not found in the specified directory!{Color.ENDC}")
        sys.exit(1)

    @staticmethod
    def print_usage(script_name):
        """Print script usage information."""
        print(f"Usage: {script_name} [-f <Doxyfile>] [-H <hostname>] [-p <port>]")
        print("This script monitors the files specified in the Doxygen configuration file for changes and regenerates Doxygen documentation upon modification.")
        print("Options:")
        print("  -f, --doxyfile <Doxyfile>: Path to the Doxygen configuration file (default is 'Doxyfile')")
        print("  -H, --hostname <hostname>: Hostname to serve the HTML output (default is 'localhost')")
        print("  -p, --port <port>: Port number to serve the HTML output (default is '8000')")

def main():
    """Main function to handle command-line arguments and start monitoring."""

    doxyfile, hostname, port = DoxygenUsage.extract_params(sys.argv[1:])
    doxygen = Doxygen(doxyfile)
    doxygen.print_info()

    doxygen_server = DoxygenServer(doxygen, hostname, port)
    doxygen_server.start()

if __name__ == "__main__":
    main()
