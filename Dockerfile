# Use the rootproject/root image as the base image
FROM rootproject/root:latest

# Update package lists and install necessary packages
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV NPROC=8

ENV LD_LIBRARY_PATH=/usr/local/lib

# Update package lists and install necessary packages
COPY Dockerfile.packages packages

RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages | grep -v '#') && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

ENV CXX="/usr/bin/clang++"
ENV CC="/usr/bin/clang"

# Download and install KFR library
RUN git clone https://gitlab.cern.ch/igwn/software/kfr.git /opt/kfr
RUN mkdir -p kfr/build
RUN cd kfr/build && cmake .. -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DKFR_ENABLE_CAPI_BUILD=ON
RUN cd kfr/build && make -j${NPROC} && make -j${NPROC} install
RUN rm -rf /usr/local/include/kfr/config.h && touch /usr/local/include/kfr/config.h
RUN rm -rf kfr

# Download and install HDF5 library (optional)
RUN wget https://github.com/HDFGroup/hdf5/archive/refs/tags/hdf5-1_14_3.tar.gz
RUN tar -xzf hdf5-1_14_3.tar.gz
RUN mkdir -p hdf5-hdf5-1_14_3/build
RUN cd hdf5-hdf5-1_14_3/build && cmake .. -DHDF5_BUILD_CPP_LIB=ON -DCMAKE_INSTALL_PREFIX=/usr/local
RUN cd hdf5-hdf5-1_14_3/build && make -j${NPROC} && make -j${NPROC} install
RUN rm -rf hdf5-hdf5-1_14_3 hdf5-1_14_3.tar.gz

# Download and install RD KAFKA (optional)
RUN wget https://github.com/confluentinc/librdkafka/archive/refs/tags/v2.3.0.tar.gz && \
    tar -xzf v2.3.0.tar.gz && \
    cd librdkafka-2.3.0 && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local && \
    make -j${NPROC} && \
    make install
RUN rm -rf librdkafka-2.3.0 v2.3.0.tar.gz

# Install Escalade UIUC library
RUN git clone --recursive https://gitlab.cern.ch/escalade/lib/UIUC.git /opt/uiuc
RUN cd /opt/uiuc && mkdir build && cd build && cmake ..
RUN cd /opt/uiuc/build && make -j${NPROC} && make -j${NPROC} install
RUN rm -rf /opt/uiuc
