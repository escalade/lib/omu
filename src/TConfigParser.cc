/**
 **********************************************
 *
 * \file TConfigParser.cc
 * \brief Source code of the TConfigParser class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TConfigParser.h>
ClassImp(OMU::TConfigParser)

namespace fs = std::filesystem;

#ifdef CURL_FOUND

char    OMU::TConfigParser::_curl_download_chr    = '=';
int     OMU::TConfigParser::_curl_download_width  = 20;
TString OMU::TConfigParser::_curl_download_prefix = "";
TString OMU::TConfigParser::_curl_download_suffix = "";

int _curl_progress_function(void* clientp, double dltotal, double dlnow,  double ultotal, double ulnow)
{
    UNUSED(clientp);

    bool isDownload = dltotal > 0;
    bool isUpload   = ultotal > 0;
    if (!isDownload && !isUpload) return 0;

    double fraction = 0;
    if (isDownload) fraction = dlnow / dltotal;
    if (isUpload  ) fraction = ulnow / ultotal;

    double mb_total = 0;
    if (isDownload) mb_total = 1e-6*dltotal;
    if (isUpload  ) mb_total = 1e-6*ultotal;

    double mb_now = 0;
    if (isDownload) mb_now = 1e-6*dlnow;
    if (isUpload  ) mb_now = 1e-6*ulnow;

    if(mb_now == mb_total) {
        std::cout << "\33[2K";
        return 0;
    }

    int i0 = 0;
    int i  = (int) round(fraction * OMU::TConfigParser::_curl_download_width);
    int iN = OMU::TConfigParser::_curl_download_width;

    std::cout << "\r" << OMU::TConfigParser::_curl_download_prefix.Data() << " [";
    while(i0 < iN) {

        if(isDownload) {

            if(i0 < i || i == iN-1) std::cout << OMU::TConfigParser::_curl_download_chr;
            else if(i0 == i) std::cout << ">";
            else std::cout << " ";

        } else {

            if(i0 < i || i == iN-1) std::cout << " ";
            else if(i0 == i) std::cout << "<";
            else std::cout <<  OMU::TConfigParser::_curl_download_chr;
        }

        i0++;
    }

    std::cout << "] ";
    printf("(%3.0f%%, %.2fMB/%.2fMB) ", 100*fraction, mb_now, mb_total);
    std::cout << OMU::TConfigParser::_curl_download_suffix.Data() << "\r";

    fflush(stdout);
    return 0;
}

size_t _curl_write_callback(char *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*) userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}
#endif 

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

bool OMU::TConfigParser::IsCached()
{
    if(bNoCache) return false;

    // Check if valid cache found
    struct stat result;

    if(stat(this->output.Data(), &result) != 0) return false;

    std::time_t now = std::time(nullptr);
    std::asctime(std::localtime(&now));

    auto time = now - result.st_mtime;
    if(time > lifetime && lifetime >= 0) return false;

    // Read existing file
    std::ifstream f(output.Data());
    if(!f.is_open()) return false;

    return true;
}

TString OMU::TConfigParser::GetCache()
{
    if(!this->IsCached()) return "";

    // Read existing file
    std::ifstream f(this->output.Data());
    if(!f.is_open()) return "";

    std::stringstream buf;
                      buf << f.rdbuf();

    return buf.str();
}

TString OMU::TConfigParser::GetCacheFile()
{
    if(!this->IsCached()) return "";

    return this->output.Data();
}

void OMU::TConfigParser::ClearCache()
{
    if(this->IsCached())
        unlink(this->output.Data());
}

TUrl OMU::TConfigParser::GetUrl() { return this->url; }
OMU::TConfigParser *OMU::TConfigParser::SetUrl(TUrl url)
{
    this->url = url;
    this->contents = "";

    return this;
}

TString OMU::TConfigParser::GetOutput()
{
    return this->output;
}

int OMU::TConfigParser::GetLifetime() { return this->lifetime; }
OMU::TConfigParser *OMU::TConfigParser::SetLifetime(int lifetime)
{
    this->lifetime = lifetime;
    return this;
}

TJsonFile OMU::TConfigParser::GetJSON()
{
    this->Fetch();

    try { return TJsonFile::parse(this->contents.Data()); }
    catch(...) { return TJsonFile("{}"); };
}

void OMU::TConfigParser::PrintJSON()
{
    this->GetJSON().dump(4, ' ', true);
}

TXMLDocument *OMU::TConfigParser::GetXML(bool checkValidation)
{
    this->Fetch();
    
    TString output = this->output;
    if(this->IsLocalFile())
    {
        output = this->GetUrl().GetUrl();
        output.ReplaceAll(TString(this->GetUrl().GetProtocol())+"://", "");
        output = output.Strip(TString::kTrailing, '/');
    }

    TDOMParser *domParser = new TDOMParser();
                domParser->SetValidate(checkValidation);

    Int_t parseCode = domParser->ParseFile(output);
    if (UIUC::HandboxMsg::Warning(parseCode < 0, __METHOD_NAME__, domParser->GetParseCodeMessage(parseCode))) {
        return nullptr; 
    } 

    return domParser->GetXMLDocument();    
}

void OMU::TConfigParser::PrintXML(bool checkValidation)
{
    TXMLDocument *xml = this->GetXML(checkValidation);
    if(xml != nullptr) PrintXMLNode(xml->GetRootNode());
}

void OMU::TConfigParser::PrintXMLNode(TXMLNode *node, int level)
{
    if(node == nullptr) return;

	uintptr_t nodeAddr = reinterpret_cast<uintptr_t>(node);
    TString nodePrefix = TString(UIUC::HandboxMsg::Spacer(4*level)) + "OBJ: " + node->GetName() + "(" + Form("0x%lu", nodeAddr) + "): ";

    bool isComment = TString(node->GetNodeName()).EqualTo("comment");
    if(isComment) return;

    bool isLeaf = TString(node->GetNodeName()).EqualTo("text");
    if(!isLeaf) {
        
        TList *attrList = node->GetAttributes();
        TIter Next(attrList); 

        std::cout << UIUC::HandboxMsg::kPurple << nodePrefix << UIUC::HandboxMsg::kNoColor <<  UIUC::HandboxMsg::kGreen << node->GetNodeName() <<  UIUC::HandboxMsg::kNoColor;
        if(attrList) std::cout << UIUC::HandboxMsg::kOrange << " (";

        TXMLAttr *attr = nullptr;
        for(int i = 0; ( attr = (TXMLAttr*) Next()); i++) 
        { 
            uintptr_t attrAddr = reinterpret_cast<uintptr_t>(attr);
            std::cout << (i ? ", " : "") << attr->GetName() << "=\"" << attr->GetValue() << "\"";
        }

        if(attrList) std::cout << ")" << UIUC::HandboxMsg::kNoColor;

        TString text = TString(node->GetText());
                text = text.Strip(TString::kBoth);
                text = text.Strip(TString::kBoth, '\n');
                text = text.Strip(TString::kBoth, '\r');
                text = text.Strip(TString::kBoth, '\t');

        if(!text.EqualTo("")) {

            std::cout << "; ";
            std::cout << UIUC::HandboxMsg::kGreen << "Text " << UIUC::HandboxMsg::kNoColor;
            if(text.Length() > 100 ) std::cout << UIUC::HandboxMsg::kBlue << "(use -v to display more text) " << UIUC::HandboxMsg::kNoColor;
            std::cout << UIUC::HandboxMsg::MakeItShorter(node->GetText(), UIUC::HandboxMsg::kNoColor, 200);
        }
        
        std::cout << std::endl;
    }
    
    TXMLNode *childNode = node->GetChildren();
    while(childNode != NULL) {
        PrintXMLNode(childNode->GetNextNode(), level+1);
        childNode = childNode->GetNextNode();
    }
}

bool OMU::TConfigParser::IsLocalFile() 
{
    struct stat buffer;   
    return (stat (this->GetUrl().GetHost(), &buffer) == 0); 
}

OMU::TConfigParser *OMU::TConfigParser::Fetch(bool showProgress)
{
    TUrl url = this->GetUrl(); 
   
    this->contents = "{}";
    if (this->IsLocalFile()) {
        
        TString output = this->GetUrl().GetUrl();
                output.ReplaceAll(TString(this->GetUrl().GetProtocol())+"://", "");
                output = output.Strip(TString::kTrailing, '/');

        TString input = url.GetUrl();
                input.ReplaceAll(TString(this->GetUrl().GetProtocol())+"://", "");
                input = input.Strip(TString::kTrailing, '/');

        if(!UIUC::TFileReader::Exists(input)) {
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "File not found.. `%s`", input.Data());
                return this;
        }

        UIUC::HandboxMsg::PrintDebug(1, __METHOD_NAME__, (TString) "Reading local file \""+input+"\"");
        std::ifstream f(output);
        if(f.is_open()) {

            std::string buffer((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
            this->contents = buffer;
        }

        return this;
    }

    if (!url.IsValid()) {

        UIUC::HandboxMsg::PrintDebug(1, __METHOD_NAME__, "Invalid URL provided.. " + TString(url.GetUrl()));
        return this;
    }

    TString cache = this->GetCache();
    if(cache.Length()) {
        
        this->contents = cache;
        return this;
    }

    // Read from CURL
    UIUC::HandboxMsg::PrintDebug(1, __METHOD_NAME__, "No cached file found for \""+TString(url.GetUrl())+"\"..");

    #ifndef CURL_FOUND
    UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Cannot download files, CURL not found. Did you linked it during compilation time ?"); 
    #else
    std::string readBuffer;
    curl_global_init(CURL_GLOBAL_ALL);
    
    CURL* curl = curl_easy_init();
    CURLcode res;

    if(curl) {

        curl_easy_setopt(curl, CURLOPT_URL, url.GetUrl());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "ReqBin Curl Client/1.0");

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _curl_write_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, !showProgress);
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, _curl_progress_function);

        res = curl_easy_perform(curl);
        if(res != CURLE_OK) fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        long httpResponseCode;
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpResponseCode);
        if(httpResponseCode) this->httpResponseCode = httpResponseCode;

        if(httpResponseCode != 200) {
            fprintf(stderr, "curl_easy_perform() http response %ld (use -vv to see more)\n", httpResponseCode);
            UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, readBuffer.c_str());
        }

        char *contentType = NULL;
        curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &contentType);
        if(contentType) this->contentType = contentType;
    
        curl_easy_cleanup(curl);
    }

    if(this->httpResponseCode != 200)
       return this;

    std::ofstream o(this->GetOutput().Data());
                  o << std::setw(4) << readBuffer << std::endl;

    this->contents = TString(readBuffer);
    o.close();
    #endif

    return this;
}
