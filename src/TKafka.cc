/**
 **********************************************
 *
 * \file TKafka.cc
 * \brief Source code of the TKafka class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TKafka.h>
ClassImp(OMU::TKafka)

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

const char *OMU::TKafka::DEFAULT_HOST  = "localhost";
const int   OMU::TKafka::DEFAULT_PORT  = 9092;
const int   OMU::TKafka::DEFAULT_DELAY = 0;
const int   OMU::TKafka::DEFAULT_TICK  = 100;

volatile sig_atomic_t OMU::TKafka::bInterrupt = false;
volatile sig_atomic_t OMU::TKafka::bCallback  = false;
OMU::TKafka::TKafka(std::vector<OMU::TKafka::Broker> brokers, std::vector<TString> topics, TString groupid): brokers(brokers), topics(topics), groupid(groupid)
{
        this->max_transactions = -1;
        this->transactions = 0;

        this->tick = OMU::TKafka::DEFAULT_DELAY;
        this->delay = 0;

        this->partition = RD_KAFKA_PARTITION_UA;
        this->offset = -1;
        this->offsetShiftBy = 0;
        this->offsetForTimes = -1;
        this->bOffsetEarliest = false;
        this->bOffsetLatest = false;

        this->bRepeat = false;
};

TString OMU::TKafka::__toString() {

        TString brokers = "";
        for(int i = 0, N = this->brokers.size(); i < N; i++) {

                TString host = this->brokers[i].host;
                        host = host.EqualTo("") ? TString(DEFAULT_HOST) : host;

                int port = this->brokers[i].port;
                    port = !port ? DEFAULT_PORT : port;

                brokers = (TString) brokers + " " + host.Data() + ":" + TString::Itoa(port,10);
        }

        return brokers.Strip(TString::kBoth, ' ');
}

void OMU::TKafka::Print(Option_t *option)
{
        if(bRun) UIUC::HandboxMsg::PrintInfo(__METHOD_NAME__, "Connection is established.");
        else UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to connect");

        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Brokers registered: %s", this->__toString().Data());
        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Subscribed to %d topic(s)", this->GetNSubscribers());
}

// bool DeleteOffset(int offset)
// {
//         return 0;
// }

// bool DeleteBeforeOffset(int offset)
// {
//         return 0;
// }

bool OMU::TKafka::Connect(Mode mode, Dict dict)
{
        bRun = false;
        char errstr[512];        /* librdkafka API error reporting buffer */

        // Prepare brokers
        TString brokers = this->__toString();

        // Initialize configuration
        if(conf != NULL) {

                this->Disconnect();
                conf = NULL;
        }

        this->conf = rd_kafka_conf_new();
        if(this->conf == NULL) {
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to initialize kafka configuration container.");
                return bRun;
        }

        DictIter it;
        for (it = dict.begin(); it != dict.end(); it++)
        {
                if (rd_kafka_conf_set(conf, it->first.Data(), it->second.Data(), &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to set `" + it->first + "` to '" + it->second + "'\n%s", &errstr[0]);
                        rd_kafka_conf_destroy(conf);
                        return bRun;
                }                
        }

        /* Set bootstrap broker(s) as a comma-separated list of
        * host or host:port (default port 9092).
        * librdkafka will use the bootstrap brokers to acquire the full
        * set of brokers from the cluster. */
        if(dict.find("bootstrap.servers") != dict.end()) {
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected option `bootstrap.servers` provided through the optional Dict in " + __METHOD_NAME__ + "\nPlease pass `bootstrap.servers` through " + __METHOD_NAME__ + "::" + __METHOD_NAME__ + " instead.");
                return bRun;
        }

        if (rd_kafka_conf_set(conf, "bootstrap.servers", brokers.Data(), &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to reach bootstrap servers: %s\n%s", brokers.Data(), &errstr[0]);
                rd_kafka_conf_destroy(conf);
                return bRun;
        }

        if(this->bLowLatency) {

                if(dict.find("linger.ms")!=dict.end()) {
                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected option `linger.ms` provided through the optional Dict in " + __METHOD_NAME__ + "\n(or disable low-latency option instead)", groupid.Data(), &errstr[0]);
                        return bRun;
                }

                if (rd_kafka_conf_set(conf, "linger.ms", "0.1", &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to set `linger.ms` to '0.1'\n%s", &errstr[0]);
                        rd_kafka_conf_destroy(conf);
                        return bRun;
                }
        }

        rd_kafka_message_t *rkm;
        switch(mode) {

                case OMU::TKafka::Mode::Producer:

                        /* Set the delivery report callback.
                        * This callback will be called once per message to inform
                        * the application if delivery succeeded or failed.
                        * See dr_msg_cb() above.
                        * The callback is only triggered from rd_kafka_poll() and
                        * rd_kafka_flush(). */
                        rd_kafka_conf_set_dr_msg_cb(conf, dr_msg_cb);

                        rk = rd_kafka_new(RD_KAFKA_PRODUCER, conf, errstr, sizeof(errstr));
                        if (!rk) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to create new producer.\n%s", &errstr[0]);
                                return bRun;
                        }

                        break;

                default:
                case OMU::TKafka::Mode::Consumer:

                        /* Set the consumer group id.
                        * All consumers sharing the same group id will join the same
                        * group, and the subscribed topic' partitions will be assigned
                        * according to the partition.assignment.strategy
                        * (consumer config property) to the consumers in the group. */
                        if(dict.find("group.id") != dict.end()) {
                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected option `group.id` provided through the optional Dict in " + __METHOD_NAME__ + "\nPlease pass `bootstrap.servers` through " + __METHOD_NAME__ + "::" + __METHOD_NAME__ + " instead.");
                                return bRun;
                        }

                        if (rd_kafka_conf_set(conf, "group.id", groupid.Data(), &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to set \"%s\" group ID.\n%s", groupid.Data(), &errstr[0]);
                                rd_kafka_conf_destroy(conf);
                                return bRun;
                        }

                        /* If there is no previously committed offset for a partition
                        * the auto.offset.reset strategy will be used to decide where
                        * in the partition to start fetching messages.
                        * By setting this to earliest the consumer will read all messages
                        * in the partition if there was no previously committed offset. */                           
                        if(dict.find("auto.offset.reset") == dict.end()) {

                                if (rd_kafka_conf_set(conf, "auto.offset.reset", "earliest", &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to set offset.\n%s", &errstr[0]);
                                        rd_kafka_conf_destroy(conf);
                                        return bRun;
                                }
                        }

                        rk = rd_kafka_new(RD_KAFKA_CONSUMER, conf, errstr, sizeof(errstr));
                        if (!rk) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to create new consumer.\n%s", &errstr[0]);
                                return bRun;
                        }

                        //
                        // Configure offsets
                        // for (int i = 0; i < topics.size(); i++) {}
                        // rd_kafka_topic_partition_list_t* partition_list = rd_kafka_topic_partition_list_new(1);
                        // rd_kafka_topic_partition_t     * pt0            = rd_kafka_topic_partition_list_add(partition_list, topic, 0);
                        //                                  pt0->offset    = ~0; // set to max integer value

                        // rd_kafka_resp_err_t offsets_err = rd_kafka_offsets_for_times(rk, partition_list, 10000);
                        // if (offsets_err != RD_KAFKA_RESP_ERR_NO_ERROR) printf("ERROR: Failed to get offsets: %d: %s.\n", offsets_err, rd_kafka_err2str(offsets_err));
                        // else printf("Successfully got high watermark offset %d.\n", pt0->offset);

                        conf = NULL; /* Configuration object is now owned, and freed,
                                      * by the rd_kafka_t instance. */

                        /* Redirect all messages from per-partition queues to
                         * the main queue so that messages can be consumed with one
                         * call from all assigned partitions.
                         *
                         * The alternative is to poll the main queue (for events)
                         * and each partition queue separately, which requires setting
                         * up a rebalance callback and keeping track of the assignment:
                         * but that is more complex and typically not recommended. */
                        rd_kafka_poll_set_consumer(rk);

                        rkm = rd_kafka_consumer_poll(rk, this->tick);

                        /* Convert the list of topics to a format suitable for librdkafka */
                        subscription = rd_kafka_topic_partition_list_new(topics.size());
                        for (int i = 0; i < topics.size(); i++) {

                                rd_kafka_topic_partition_list_set_offset(subscription, topics[i], this->partition, this->offset);
                                rd_kafka_topic_partition_list_add(subscription, topics[i], this->partition);
                        }

                        err = rd_kafka_subscribe(rk, subscription);
                        if (err) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to subscribe to %d topics: %s", subscription->cnt, rd_kafka_err2str(err));
                                rd_kafka_topic_partition_list_destroy(subscription);
                                rd_kafka_destroy(rk);
                                return bRun;
                        }

                        UIUC::HandboxMsg::PrintDebug(1, __METHOD_NAME__, "Subscribed to %d topic(s)", this->GetNSubscribers());
                        UIUC::HandboxMsg::PrintDebug(1, __METHOD_NAME__, "Waiting for rebalance and messages...", subscription->cnt);

                        rd_kafka_topic_partition_list_destroy(subscription);

                        break;
        }

        /* Signal handler for clean shutdown */
        bRun = true;
        OMU::TKafka::EnableSignalHandler();

        time(&oot);
        return bRun;
}

int OMU::TKafka::GetNSubscribers() {
        return this->subscription->cnt;
}

OMU::TKafka::Record ParseRecord(rd_kafka_message_t *rkm)
{
        time_t epoch;
        time(&epoch);

        TString key = UIUC::IsPrintable((const char*) rkm->key, rkm->key_len) ? TString((const char*)rkm->key, rkm->key_len) : "";
        TString msg;

        OMU::TKafka::Record r;
                                r.topic     = rd_kafka_topic_name(rkm->rkt);
                                r.offset    = rkm->offset;
                                r.partition = rkm->partition;
                                r.key       = key;
                                r.msg       = msg;
                                r.epoch     = epoch;
        return r;
}

void OMU::TKafka::EnableSignalHandler(){

        OMU::TKafka::bInterrupt = false;

        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Enable Ctrl+C catcher..");
        struct sigaction sigIntHandler;
        sigIntHandler.sa_handler = OMU::TKafka::HandlerCTRL_C;
        sigemptyset(&sigIntHandler.sa_mask);
        sigIntHandler.sa_flags = 0;
        sigaction(SIGINT, &sigIntHandler, NULL);
}

void OMU::TKafka::DisableSignalHandler() {

        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Disable Ctrl+C catcher..");
        signal(SIGINT, SIG_DFL);
}

void OMU::TKafka::HandlerCTRL_C(int s){

        std::cerr << std::endl;
        std::cerr << "** Caught signal " << s << std::endl;
        std::cerr << "** You stopped processing pressing CTRL-C.."<< std::endl;
        
        OMU::TKafka::bInterrupt = true;
}

int OMU::TKafka::Disconnect()
{
        OMU::TKafka::DisableSignalHandler();

        /* Destroy the config */
        if(conf != NULL) rd_kafka_conf_destroy(conf);

        /* Close the consumer: commit final offsets and leave the group. */
        if(rk != NULL) rd_kafka_consumer_close(rk);

        /* Destroy the consumer */
        if(rk != NULL) rd_kafka_destroy(rk);

        if(this->bRun) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Connection closed.\n");
        this->bRun = false;
        
        return 1;
}


bool OMU::TKafka::Produce(TString topic, std::vector<unsigned char> message)
{
        int iTry = 0;

        UIUC::HandboxMsg::DecrementTab();

        bool bInteractive = message.size() == 0;
        while (this->bRun && (this->max_transactions < 0 || this->max_transactions > this->transactions)) {

                auto start = std::chrono::high_resolution_clock::now();
                if(bInterrupt) break;

                if(bInteractive) {

                        std::cout << "> ";

                        TString buffer;
                                buffer.ReadLine(std::cin);

                        message = str2vec(buffer);
                        this->transactions++;
                }

                size_t len = message.size();
                rd_kafka_resp_err_t err;

                if (len == 0) {
                        /* Empty line: only serve delivery reports */
                        rd_kafka_poll(rk, 0 /*non-blocking */);
                        break;
                }

                if (message[len - 1] == '\n') /* Remove newline */
                        message[--len] = '\0';


                /*
                * Send/Produce message.
                * This is an asynchronous call, on success it will only
                * enqueue the message on the internal producer queue.
                * The actual delivery attempts to the broker are handled
                * by background threads.
                * The previously registered delivery report callback
                * (dr_msg_cb) is used to signal back to the application
                * when the message has been delivered (or failed).
                */
        retry:
                err = rd_kafka_producev(
                /* Producer handle */
                rk,
                RD_KAFKA_V_TOPIC(topic.Data()),
                RD_KAFKA_V_PARTITION(partition),
                RD_KAFKA_V_MSGFLAGS(RD_KAFKA_MSG_F_COPY),
                RD_KAFKA_V_VALUE((void*) &message[0], len),
                RD_KAFKA_V_OPAQUE(NULL),
                RD_KAFKA_V_END);

                if (UIUC::HandboxMsg::Error(err, __METHOD_NAME__, "Failed to produce message for topic `%s`: %s", topic.Data(), rd_kafka_err2str(err))) {

                        if (err == RD_KAFKA_RESP_ERR__QUEUE_FULL) {
                                /* If the internal queue is full, wait for
                                * messages to be delivered and then retry.
                                * The internal queue represents both
                                * messages to be sent and messages that have
                                * been sent or failed, awaiting their
                                * delivery report callback to be called.
                                *
                                * The internal queue is limited by the
                                * configuration property
                                * queue.buffering.max.messages */
                                rd_kafka_poll(rk, 10*this->tick /*block for max X ms*/);

                                if(this->GetMaxTry() > 0 && this->GetMaxTry() > iTry++) break;

                                goto retry;
                        }

                } else UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Enqueued message (%zd bytes) for topic `%s`", TMath::Max((int) 0, (int) message.size()-1), topic.Data());

                /* A producer application should continually serve
                * the delivery report queue by calling rd_kafka_poll()
                * at frequent intervals.
                * Either put the poll call in your main loop, or in a
                * dedicated thread, or call it after every
                * rd_kafka_produce() call.
                * Just make sure that rd_kafka_poll() is still called
                * during periods where you are not producing any messages
                * to make sure previously produced messages have their
                * delivery report callback served (and any other callbacks
                * you register). */
                rd_kafka_poll(rk, 0 /*non-blocking*/);
                if(!bInteractive) {

                        while(bRun && OMU::TKafka::bCallback == false)
                                rd_kafka_poll(rk, this->tick);

                        if(!bRepeat) return 0;
                }

                auto elapsed = std::chrono::high_resolution_clock::now() - start;
                long long microseconds = this->delay - std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count()/1000;
                this->Await(microseconds);
        }

        /* Wait for final messages to be delivered or fail.
         * rd_kafka_flush() is an abstraction over rd_kafka_poll() which
         * waits for all messages to be delivered. */
        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Flushing final messages..");
        rd_kafka_flush(rk, 100 * this->tick /* wait for max 10 seconds */);

        /* If the output queue is still not empty there is an issue
         * with producing messages to the clusters. */
        UIUC::HandboxMsg::Warning(rd_kafka_outq_len(rk) > 0, __METHOD_NAME__, "%d message(s) were not delivered", rd_kafka_outq_len(rk));

        return 1;
}

std::vector<unsigned char> OMU::TKafka::ReadFile(const char* fname)
{
    // open the file:
    std::ifstream file(fname, std::ios::binary);

    if(UIUC::HandboxMsg::Error(!file.is_open(), __METHOD_NAME__, "Failed to open file \"%s\"", fname))
        return {};

    // Stop eating new lines in binary mode!!!
    file.unsetf(std::ios::skipws);

    // get its size:
    std::streampos fileSize;

    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // reserve capacity
    std::vector<unsigned char>  vec;
                                vec.reserve(fileSize);

                                // read the data:
                                vec.insert(vec.begin(),
                                        std::istream_iterator<unsigned char>(file),
                                        std::istream_iterator<unsigned char>());

    return vec;
}

bool OMU::TKafka::Save(std::vector<unsigned char> bytes, TString fOutputPattern, std::vector<TString> fIdentifiers)
{
        if(UIUC::TFileReader::HasPattern(fOutputPattern) && !fIdentifiers.size()) {

		UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Save output enabled. Replacement char found.. But no ID found.. (Use {} in the --output options)");
		return 0;

	} else if(fOutputPattern.EqualTo("")) {

		TString eMessage = "No output filename found.. (Use --output/--output-finalize options)";
		UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, eMessage);
                return 0;

	}

        // TODO..
        TString fOutputFullname = UIUC::TFileReader::SubstituteIdentifier(fOutputPattern, fIdentifiers);
	UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Saving %d bytes into:\n%s", bytes.size(), fOutputFullname.Data());

        std::ofstream f;
                      f.open(fOutputFullname.Data(), std::ofstream::out | std::ofstream::app);

        if(f.is_open()) {

                std::copy(bytes.cbegin(), bytes.cend(), std::ostream_iterator<unsigned char>(f));
                f.close();

                return 1;
        }

        return 0;
}

std::vector<OMU::TKafka::Record> OMU::TKafka::Consume(int N, bool bProgressBar)
{
        int iTry = 0;
        // if(UIUC::HandboxMsg::Error(this->output.Data(), __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the latest offset.")) return {};

        if(UIUC::HandboxMsg::Error(this->offset >= 0 && this->bOffsetLatest, __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the latest offset.")) return {};
        else if(UIUC::HandboxMsg::Error(this->offset > 0 && this->bOffsetLatest, __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the earliest offset.")) return {};
        else if(UIUC::HandboxMsg::Error(this->bOffsetEarliest && this->bOffsetLatest, __METHOD_NAME__, "Ambiguous options provided. You asked for both earliest and latest offsets.")) return {};

        this->ResetTransactions();                
        for (int i = 0; this->bRun && (this->max_transactions < 0 || this->max_transactions > this->transactions) && (N == 0 || this->transactions < N); i++) {

                auto start = std::chrono::high_resolution_clock::now();
                if(OMU::TKafka::bInterrupt) break;
                
                rd_kafka_message_t *rkm;

                rkm = rd_kafka_consumer_poll(rk, this->tick);
                if (!rkm) {

                        if(i == 0) {
                                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Waiting for the next incoming message..");
                        } else if(i++ % 100 == 0) {
                                UIUC::HandboxMsg::PrintDebug(100, __METHOD_NAME__, "Waiting for the next incoming message..");
                        }
 
                        continue; /* Timeout: no message within dt,
                                   *  try again. This short timeout allows
                                   *  checking for `run` at frequent intervals.
                                   */
                }

                if(bProgressBar && (N > 0 || this->max_transactions > 0)) UIUC::HandboxMsg::PrintProgressBar(__METHOD_NAME__, this->transactions, TMath::Max(N, this->max_transactions));
                if(UIUC::HandboxMsg::IsDebugEnabled(10)) {
                        UIUC::HandboxMsg::ErasePreviousLine();
                }
                
                /* consumer_poll() will return either a proper message
                 * or a consumer error (rkm->err is set). */
                if (rkm->err) {
                        /* Consumer errors are generally to be considered
                         * informational as the consumer will automatically
                         * try to recover from all types of errors. */
                        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Consumer error: %s", rd_kafka_message_errstr(rkm));
                        rd_kafka_message_destroy(rkm);

                        if(this->GetMaxTry() > 0 && this->GetMaxTry() > iTry++) break;
                        continue;
                }

                /* Print the message key. */
                TString str = "";
                if (rkm->key)
                        str += TString::Format("[Length: %d bytes (group \"%s\"; offset %" PRId64 ")]", (int)rkm->key_len, this->groupid.Data(), rkm->offset);
                if (rkm->key && UIUC::IsPrintable((const char*) rkm->key, rkm->key_len))
                        str += TString::Format("Key: \"%.*s\"", (int)rkm->key_len, (const char *)rkm->key);

                /* Print the message value/payload. */
                if (rkm->payload) this->transactions++;
                if (rkm->payload)
                        str += TString::Format("[Payload: %d bytes (group \"%s\"; offset %" PRId64 ")]", (int)rkm->len, this->groupid.Data(), rkm->offset);

                /* Processing message. */
                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Message received in topic \"%s\" on partition: %" PRId32, rd_kafka_topic_name(rkm->rkt), rkm->partition, str.Data());
                OMU::TKafka::Record record;
                                    record.offset = rkm->offset;
                                    record.partition = rkm->partition;
                                    record.topic = rd_kafka_topic_name(rkm->rkt);
                                    record.epoch = time(0);

                if(rkm->key_len > 0) record.key = TString((const char *)rkm->key, rkm->key_len);
                if(rkm->len     > 0) record.msg = TString((const char *)rkm->payload, rkm->len);

                records.push_back(record);

                if(!this->output.EqualTo("")) {

                        if (rkm->payload && UIUC::IsPrintable((const char*) rkm->payload, rkm->len))
                                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, TString::Format("[Message received: \"%.*s\"]", (int)rkm->len, (const char *)rkm->payload));
                
                        std::vector<unsigned char> bytes;
                                                   bytes.insert(bytes.end(), (const char *) rkm->payload,  (const char *) rkm->payload + rkm->len);

                        std::vector<TString> fIdentifiers;
                                             fIdentifiers.push_back(rd_kafka_topic_name(rkm->rkt));
                                             fIdentifiers.push_back(TString::Itoa(rkm->partition, 10));
                                             fIdentifiers.push_back(TString::Itoa(rkm->offset, 10));

                        OMU::TKafka::Save(bytes, this->output, fIdentifiers);

                } else {

                        if (rkm->payload && UIUC::IsPrintable((const char*) rkm->payload, rkm->len))
                                UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, TString::Format("[Message received: \"%.*s\"]", (int)rkm->len, (const char *)rkm->payload));
                }

                rd_kafka_message_destroy(rkm);

                auto elapsed = std::chrono::high_resolution_clock::now() - start;
                long long microseconds = this->delay - std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count()/1000;
                
                this->Await(microseconds);
        }

        return records;
}

std::vector<OMU::TKafka::Broker>  OMU::TKafka::GetBrokers() { return this->brokers; }
OMU::TKafka *OMU::TKafka::AddBroker(OMU::TKafka::Broker broker)
{
        std::vector<OMU::TKafka::Broker> _b;
                            _b.push_back(broker);

        std::vector<OMU::TKafka::Broker>::iterator it = std::search(this->brokers.begin(),this->brokers.end(),_b.begin(),_b.end(), OMU::TKafka::_match_broker);
        if(it == this->brokers.end())
                    this->brokers.push_back(broker);

        return this;
}

std::vector<OMU::TKafka::Broker> OMU::TKafka::ParseBroker(std::vector<TString> hostport)
{
        std::vector<Broker> brokers;
        for(int i = 0, N = hostport.size(); i < N; i++) {
                brokers.push_back(ParseBroker(hostport[i]));
        }

        return brokers;
}

OMU::TKafka::Broker OMU::TKafka::ParseBroker(TString hostport)
{
    TObjArray *array = hostport.Tokenize(":");
    TString host = (array->GetEntries() > 0) ? ((TObjString *)(array->At(0)))->String() : TString(OMU::TKafka::DEFAULT_HOST);
    int port = (array->GetEntries() > 1) ? ((TObjString *)(array->At(1)))->String().Atoi() : OMU::TKafka::DEFAULT_PORT;

    OMU::TKafka::Broker b;
                            b.host = host;
                            b.port = port;

    return b;
}

OMU::TKafka *OMU::TKafka::RemoveBroker(OMU::TKafka::Broker broker)
{
        std::vector<OMU::TKafka::Broker> _b;
                            _b.push_back(broker);

        std::vector<OMU::TKafka::Broker>::iterator it = std::search(this->brokers.begin(),this->brokers.end(),_b.begin(),_b.end(), OMU::TKafka::_match_broker);
        if(it != this->brokers.end())
                    this->brokers.erase(it);

        return this;
}
