/**
 **********************************************
 *
 * \file TKalmanFilter.cc
 * \brief Source code of the TKalmanFilter class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TKalmanFilter.h>
ClassImp(OMU::TKalmanFilter)

using State = OMU::TKalmanFilter::State;
using Measurement = OMU::TKalmanFilter::Measurement;

void OMU::TKalmanFilter::Initialize(const TMatrixD& A, const TMatrixD& H, const TMatrixD& B, const TVectorD& u)
{
    this->A.ResizeTo(A.GetNrows(), A.GetNcols());
    this->A = A;
    this->At.ResizeTo(A.GetNcols(), A.GetNrows());
    this->At.Transpose(this->A);

    this->H.ResizeTo(H.GetNrows(), H.GetNcols());
    this->H = H;
    this->Ht.ResizeTo(H.GetNcols(), H.GetNrows());
    this->Ht.Transpose(this->H);

    this->B.ResizeTo(B.GetNrows(), B.GetNcols());
    this->B = B; 
    this->Bt.ResizeTo(B.GetNcols(), B.GetNrows());
    this->Bt.Transpose(this->B);

    this->u.ResizeTo(u);
    this->u = u;

    this->K.ResizeTo(H.GetNcols(), H.GetNrows());
    this->K = 0;
    this->I.ResizeTo(A.GetNcols(), A.GetNrows());
    this->I = OMU::Identity<double>(A.GetNcols(), A.GetNrows());

    this->xk = State(TVectorD(A.GetNrows()));
    this->wk = State(TVectorD(A.GetNrows()));
    this->vk = State(TVectorD(H.GetNrows()));
    this->zk = Measurement(TVectorD(H.GetNrows()));
}

OMU::TKalmanFilter::~TKalmanFilter() 
{
    this->A.Clear();
    this->At.Clear();
    this->B.Clear();
    this->Bt.Clear();
    this->H.Clear();
    this->Ht.Clear();

    this->u.Clear();

    this->K.Clear();
    this->I.Clear();
};

void OMU::TKalmanFilter::First(double t0, const TVectorD& x0, const State &wk0, const State &vk0, double timeStep)
{ 
    State xk0(x0, isnan(t0) ? 0 : t0);
    First(xk0, wk0, vk0, timeStep);
}

void OMU::TKalmanFilter::First(double t0, const TVectorD& x0, const TMatrix &Q, const TMatrix &R, double timeStep)
{ 
    State xk0(x0, isnan(t0) ? 0 : t0);
    First(xk0, Q, R, timeStep);
}

void OMU::TKalmanFilter::First(const OMU::TKalmanFilter::State &xk0, const TMatrix &Q, const TMatrix &R, double timeStep)
{ 
    State wk0(TVectorD(Q.GetNrows()));
          wk0._cov = Q;
    State vk0(TVectorD(R.GetNrows()));
          vk0._cov = R;

    First(xk0, wk0, vk0, timeStep);
}

void OMU::TKalmanFilter::First(const OMU::TKalmanFilter::State &xk0, const OMU::TKalmanFilter::State &wk0, const OMU::TKalmanFilter::State &vk0, double timeStep) 
{ 
    Reset();
    
    xk = xk0;
    xk._time = isnan(xk0._time) ? 0 : xk0._time;

    wk = wk0;
    vk = vk0;
    
    this->timeStep = timeStep;
}

OMU::TKalmanFilter::State OMU::TKalmanFilter::Update(const OMU::TKalmanFilter::Measurement& zk) {

    if (UIUC::HandboxMsg::Error(xk().GetNrows() == 0, __METHOD_NAME__, "Kalman filter is not initialized.. no initial state provided"))
        return State();

    if (UIUC::HandboxMsg::Error(isnan(zk._time) && isnan(this->timeStep), __METHOD_NAME__, "No time information provided at initialization stage")) {
        return State();
    }

    // Prediction
    TMatrixD P = this->xk._cov;
    TMatrixD Q = this->wk._cov;
    TMatrixD R = this->vk._cov;

    // Update estimate
    P = A*P*At + Q;
    K = P*Ht * (H*P*Ht + R).Invert();
    
    // Measurement pre-fit residual
    const TVectorD &x = A*this->xk() + B*u;
    const TVectorD &z = zk();
    const TVectorD &y = z - H*x;
    
    // New estimate
    this->xk._self = x + K*y;
    this->xk._cov  = (I - K*H) * P;
    this->xk._time = isnan(zk._time) ? this->xk._time + timeStep : zk._time;

    return this->xk;
}

void OMU::TKalmanFilter::Print(Option_t *option)
{
    TString opt = option;
            opt.ToLower();

    std::cout << "TKalmanFilter(" << UIUC::HandboxMsg::Address(this) << ")" << std::endl;

    std::cout << "-- Matrix A";
    A.Print();
    std::cout << "-- Matrix B";
    B.Print();
    std::cout << "-- Matrix H";
    H.Print();

    std::cout << "-- Covariance matrix P";
    xk._cov.Print();
    std::cout << "-- Covariance matrix Q";
    wk._cov.Print();
    std::cout << "-- Covariance matrix R";
    vk._cov.Print();
}