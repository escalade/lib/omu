/**
 **********************************************
 *
 * \file OMU::TObjectStream.cc
 * \brief Source code of the OMU::TObjectStream class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TObjectStream.h>
ClassImp(OMU::TObjectStream)

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

int OMU::TObjectStream::Initialize(const char *token)
{
    this->token = token;
    this->connect = (this->Open() == this);

    return this->connect;
}

void OMU::TObjectStream::Close()
{
    this->connect = false;
    TPSocket::Close();
}

OMU::TObjectStream* OMU::TObjectStream::Open(const char *host, const char *service, const char *token, Int_t tcpwindowsize)
{
    UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Try to connect.. %s on %s", host, UIUC::HandboxUsage::GetCurrentTime().Data());

    OMU::TObjectStream *stream = NULL;
    for(int i = 0, authAttempts = 0; authAttempts < maxAuthAttempts; i++) {
    
        OMU::TObjectStream *stream = NULL;
        try {
    
            auto errorHandler = SetErrorHandler(TObjectStream::ErrorHandler);
            stream = new OMU::TObjectStream(host, service, token, tcpwindowsize);
            SetErrorHandler(errorHandler);

        } catch(const std::runtime_error& e) { authAttempts++; }

        if(stream != NULL) {
            
            if(stream->IsOpen()) return stream;
            
            delete stream;
            stream = NULL;
        }

        gSystem->Sleep(OMU::TObjectStream::timeout);
    }

    UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Too many failed attempts to connect %s.. aborted on %s.", host, UIUC::HandboxUsage::GetCurrentTime().Data());
    return NULL;
}

Long_t OMU::TObjectStream::Ticks()
{
    return this->ticks;
}

Long_t OMU::TObjectStream::LastUpdate()
{
    this->ticks++;
    return (Long_t) 1000*(TTimeStamp().AsDouble() - this->lastUpdate);
}

OMU::TObjectStream* OMU::TObjectStream::Open(const char *host, Int_t port, const char *token, Int_t tcpwindowsize)
{
    return OMU::TObjectStream::Open(host, gSystem->GetServiceByPort(port), token, tcpwindowsize);
}

OMU::TObjectStream* OMU::TObjectStream::Open()
{
    if (!this->IsValid()) {

        OMU::TObjectStream *stream = NULL;
        try {
    
            auto errorHandler = SetErrorHandler(TObjectStream::ErrorHandler);
            stream = new OMU::TObjectStream(this->GetName(), this->fService, this->token, this->fTcpWindowSize);       
            SetErrorHandler(errorHandler);

        } catch(const std::runtime_error& e) { }

        if(stream != NULL) {

            if(stream->IsOpen()) return stream;
            delete stream;
        }

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to reach %s at %s", this->GetName(), UIUC::HandboxUsage::GetCurrentTime().Data());
        return NULL;
      }

    if(!this->Authenticate()) {
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Authentication failed on `%s` (received on %s) ", this->GetName(), UIUC::HandboxUsage::GetCurrentTime().Data());
        throw std::runtime_error("Connection refused.");
    }

    UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Connection established on %s with `%s`", UIUC::HandboxUsage::GetCurrentTime().Data(), this->GetName());
    return this;
}

TMessage* OMU::TObjectStream::FindObject(const char *objName, TClass *cl)
{
    return this->FindObject(objName, cl->GetName());
}

TMessage* OMU::TObjectStream::FindObject(const char *objName, const char *cl)
{
    if(!this->IsOpen()) return NULL;

    TMessage *message = this->Send(objName);
    if (!TString(cl).EqualTo("") && !message->GetClass()->InheritsFrom(cl)) return NULL;
    
    return message;
}

bool OMU::TObjectStream::IsOpen()
{ 
    return connect == SUCCESS && this->IsValid();
}

bool OMU::TObjectStream::Authenticate()
{
    bool success;
    TMessage *message = this->Send(token);
    if(message == NULL) return false;
    
    message->ReadBool(success);
    return success;
}

TMessage* OMU::TObjectStream::Send(const char *msg)
{
    TMessage *message = NULL;
    try {

        this->lastUpdate = TTimeStamp().AsDouble();
        this->ticks = 0;

        auto errorHandler = SetErrorHandler(TObjectStream::ErrorHandler);
        TPSocket::Send(msg);

        if (this->Recv(message) <= 0) return NULL;

        SetErrorHandler(errorHandler);

    } catch(const std::runtime_error& e) {
        this->Close();
    }

    return message;
}

TObject* OMU::TObjectStream::ReadObject(const char *objName, TClass *cl)
{
    return this->ReadObject(objName, cl->GetName());
}

TObject* OMU::TObjectStream::ReadObject(const char *objName, const char *cl)
{
    TMessage *message = this->FindObject(objName);
    if(message == NULL) return NULL;

    return this->ReadObject(message);
}

TObject* OMU::TObjectStream::ReadObject(TMessage *message)
{
    if(!this->IsOpen()) return NULL;
    return message->ReadObject(message->GetClass());
}

void OMU::TObjectStream::ErrorHandler(int level, Bool_t abort, const char *location, const char *msg)
{
    if (level >= ::kError) throw std::runtime_error(msg);
    DefaultErrorHandler(level, abort, location, msg);
}

void OMU::TObjectStream::Print(Option_t *opt)
{
    this->Print(opt);
}