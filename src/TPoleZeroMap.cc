/**
 **********************************************
 *
 * \file TPoleZeroMap.cc
 * \brief Source code of the TPoleZeroMap class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TPoleZeroMap.h>
ClassImp(OMU::TPoleZeroMap)

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

using namespace std;
using namespace kfr;

OMU::TPoleZeroMap::TPoleZeroMap(const char *name, Filter type, double k, const std::vector<double> &B, const std::vector<double> &A, Double_t xmin, Double_t xmax, Double_t ymin, Double_t ymax, Option_t * opt)
: TComplexFilter(type, k, B, A), TComplexPlan(name, OMU::tfunc, xmin, xmax, ymin, ymax, B.size()+A.size()+3, opt)
{
    this->SetTitle(Form("H(%s) = B(%s)/A(%s); Re[H(%s)]%s; Im[H(%s)]%s", 
        OMU::TKFR::enum2str(type).Data(), OMU::TKFR::enum2str(type).Data(), OMU::TKFR::enum2str(type).Data(), 
        OMU::TKFR::enum2str(type).Data(), type == Filter::Analog ? " (in s^{-1})" : "",
        OMU::TKFR::enum2str(type).Data(), type == Filter::Analog ? " (in s^{-1})" : "")
    );

    this->SetParameter(0, this->B->NPar());
    this->SetParameter(1, this->A->NPar());
    this->SetParameter(2, (bool) type);

    int i0 = 3;
    int order = TMath::Max(this->B->NPar(), this->A->NPar());
    for(int i = 0; i < order; i++) this->SetParameter(i+i0 , this->B->Parameters()[i]);
    for(int i = 0; i < order; i++) this->SetParameter(i+i0 + this->B->NPar(), this->A->Parameters()[i]);
}

void OMU::TPoleZeroMap::Draw(Option_t *option, int level)	
{
    std::vector<std::complex<double>> p = this->GetPoles();
    std::vector<std::complex<double>> z = this->GetZeros();

    TString opt = option;
    if(!opt.EqualTo("") && !opt.EqualTo("SAME")) {
    
        OMU::TComplexPlan::Draw(option);
        this->DrawUnitCircle(IsDigital() ? level : 0);
        this->DrawSectors(IsDigital() ? level : 0);

    } else {

        OMU::TComplexPlan::DrawContour(option);
        gPad->SetLogz(1);

        for(int i = 0, I = z.size(); i < I; i++) {
            TGraph *m = new TGraph();
                    m->AddPoint(z[i].real(), z[i].imag());
                    m->SetMarkerStyle(71);
                    m->SetMarkerColor(this->GetLineColor() == kBlack ? kRed : this->GetLineColor()+2);
                    m->Draw("SAMEP");
        }

        for(int i = 0, I = p.size(); i < I; i++) {
            TGraph *m = new TGraph();
                    m->AddPoint(p[i].real(), p[i].imag());
                    m->SetMarkerStyle(70);
                    m->SetMarkerColor(this->GetLineColor() == kBlack ? kRed : this->GetLineColor()+2);
                    m->Draw("SAMEP");
        }
    }
}

void OMU::TPoleZeroMap::DrawNyquist(Option_t *option, int N)	
{
    TGraph *contour = Nyquist(N);
            contour->SetLineColor(this->GetLineColor() == kBlack ? kBlue : this->GetLineColor());
            contour->SetMarkerColor(this->GetMarkerColor() == kBlack ? this->GetLineColor() : this->GetMarkerColor());
            contour->SetLineWidth(2);
            contour->Draw(option);
}

TGraph *OMU::TPoleZeroMap::Nyquist(int N)
{
    std::vector<std::complex<double>> z;
    for(int i = 0; i < N; i++)
        z.push_back(std::complex(TMath::Cos(2*TMath::Pi() * i/N), TMath::Sin(2*TMath::Pi() * i/N)));

    std::vector<OMU::Complex> H = OMU::TComplexFilter::Response(z);
    std::vector<double> Re = OMU::Real(H);
                        Re.push_back(Re[0]);

    std::vector<double> Im = OMU::Imag(H);
                        Im.push_back(Im[0]);

    TString contourName = "Graph_"+UIUC::GetRandomStr(6);
    TExec *ex = new TExec("ex","OMU::TPoleZeroMap::NyquistArrows(\""+contourName+"\", 25);");
    
    TGraph *contour = new TGraph(H.size()+1, &Re[0], &Im[0]);
            contour->SetName(contourName);
            contour->SetTitle("Nyquist plot");
            contour->GetListOfFunctions()->Add(ex);

    return contour;
}


void OMU::TPoleZeroMap::NyquistArrows(TString contourName, int dMin)
{
    TGraph *contour = (TGraph*) gPad->GetListOfPrimitives()->FindObject(contourName);
    if(contour == NULL) return;

    Double_t x0,y0;
    if (contour->GetN() > 0) {

        contour->GetPoint(0, x0, y0);

        TGraph m;
               m.AddPoint(x0, y0);
               m.SetMarkerColor(contour->GetLineColor());
               m.SetMarkerStyle(20);
               m.Paint("");
    }

    for (int i = 0, N = contour->GetN()-1; i < N; i++) {

        if (i % dMin == 0) {

            Double_t x1,y1, x2,y2;
            contour->GetPoint(i, x1,y1);
            contour->GetPoint(i+1, x2,y2);

            double padSize = OMU::Distance(std::vector<double>({gPad->GetUxmin(), gPad->GetUymin()}), std::vector<double>({gPad->GetUxmax(), gPad->GetUymax()}));
            
            if (OMU::Distance(std::vector<double>({x0, y0}), std::vector<double>({x1, y1}))/padSize > 0.1) {

                double scale = gPad->GetAbsWNDC() * gPad->GetAbsHNDC();
                TArrow a(x1,y1,x2,y2, 0.025*scale, "|>");
                       a.SetLineColor(contour->GetLineColor());
                       a.SetFillColor(contour->GetLineColor());
                       a.SetAngle(45.);
                       a.Paint();

                x0 = x1;
                y0 = y1;
            }
        }
    }
}

OMU::TPoleZeroMap *OMU::TPoleZeroMap::Cast(Filter type, double Ts, OMU::TPoleZeroMap::FilterTransform method, double alpha)
{
    TPoleZeroMap *pzmap = NULL;
    if(IsDigital()) {

            if(type == Filter::Digital) return this;

            TF tf = OMU::TKFR::Analog(this->GetTF(), Ts, method, alpha);
            pzmap = new OMU::TPoleZeroMap(this->GetName() + TString("[analog]"), Filter::Analog, tf.first, tf.second, this->GetXaxis()->GetXmin(),  this->GetXaxis()->GetXmax(),  this->GetYaxis()->GetXmin(),  this->GetYaxis()->GetXmax());

    } else {

            if(type == Filter::Analog) return this;

            TF tf = OMU::TKFR::Digital(this->GetTF(), Ts, method, alpha);
            pzmap = new OMU::TPoleZeroMap(this->GetName() + TString("[digital]"), Filter::Digital, tf.first, tf.second, this->GetXaxis()->GetXmin(),  this->GetXaxis()->GetXmax(),  this->GetYaxis()->GetXmin(),  this->GetYaxis()->GetXmax());
    }

    pzmap->SetLineColor(UIUC::Invert(this->GetLineColor()));

    return pzmap;
}
