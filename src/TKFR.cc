/**
 **********************************************
 *
 * \file TKFR.cc
 * \brief Source code of the TKFR class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TKFR.h>
ClassImp(OMU::TKFR)

#include <UIUC/TFactory.h>
#include <OMU/MathUtils.h>
#include <TSpectrum.h>
#include <TPolyMarker.h>

#include <TFitResultPtr.h>
#include <TFitResult.h>

#include <kfr/all.hpp>
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

using namespace std;
using namespace kfr;

#if KFR_VERSION_MAJOR < 5
    #define KFR__EXPRESSION_HANDLE kfr::expression_pointer
    #define KFR__TO_HANDLE kfr::to_pointer
#else
    #define KFR__EXPRESSION_HANDLE kfr::expression_handle
    #define KFR__TO_HANDLE kfr::to_handle
#endif 

//
//
//

bool OMU::TKFR::IsAlreadyRunningMT = true;
std::atomic<int> OMU::TKFR::finishedWorkers = 0;
vector<double> OMU::TKFR::Get(ComplexPart part, const vector<OMU::Complex> &Xn)
{
        switch(part) {

                case ComplexPart::Real     : return OMU::Real(Xn);
                case ComplexPart::Imag     : return OMU::Imag(Xn);
                case ComplexPart::Magnitude: return OMU::Mag(Xn);
                case ComplexPart::Decibel  : return OMU::dB(Xn);
                case ComplexPart::Phase    : return OMU::Phase(Xn);
                case ComplexPart::Argument : return OMU::Argument(Xn);
                default:
                  throw std::invalid_argument("Unexpected FFT part requested..");
        }
}

vector<vector<double>> OMU::TKFR::Get(ComplexPart part, const vector<vector<OMU::Complex>> &Xn)
{
    vector<vector<double>> x;
    for(int i = 0, N = Xn.size(); i < N; i++)
        x.push_back(Get(part, Xn[i]));
    
    return x;
}

vector<OMU::Complex> OMU::TKFR::FFT(vector<OMU::Complex> xk, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    TString opt = option;
            opt.ToLower();

    //
    // Reshape input based on nFFT
    // NB: Numpy is doing -> nFFT = nFFT < 1 ? xk.size() : nFFT;
    nFFT = nFFT < 1 ? xk.size() : TMath::Min((int) nFFT, (int) xk.size());
    if(opt.Contains((char) Operator::Concatenate)) xk.resize(xk.size() + (OMU::IsPowerOf(xk.size(), 2) ? 0 : nFFT), 0);
    else if(!opt.Contains((char) Operator::Average)) xk.resize(nFFT, 0);

    int Nslices = xk.size()/nFFT;

    //
    // FFT by parts and average
    vector<OMU::Complex> Xk;
    if(opt.Contains((char) Operator::Average) && Nslices > 1) {

        for(int i = 0; i < Nslices; i++) {

            vector<OMU::Complex> xki = OMU::Slice(xk, i*nFFT, nFFT);
            vector<OMU::Complex> Xki = OMU::TKFR::FFT(xki, domain, opt, nFFT);

            if(Xk.size() == 0) Xk.resize(Xki.size());
            for(int k = 0, K = Xk.size(); k < K; k++)
                Xk[k] = Xk[k] + Xki[k] / ((double) Nslices);
        }

        return Xk;
    }

    //
    // Generic case.
    vector<vector<OMU::Complex>> xpk;
    for(int i = 0, I = Nslices; i < I; i++) // Slice into [k*n+p] pairs
        xpk.push_back(OMU::SliceXn(xk, I, i));
    
    vector<vector<OMU::Complex>> Xpk;
    switch(domain) {

        case Domain::ComplexFrequency :
        {
            Xk.resize(xk.size());
            for(int i = 0, I = Nslices; i < I; i++)
                Xpk.push_back(OMU::FFT(xpk[i]));
        }
        break;

        case Domain::Frequency :
        {
            Xk.resize(xk.size() > 0 ? xk.size()/2+1 : 0);
            for(int i = 0, I = Nslices; i < I; i++)
                Xpk.push_back(OMU::MirroringFFT( OMU::RealFFT(OMU::Real(xpk[i])) ));
        }
        break;

        default:
            throw std::invalid_argument("Wrong domain provided: expected \"Domain::Frequency\" or \"Domain::ComplexFrequency\".");
    }

    // Combine segments using Euler formula
    for(int k = 0, K = Xk.size(); k < K; k++) {

        Xk[k] = 0;
        for(int p = 0; p < Nslices; p++) {

            double cos = TMath::Cos(2*TMath::Pi()*p*k/(Nslices*nFFT));
            double sin = TMath::Sin(2*TMath::Pi()*p*k/(Nslices*nFFT));
            Xk[k] = Xk[k] + Complex(cos, -sin) * Xpk[p][UIUC::MathMod(k, Xpk[p].size())];
        }
    }

    // Concatenate segments using Euler formula
    if (opt.Contains((char) Operator::Orthonormal)) Xk = OMU::Multiply(Xk, 1./TMath::Sqrt(xk.size()));
    else if(opt.Contains((char) Operator::Forward)) Xk = OMU::Multiply(Xk, 1./(xk.size()));

    return Xk;
}

std::vector<OMU::Complex> OMU::TKFR::FFT(TH1 *h, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    TString opt = option;
            opt.ToLower();

    if(h == NULL) return {};

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h->GetXaxis();
    else if(opt.Contains("y")) axis = h->GetYaxis();
    else if(opt.Contains("z")) axis = h->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return {};
    }

    TString freqStr = nFFT < 1 ? "" : "-"+TString::Itoa(nFFT, 10); // Don't move this line (use input parameter for user's sanity, although it might change in between..)
    TString actionStr = "";
            actionStr = opt.Contains((char) Operator::Concatenate) ? "-"+enum2str(Operator::Concatenate) : actionStr;
            actionStr = opt.Contains((char) Operator::Average) ? "-"+enum2str(Operator::Average)     : actionStr;

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    
    nFFT = nFFT < 1 ? axis->GetNbins() : TMath::Min(nFFT, axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute FFT from histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");

    vector<double> xn = UIUC::TFactory::GetVectorContent(h);
    return OMU::TKFR::FFT(xn, domain, option, nFFT);
}

TH1* OMU::TKFR::FFT(ComplexPart part, TH1 *h, Option_t *option, int nFFT)
{
    TString opt = option;
            opt.ToLower();

    if(h == NULL) return NULL;

    TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    TString freqStr = nFFT < 1 ? "" : "-"+TString::Itoa(nFFT, 10); // Don't move this line (use input parameter for user's sanity, although it might change in between..)
    TString actionStr = "";
            actionStr = opt.Contains((char) Operator::Concatenate) ? "-"+enum2str(Operator::Concatenate) : actionStr;
            actionStr = opt.Contains((char) Operator::Average) ? "-"+enum2str(Operator::Average)     : actionStr;

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    
    nFFT = nFFT < 1 ? axis->GetNbins() : TMath::Min(nFFT, axis->GetNbins());
    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute FFT from histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn = UIUC::TFactory::GetVectorContent(h);
    vector<OMU::Complex> Xn = OMU::TKFR::FFT(xn, OMU::TKFR::Domain::Frequency, option, nFFT);
    if(opt.Contains((char) Operator::Concatenate)) nFFT = axis->GetNbins();
    
    double df = fs/nFFT;              // Spectral resolution

    h0->SetName(h->GetName() + (TString) ":"+OMU::TKFR::enum2str(part) + freqStr + actionStr);
    h0->SetTitle(Form("Corrected Frequency Spectrum (f_{s} = %.2f Hz, nFFT = %d d_{f} = %.2f); Frequency (Hz)", fs, nFFT, df));

    // Make sure outgoing histogram ranges between [0;fNyquist]
    h0->SetBins(Xn.size(), 0, fs/2+1);
    axis->SetRangeUser(0, fs/2+1);
    
    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, OMU::TKFR::Get(part, Xn));
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }
    
    return h0;
}

vector<OMU::Complex> OMU::TKFR::iFFT(vector<OMU::Complex> Xk, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    TString opt = option;
            opt.ToLower();

    //
    // Reshape input based on nFFT
    switch(domain) {

        case Domain::ComplexFrequency :
    
            nFFT = nFFT < 1 ? Xk.size()   : TMath::Min((int) nFFT, (int) Xk.size());
            Xk.resize(nFFT, 0);
            break;

        case Domain::Frequency :

            nFFT = nFFT < 1 ? Xk.size()-1 : TMath::Min((int) nFFT, (int) Xk.size()-1);
            Xk.resize(nFFT+1, 0);
            break;

        default:
            throw std::invalid_argument("Unexpected domain provided..");
    }

    //
    // Generic case
    vector<OMU::Complex> xk;
    switch(domain) {

        case Domain::ComplexFrequency :
            xk = OMU::iFFT(OMU::IsOneSidedFFT(Xk) ? OMU::MirroringFFT(Xk) : Xk);
            break;

        case Domain::Frequency : 
            xk = OMU::Complexify(OMU::iRealFFT(Xk));
            break;

        default:
            throw std::invalid_argument("Wrong domain provided: expected \"Domain::Frequency\" or \"Domain::ComplexFrequency\".");
    }
    
    // Concatenate segments using Euler formula
    if (opt.Contains((char) Operator::Orthonormal))  xk = OMU::Multiply(xk, 1./TMath::Sqrt(xk.size()));
    else if(!opt.Contains((char) Operator::Forward)) xk = OMU::Multiply(xk, 1./(xk.size()));
   
    return xk;
}

std::vector<std::vector<OMU::Complex>> OMU::TKFR::STFT(const std::vector<OMU::Complex> &x, double fs, OMU::TKFR::Domain domain, Option_t *option, int nFFT, double noverlap, WindowType window_hamming, double ab, WindowSymmetry symmetry)
{
    throw std::invalid_argument("STFT is not implemented yet.");
    return {};
}

TH2* OMU::TKFR::STFT(ComplexPart part, TH2 *h, OMU::TKFR::Domain domain, Option_t *option, int nFFT, double noverlap, WindowType window, double ab , WindowSymmetry symmetry)
{
    STFT(UIUC::TFactory::GetVectorContent(h), h->GetXaxis()->GetBinWidth(1), domain, option, nFFT, noverlap, window, ab, symmetry);
    return NULL;
}

std::vector<std::vector<OMU::Complex>> OMU::TKFR::STFT(TH2 *h, OMU::TKFR::Domain domain, Option_t *option, int nFFT, double noverlap, WindowType window, double ab, WindowSymmetry symmetry)
{
    throw std::invalid_argument("STFT is not implemented yet.");
    return {};
}

std::vector<OMU::Complex> OMU::TKFR::iSTFT(const std::vector<std::vector<OMU::Complex>> &X, double fs, OMU::TKFR::Domain domain, Option_t *option, int nFFT, double noverlap, WindowType window, double ab, WindowSymmetry symmetry)
{
    throw std::invalid_argument("iSTFT is not implemented yet.");
    return {};
}

bool OMU::TKFR::NOLA(double nFFT, double noverlap, WindowType window, double ab, WindowSymmetry symmetry, double tolerance)
{
    std::vector<double> w = OMU::TKFR::Window(nFFT, window, ab, symmetry);

    int H = nFFT*(1 - noverlap);
    std::vector<double> NOLA(H,0);

    for(int i = 0, N = nFFT/H + (UIUC::MathMod(nFFT, H) ? 1 : 0); i < nFFT/H; i++) {

        std::vector<double> _w(w.begin() + i*H, w.begin() + TMath::Min((int) (i+1)*H, (int) w.size()));
                            _w = OMU::Power(_w, 2);
                            _w.resize(H, 0);

        NOLA = OMU::Add(NOLA, _w);
    }

    return OMU::Min(NOLA) > tolerance;
}

bool OMU::TKFR::COLA(double nFFT, double noverlap, WindowType window, double ab, WindowSymmetry symmetry, double tolerance)
{
    std::vector<double> w = OMU::TKFR::Window(nFFT, window, ab, symmetry);

    int H = nFFT*(1 - noverlap);
    std::vector<double> COLA(H,0);

    for(int i = 0, N = nFFT/H + (UIUC::MathMod(nFFT, H) ? 1 : 0); i < nFFT/H; i++) {

        std::vector<double> _w(w.begin() + i*H, w.begin() + TMath::Min((int) (i+1)*H, (int) w.size()));
                            _w.resize(H, 0);

        COLA = OMU::Add(COLA, _w);
    }

    std::vector<double> dx = OMU::Subtract(COLA, OMU::Median(COLA));
    return OMU::Max(OMU::Abs(dx)) < tolerance;
}


OMU::SOS OMU::TKFR::Biquad(FilterType filter, double f, double fs, double Q, double gain)
{
    OMU::SOS sos;
    switch(filter)
    {
        case FilterType::LowPass:
            sos.push_back(kfr::biquad_lowpass (f/fs, Q));
            break;

        case FilterType::HighPass:
            sos.push_back(kfr::biquad_highpass(f/fs, Q));
            break;

        case FilterType::BandPass:
            sos.push_back(kfr::biquad_bandpass(f/fs, Q));
            break;

        case FilterType::AllPass:
            sos.push_back(kfr::biquad_allpass(f/fs, Q));
            break;

        case FilterType::HighShelf:
            sos.push_back(kfr::biquad_highshelf(f/fs, gain));
            break;

        case FilterType::LowShelf:
            sos.push_back(kfr::biquad_lowshelf(f/fs, gain));
            break;

        case FilterType::Peak:
            sos.push_back(kfr::biquad_peak(f/fs, Q, gain));
            break;
        
        case FilterType::Notch: [[fallthrough]];
        case FilterType::BandStop:
            sos.push_back(kfr::biquad_notch(f/fs, Q));
            break;
        
        default: [[fallthrough]];
        case FilterType::None:
            break;
    }

    return sos;
}

OMU::SOS OMU::TKFR::Biquad(int N, FilterDesign design, const std::pair<double, double> &RpRs, OMU::TKFR::FilterType filter, const std::vector<double> &f, double fs)
{
    switch(design) {

            case FilterDesign::Butterworth:
                return OMU::TKFR::Butterworth(N, filter, f, fs);
            case FilterDesign::Elliptic: 
                return OMU::TKFR::Elliptic(N, filter, RpRs.first, RpRs.second, f, fs);
            case FilterDesign::ChebyshevI: 
                return OMU::TKFR::ChebyshevI(N, filter, RpRs.second, f, fs);
            case FilterDesign::ChebyshevII: 
                return OMU::TKFR::ChebyshevII(N, filter, RpRs.first, f, fs);
            case FilterDesign::Bessel: 
                return OMU::TKFR::Bessel(N, filter, f, fs);

            default: [[fallthrough]];
            case FilterDesign::None: return OMU::SOS();
    }
}

OMU::SOS OMU::TKFR::Biquad(const std::vector<double> &wp, const std::vector<double> &ws, FilterDesign design, const std::pair<double,double> &RpRs, OMU::TKFR::FilterType type, const std::vector<double> &f, double fs)
{
    switch(design) {

            case FilterDesign::Butterworth: 
                return OMU::TKFR::Butterworth(wp, ws, type, RpRs, f, fs);
            case FilterDesign::Elliptic: 
                return OMU::TKFR::Elliptic(wp, ws, type, RpRs, f, fs);
            case FilterDesign::ChebyshevI: 
                return OMU::TKFR::ChebyshevI(wp, ws, type, RpRs, f, fs);
            case FilterDesign::ChebyshevII: 
                return OMU::TKFR::ChebyshevII(wp, ws, type, RpRs, f, fs);
            case FilterDesign::Bessel: 
                throw std::invalid_argument("Bessel order calculation not implemented.");
                //return OMU::TKFR::Bessel(wp, ws, filter, f, fs);

            default: [[fallthrough]];
            case FilterDesign::None: return OMU::SOS();
    }
}

OMU::SOS OMU::TKFR::Notch(const std::vector<double> &f, const std::pair<double,double> &df, double fs, OMU::TKFR::FilterDesign filterDesign, const std::pair<double, double> &filterRpRs, int N)
{
    OMU::SOS sos;
    for(int i = 0; i < f.size(); i++) {

        double f0  = f[i];
        double fLow   = (f0 - df.first ) / (fs/2);
        double fHigh  = (f0 + df.second ) / (fs/2);

        OMU::SOS _sos = Biquad(N, filterDesign, filterRpRs, FilterType::BandStop, std::vector<double>({fLow, fHigh}), fs);
        sos.insert(sos.end(), _sos.begin(), _sos.end());
    }

    return sos;
}

OMU::SOS OMU::TKFR::Notch(const std::vector<double> &f, const std::pair<double,double> &df, double fs, OMU::TKFR::FilterDesign filterDesign, const std::pair<double, double> &filterRpRs)
{
    OMU::SOS sos;
    for(int i = 0; i < f.size(); i++) {

        double f0  = f[i];
        double df1 = df.first;
        double df2 = df.second;
        
        if(df1 < df2) {

            double tmp = df2;
            df2 = df1; df1 = tmp;
        }

        double fLow   = (f0 - df1) / (fs/2);
        double fHigh  = (f0 + df1) / (fs/2);
        double fLow2  = (f0 - df2) / (fs/2);
        double fHigh2 = (f0 + df2) / (fs/2);

        OMU::SOS _sos = Biquad(std::vector<double>({fLow, fHigh}), vector<double>({fLow2, fHigh2}), filterDesign, filterRpRs, FilterType::BandStop, std::vector<double>({fLow, fHigh}), fs);
        sos.insert(sos.end(), _sos.begin(), _sos.end());
    }

    return sos;
}

OMU::SOS OMU::TKFR::Elliptic(int N, FilterType filter, double rp, double rs, const vector<double> &f, double fs)
{
    switch(filter)
    {
        case FilterType::LowPass:
            return to_sos(kfr::iir_lowpass (kfr::elliptic<double>(N, rp, rs), f[0], fs));
        case FilterType::HighPass:
            return to_sos(kfr::iir_highpass(kfr::elliptic<double>(N, rp, rs), f[0], fs));
        case FilterType::BandPass:
            return to_sos(kfr::iir_bandpass(kfr::elliptic<double>(N, rp, rs), f[0], f[1], fs));
        case FilterType::BandStop:
            return to_sos(kfr::iir_bandstop(kfr::elliptic<double>(N, rp, rs), f[0], f[1], fs));

        default: [[fallthrough]];
        case FilterType::None:
          throw std::invalid_argument("Unexpected filter provided.");
            return OMU::SOS();
    }
}

OMU::SOS OMU::TKFR::Elliptic(const std::vector<double> &wp, const std::vector<double> &ws, FilterType filter, const std::pair<double,double> &RpRs, const vector<double> &f, double fs)
{
    int N = kfr::elliptic_order(wp, ws, RpRs.first, RpRs.second);
    return OMU::TKFR::Elliptic(N, filter, RpRs.first, RpRs.second, f, fs);
}

OMU::SOS OMU::TKFR::Butterworth(int N, FilterType filter, const vector<double> &f, double fs)
{
    switch(filter)
    {
        case FilterType::LowPass:
            return to_sos(kfr::iir_lowpass (kfr::butterworth<double>(N), f[0], fs));
        case FilterType::HighPass:
            return to_sos(kfr::iir_highpass(kfr::butterworth<double>(N), f[0], fs));
        case FilterType::BandPass:
            return to_sos(kfr::iir_bandpass(kfr::butterworth<double>(N), f[0], f[1], fs));
        case FilterType::BandStop:
            return to_sos(kfr::iir_bandstop(kfr::butterworth<double>(N), f[0], f[1], fs));

        default: [[fallthrough]];
        case FilterType::None:
          throw std::invalid_argument("Unexpected filter provided.");
            return OMU::SOS();
    }
}

OMU::SOS OMU::TKFR::Butterworth(const std::vector<double> &wp, const std::vector<double> &ws, FilterType filter, const std::pair<double,double> &RpRs, const vector<double> &f, double fs)
{
    int N = kfr::butterworth_order(wp, ws, RpRs.first, RpRs.second);
    return OMU::TKFR::Butterworth(N, filter, f, fs);
}

OMU::SOS OMU::TKFR::ChebyshevI(int N, FilterType filter, double rp, const vector<double> &f, double fs)
{
    switch(filter) {
        case FilterType::LowPass:
            return to_sos(kfr::iir_lowpass (kfr::chebyshev1<double>(N, rp), f[0], fs));
        case FilterType::HighPass:
            return to_sos(kfr::iir_highpass(kfr::chebyshev1<double>(N, rp), f[0], fs));
        case FilterType::BandPass:
            return to_sos(kfr::iir_bandpass(kfr::chebyshev1<double>(N, rp), f[0], f[1], fs));
        case FilterType::BandStop:
            return to_sos(kfr::iir_bandstop(kfr::chebyshev1<double>(N, rp), f[0], f[1], fs));

        default: [[fallthrough]];
        case FilterType::None:
          throw std::invalid_argument("Unexpected filter provided.");
            return OMU::SOS();
    }
}

OMU::SOS OMU::TKFR::ChebyshevI(const std::vector<double> &wp, const std::vector<double> &ws, FilterType filter, const std::pair<double,double> &RpRs, const vector<double> &f, double fs)
{
    int N = kfr::chebyshev_order(wp, ws, RpRs.first, RpRs.second);
    return OMU::TKFR::ChebyshevI(N, filter, RpRs.first, f, fs);
}

OMU::SOS OMU::TKFR::ChebyshevII(int N, FilterType filter, double rs, const vector<double> &f, double fs)
{
    switch(filter) {
        case FilterType::LowPass:
            return to_sos(kfr::iir_lowpass (kfr::chebyshev2<double>(N, rs), f[0], fs));
        case FilterType::HighPass:
            return to_sos(kfr::iir_highpass(kfr::chebyshev2<double>(N, rs), f[0], fs));
        case FilterType::BandPass:
            return to_sos(kfr::iir_bandpass(kfr::chebyshev2<double>(N, rs), f[0], f[1], fs));
        case FilterType::BandStop:
            return to_sos(kfr::iir_bandstop(kfr::chebyshev2<double>(N, rs), f[0], f[1], fs));

        default: [[fallthrough]];
        case FilterType::None:
          throw std::invalid_argument("Unexpected filter provided.");
            return OMU::SOS();
    }
}

OMU::SOS OMU::TKFR::ChebyshevII(const std::vector<double> &wp, const std::vector<double> &ws, FilterType filter, const std::pair<double,double> &RpRs, const vector<double> &f, double fs)
{
    int N = kfr::chebyshev_order(wp, ws, RpRs.first, RpRs.second);
    return OMU::TKFR::ChebyshevII(N, filter, RpRs.second, f, fs);
}

OMU::SOS OMU::TKFR::Bessel(int N, FilterType filter, const vector<double> &f, double fs)
{
    switch(filter) {
        case FilterType::LowPass:
            return to_sos(kfr::iir_lowpass(kfr::bessel<double>(N), f[0], fs));
        case FilterType::HighPass:
            return to_sos(kfr::iir_highpass(kfr::bessel<double>(N), f[0], fs));
        case FilterType::BandPass:
            return to_sos(kfr::iir_bandpass(kfr::bessel<double>(N), f[0], f[1], fs));
        case FilterType::BandStop:
            return to_sos(kfr::iir_bandstop(kfr::bessel<double>(N), f[0], f[1], fs));

        default: [[fallthrough]];
        case FilterType::None:
          throw std::invalid_argument("Unexpected filter provided.");
            return OMU::SOS();
    }
}

vector<double> OMU::TKFR::Detrend(const vector<double> &Xn, Trend detrend)
{
    if(detrend == Trend::None) return Xn;

    std::pair<double, double> ab = OMU::Regression(Xn);

    std::vector<double> regression(Xn.size(), 0);
    for(int i = 0, N = regression.size(); i < N; i++)
        regression[i] = i*ab.first + ab.second;

    return OMU::Subtract(Xn, regression);
}

vector<OMU::Complex> OMU::TKFR::Detrend(const vector<OMU::Complex> &Zn, Trend detrend)
{
    if(detrend == Trend::None) return Zn;

    return OMU::Complexify(
        OMU::TKFR::Detrend(OMU::Real(Zn), detrend),
        OMU::TKFR::Detrend(OMU::Imag(Zn), detrend)
    );
}

vector<double> OMU::TKFR::Noise(int N, double amplitude, double mean0, double sigma0, double alpha, double mean1, double sigma1)
{
    alpha = TMath::Min(1.0, TMath::Max(0.0, alpha));
    double xMin = TMath::Min(
        mean0 - 6*TMath::Abs(sigma0), 
        mean1 - 6*TMath::Abs(sigma1)
    );

    double xMax = TMath::Max(
        mean0 + 6*TMath::Abs(sigma0), 
        mean1 + 6*TMath::Abs(sigma1)
    );

    double amplitude0 = UIUC::IsNaN(mean0) || UIUC::IsNaN(sigma0) || UIUC::EpsilonEqualTo(sigma0, 0) ? 0 : amplitude;
           mean0      = UIUC::IsNaN(mean0) ? 0 : mean0;
           sigma0     = UIUC::IsNaN(sigma0) || UIUC::EpsilonEqualTo(sigma0, 0) ? 1 : sigma0;

    double amplitude1 = UIUC::IsNaN(mean1) || UIUC::IsNaN(sigma1) || UIUC::EpsilonEqualTo(sigma1, 0) ? 0 : amplitude;
           mean1      = UIUC::IsNaN(mean1) ? 0 : mean1;
           sigma1     = UIUC::IsNaN(sigma1) || UIUC::EpsilonEqualTo(sigma1, 0) ? 1 : sigma1;

    TF1 *formula = new TF1("white_noise["+UIUC::GetRandomStr(6)+"]", "[6]*gaus(x,[0],[1],[2]) + (1 - [6])*gaus(x,[3],[4],[5])", xMin, xMax);
         formula->SetParameter(0, amplitude0);
         formula->SetParameter(1, mean0);
         formula->SetParameter(2, sigma0);
         formula->SetParameter(3, amplitude1);
         formula->SetParameter(4, mean1);
         formula->SetParameter(5, sigma1);
         formula->SetParameter(6, alpha);

    std::vector<double> noise(N, 0);
    bool bNoise0 = !UIUC::EpsilonEqualTo(amplitude0, 0) && !UIUC::EpsilonEqualTo(alpha, 0) && !UIUC::EpsilonEqualTo(sigma0, 0);
    bool bNoise1 = !UIUC::EpsilonEqualTo(amplitude1, 0) && !UIUC::EpsilonEqualTo(alpha, 1) && !UIUC::EpsilonEqualTo(sigma1, 0);
    if(bNoise0 || bNoise1) noise = UIUC::TFactory::RandomVector(N, formula);
    else noise = OMU::Add(noise, amplitude0*alpha*mean0 + amplitude1*(1-alpha)*mean1);

    std::cout << bNoise0 << bNoise1 << std::endl;
    formula->Delete();
    return noise;
}

TH1* OMU::TKFR::Noise(TString name, TString title, double fs, double duration, double amplitude, double mean1, double sigma1, double alpha, double mean2, double sigma2)
{
    TH1D *h = new TH1D(name, title, fs * duration, 0, duration);
    return (TH1*) UIUC::TFactory::AddVectorContent((TH1*) h, OMU::TKFR::Noise(fs*duration, amplitude, mean1, sigma1, alpha, mean2, sigma2));
}

vector<double> OMU::TKFR::Filtering(const vector<double> &x, vector<OMU::SOS> sos, Direction direction)
{
    switch(direction)
    {
        case Direction::Forward:
        {
            // Prepare cascade filter based on SOS input
            OMU::SOS cascade;
            for(int i = 0, I = sos.size(); i < I; i++) {       
                for(int j = 0, J = sos[i].size(); j < J; j++) {

                    cascade.push_back(sos[i][j]);
                }
            }
 
            kfr::univector<double> y = OMU::kfrvector(x);

                 if(cascade.size() <= 1    ) y = kfr::biquad<1>    (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 2    ) y = kfr::biquad<2>    (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 4    ) y = kfr::biquad<4>    (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 8    ) y = kfr::biquad<8>    (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 16   ) y = kfr::biquad<16>   (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 32   ) y = kfr::biquad<32>   (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 64   ) y = kfr::biquad<64>   (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 128  ) y = kfr::biquad<128>  (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 256  ) y = kfr::biquad<256>  (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 512  ) y = kfr::biquad<512>  (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 1024 ) y = kfr::biquad<1024> (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 2048 ) y = kfr::biquad<2048> (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 4096 ) y = kfr::biquad<4096> (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 8192 ) y = kfr::biquad<8192> (cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 16384) y = kfr::biquad<16384>(cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 32768) y = kfr::biquad<32768>(cascade, KFR__TO_HANDLE(y));
            else if(cascade.size() <= 65536) y = kfr::biquad<65536>(cascade, KFR__TO_HANDLE(y));
            else throw std::invalid_argument("Too high biquad order");
            
            return OMU::stdvector(y);
        }

        case Direction::Backward:
        {
            vector<double> y = OMU::Flip(x);
                           y = OMU::TKFR::Filtering(y, sos, Direction::Forward);

            return OMU::Flip(y);
        }

        case Direction::Both: [[fallthrough]];
        case Direction::Orthonormal: [[fallthrough]];
        case Direction::BackwardForward: [[fallthrough]];
        case Direction::ForwardBackward: 
        {
            vector<double> y = x;
                           y = OMU::TKFR::Filtering(y, sos, Direction::Forward);
                           y = OMU::TKFR::Filtering(y, sos, Direction::Backward);

            return y;
        }

        default:
        {
            throw std::invalid_argument("Unexpected filter provided.");
            return vector<double>();
        }
    }
}

std::vector<double> OMU::TKFR::Interpolate(int N, const std::vector<double> &y, ROOT::Math::Interpolation::Type type)
{
    std::vector<double> x0 = OMU::Range(N+1, 1./(N));
    std::vector<double> x  = OMU::Range(y.size(), 1./(y.size()-1));

    return OMU::TKFR::Interpolate(x0, x, y, type);
}

vector<double> OMU::TKFR::Interpolate(const vector<double> &x0, const ROOT::Math::Interpolator &itp)
{
    vector<double> y0;
    for(int i = 0, N = x0.size(); i < N; i++)
        y0.push_back(itp.Eval(x0[i]));

    return y0;
}

TH1D *OMU::TKFR::Interpolate(const std::vector<double> &x0, TH1D *h, ROOT::Math::Interpolation::Type type)
{ 
    if(x0.size() == 0) return NULL;

    std::vector<double> x = UIUC::TFactory::GetVectorAxis(h);
    std::vector<double> y = UIUC::TFactory::GetVectorContent(h);
        
    if(!OMU::Contains(x0[0], x)) return NULL;
    if(!OMU::Contains(x0[x0.size() - 1], x)) return NULL;

    std::vector<double> y0 = OMU::TKFR::Interpolate(x0, ROOT::Math::Interpolator(x, y, type));
    return (TH1D*) UIUC::TFactory::Histogram((TString) h->GetName() + ":x-interpolate", h->GetTitle(), x0, y0);
}

vector<OMU::Complex> OMU::TKFR::Whiten(const vector<OMU::Complex> &x, const vector<double> &ASD, double fs, Option_t *option, int nFFT, double timeshift, double phase)
{
    return OMU::Complexify(
        OMU::TKFR::Whiten(OMU::Real(x), ASD, fs, option, nFFT, timeshift, phase),
        OMU::TKFR::Whiten(OMU::Imag(x), ASD, fs, option, nFFT, timeshift, phase)
    );
}
vector<double> OMU::TKFR::Whiten(const vector<double> &x, const vector<double> &ASD, double fs, Option_t *option, int nFFT, double timeshift, double phase)
{
    vector<OMU::Complex> Xn = OMU::TKFR::FFT(x, OMU::TKFR::Domain::Frequency, option, nFFT);
    if(!UIUC::IsNaN(timeshift)) Xn = OMU::TimeShift(Xn, fs, timeshift);
    if(!UIUC::IsNaN(phase)) Xn = OMU::PhaseShift(Xn, phase);
    
    vector<double> interpASD = OMU::TKFR::Interpolate(
            OMU::Range( Xn.size(), 0., fs/2.), 
            OMU::Range(ASD.size(), 0., fs/2.), ASD
        );

    interpASD = OMU::Multiply(interpASD, TMath::Sqrt(fs/2)); // .. get normalization right
    // @TODO, please consider to recompute DC component..
    
    return OMU::Real(TKFR::iFFT(OMU::Divide(Xn, interpASD), OMU::TKFR::Domain::Frequency, option));
}

vector<OMU::Complex> OMU::TKFR::Whiten(const vector<OMU::Complex> &x, double fs, Option_t *option, int nFFT, double timeshift, double phase, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry symmetry)
{
    vector<double> ASD = OMU::TKFR::ASD(x, fs, OMU::TKFR::Domain::Frequency, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    return OMU::Complexify(
        OMU::TKFR::Whiten(OMU::Real(x), ASD, fs, option, 0, timeshift, phase),
        OMU::TKFR::Whiten(OMU::Imag(x), ASD, fs, option, 0, timeshift, phase)
    );
}

vector<double> OMU::TKFR::Whiten(const vector<double> &x, double fs, Option_t *option, int nFFT, double timeshift, double phase, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry symmetry)
{
    vector<double> ASD = OMU::TKFR::ASD(x, fs, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    return OMU::TKFR::Whiten(x, ASD, fs, option, 0, timeshift, phase);
}

TH1* OMU::TKFR::Whiten(TH1 *h, TH1 *hASD, Option_t *option, int nFFT, double timeshift, double phase)
{
    TString opt = option;
            opt.ToLower();

    TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":"+opt+"-whiten");

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }
 
    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? axis->GetNbins() : TMath::Min(nFFT, axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Whiten histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    
    vector<double> x         = UIUC::TFactory::GetVectorContent(h);
    vector<double> xWhitened = OMU::TKFR::Whiten(x, UIUC::TFactory::GetVectorContent(hASD), fs, option, 0, timeshift, phase);
    if(opt.Contains((char) Operator::Concatenate)) nFFT = axis->GetNbins();

    if(opt.Contains("x")) h0 = UIUC::TFactory::SetVectorContent(h0, xWhitened);
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

std::vector<OMU::Complex> OMU::TKFR::WienerFiltering(const std::vector<OMU::Complex> &x, double fs, std::vector<double> PSD /* noise PSD */, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    std::vector<OMU::Complex> Xn = OMU::TKFR::FFT(x, domain, option, nFFT);
    if(PSD.size()) {

        switch(domain) {

            case Domain::Frequency: break;
            case Domain::ComplexFrequency: PSD = OMU::MirroringPSD(PSD);
                break;
            default:
                throw std::invalid_argument( "Unexpected domain provided.");
        }  

        PSD = OMU::TKFR::Interpolate(OMU::Range( Xn.size(), 0., fs), OMU::Range(PSD.size(), 0., fs), PSD);
    }

    std::vector<double> Cn = OMU::TKFR::PSD(x, fs, domain, option, nFFT);
    std::vector<double> Sn = OMU::Subtract(Cn, PSD);
    
    return OMU::TKFR::iFFT(OMU::Multiply(Xn, OMU::Divide(Sn, Cn)));
}

TH1* OMU::TKFR::Whiten(TH1 *h, Option_t *option, int nFFT, double timeshift, double phase, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry symmetry)
{
    TH1D *hASD = (TH1D*) OMU::TKFR::ASD(h, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    return OMU::TKFR::Whiten(h, hASD, option, nFFT, timeshift, phase);
}

std::pair<vector<OMU::Complex>, double> OMU::TKFR::SNR(const vector<OMU::Complex> &x, const vector<OMU::Complex> &y, double fs, vector<double> PSD, OMU::TKFR::Domain domain, Option_t *option, int nFFT, double fLow, double fHigh)
{
    if(x.size() != y.size())
        throw std::invalid_argument("vectors are not same size: x("+TString::Itoa(x.size(),10)+") != y("+TString::Itoa(y.size(),10)+")");
    
    TString opt = option;
            opt.ToLower();

    //
    // Noise weighted inner product x and y
    std::vector<OMU::Complex> Xn = OMU::Divide(OMU::TKFR::FFT(x, domain, option, nFFT), fs);
                              Xn = OMU::TKFR::SpectrumCut(Xn, fLow, fHigh);

    std::vector<OMU::Complex> Yn = OMU::Divide(OMU::TKFR::FFT(y, domain, option, nFFT), fs);
                              Yn = OMU::TKFR::SpectrumCut(Yn, fLow, fHigh);

    if(PSD.size()) {

        switch(domain) {

            case Domain::Frequency: break;
            case Domain::ComplexFrequency: PSD = OMU::MirroringPSD(PSD);
                break;
            default:
                throw std::invalid_argument( "Unexpected domain provided.");
        }  

        PSD = OMU::TKFR::Interpolate(OMU::Range( Xn.size(), 0., fs), OMU::Range(PSD.size(), 0., fs), PSD);
    }

    std::vector<OMU::Complex> optimalSignalFFT   = OMU::Multiply(Xn, OMU::CConj(Yn));
    if(PSD.size()) optimalSignalFFT = OMU::Divide(optimalSignalFFT, PSD, true);
    
    std::vector<OMU::Complex> optimalSignal = OMU::Multiply(fs, OMU::TKFR::iFFT(optimalSignalFFT, domain, option, nFFT));
    
    //
    // Optimal inner product
    // NB: no need to apply iFFT in that case.. just renormalize properly and integrate
    std::vector<OMU::Complex> optimalTemplateFFT = OMU::Multiply(Yn, OMU::CConj(Yn));
    if(PSD.size()) optimalTemplateFFT = OMU::Divide(optimalTemplateFFT, PSD, true);
    nFFT = nFFT == 0 ? optimalTemplateFFT.size() : nFFT;

    double a = 1 + (domain == Domain::Frequency ? 1 : 0); // one-sided/two-sided unfolding
    
    double df = fs/nFFT;
    double sigma2 = a * df * OMU::Mag(OMU::Sum(optimalTemplateFFT));
    double sigma  = TMath::Sqrt(sigma2)/TMath::Sqrt(2);

    //
    // Signal-to-noise ratio
    std::vector<OMU::Complex> SNR = OMU::Divide(optimalSignal, sigma);
    return std::make_pair(OMU::Roll(SNR, optimalSignal.size()/2.), sigma);
}

std::vector<OMU::Complex> OMU::TKFR::SpectrumCut(const std::vector<OMU::Complex> &input, double fs, double fLow, double fHigh)
{
        if(input.size() == 0) return {};
        
        std::vector<OMU::Complex> output(input.begin(), input.end());
        bool isComplex = UIUC::MathMod(input.size(), 2) == 0;
        
        int nFFT = isComplex ? input.size() : (input.size()-1)*2;
        double df = fs / nFFT;

        for(int i = 1; i <= nFFT/2; i++) {

                if(!UIUC::IsNaN(fLow) && i < fLow/df) {
                        output[i] = 0;
                        if(isComplex) output[output.size()-i] = 0;
                }

                if(!UIUC::IsNaN(fHigh) && i >= fHigh/df) {
                        output[i] = 0;
                        if(isComplex) output[output.size()-i] = 0;
                }
        }

        return input;
}

std::vector<OMU::Complex> OMU::TKFR::NoiseWeightedInnerProduct(const std::vector<OMU::Complex> &x, const std::vector<OMU::Complex> &y, double fs, std::vector<double>  PSD, OMU::TKFR::Domain domain, Option_t *option, int nFFT, double fLow, double fHigh)
{
    std::vector<OMU::Complex> Xn = OMU::Divide(OMU::TKFR::FFT(x, domain, option, nFFT), fs);
                              Xn = OMU::TKFR::SpectrumCut(Xn, fs, fLow, fHigh);

    std::vector<OMU::Complex> Yn = OMU::Divide(OMU::TKFR::FFT(y, domain, option, nFFT), fs);
                              Yn = OMU::TKFR::SpectrumCut(Xn, fs, fLow, fHigh);

    if(PSD.size()) {

        switch(domain) {

            case Domain::Frequency: break;
            case Domain::ComplexFrequency: PSD = OMU::MirroringPSD(PSD);
                break;
            default:
                throw std::invalid_argument( "Unexpected domain provided.");
        }  

        PSD = OMU::TKFR::Interpolate(OMU::Range( Xn.size(), 0., fs), OMU::Range(PSD.size(), 0., fs), PSD);
    }
    
    std::vector<OMU::Complex> optimalSignalFFT   = OMU::Multiply(Xn, OMU::CConj(Yn));
    if(PSD.size()) optimalSignalFFT = OMU::Divide(optimalSignalFFT, PSD, true);

    return OMU::Multiply(4.*fs, OMU::TKFR::iFFT(optimalSignalFFT, domain, option, nFFT));
}

std::vector<double> OMU::TKFR::FrequencyMoment(int n, const std::vector<OMU::Complex> &h, double fs, std::vector<double>  PSD, OMU::TKFR::Domain domain, Option_t *option, int nFFT, double fLow, double fHigh)
{
    std::vector<OMU::Complex> Hn = OMU::Divide(OMU::TKFR::FFT(h, domain, option, nFFT), fs);
                              Hn = OMU::TKFR::SpectrumCut(Hn, fs, fLow, fHigh);

    if(PSD.size()) {

        switch(domain) {

            case Domain::Frequency: break;
            case Domain::ComplexFrequency: PSD = OMU::MirroringPSD(PSD);
                break;
            default:
                throw std::invalid_argument( "Unexpected domain provided.");
        }  

        PSD = OMU::TKFR::Interpolate(OMU::Range( Hn.size(), 0., fs), OMU::Range(PSD.size(), 0., fs), PSD);
    }
    
    std::vector<double> fn = OMU::Power(OMU::Range( Hn.size(), 0., fs), n);
    std::vector<OMU::Complex> fnHn = OMU::Multiply(fn, Hn);
    std::vector<OMU::Complex> optimalSignalFFT   = OMU::Multiply(fnHn, OMU::CConj(Hn));
    if(PSD.size()) optimalSignalFFT = OMU::Divide(optimalSignalFFT, PSD, true);

    return OMU::Multiply(4.*fs, OMU::Real(OMU::TKFR::iFFT(optimalSignalFFT, OMU::TKFR::Domain::Frequency, option, nFFT)));
}

std::pair<vector<OMU::Complex>, double> OMU::TKFR::SNR(TH1 *h1, TH1 *h2, TH1 *hPSD, Option_t *option, int nFFT, double fLow, double fHigh)
{
    TString opt = option;
            opt.ToLower();

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h1->GetXaxis();
    else if(opt.Contains("y")) axis = h1->GetYaxis();
    else if(opt.Contains("z")) axis = h1->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + ((TString) h1->GetName()+ ":"+opt+"-snr") + " (Please use \"x\",\"y\",\"z\")");
        return {{}, NAN};
    }
 
    double dt = axis->GetBinWidth(1);  // Time resolution
    double fs = 1./dt;                 // Sample frequency

    vector<double> x    = UIUC::TFactory::GetVectorContent(h1);
    vector<double> y    = UIUC::TFactory::GetVectorContent(h2);
    vector<double> PSD  = UIUC::TFactory::GetVectorContent(hPSD);

    return OMU::TKFR::SNR(x, y, fs, PSD, option, nFFT, fLow, fHigh);
}

std::pair<TH1*, double> OMU::TKFR::SNR (ComplexPart part, TH1 *h1, TH1 *h2, TH1 *hPSD, Option_t *option, int nFFT, double fLow, double fHigh)
{
    TString opt = option;
            opt.ToLower();

    TH1* h0 = (TH1*) h1->Clone((TString) h1->GetName()+ ":"+opt+"-snr");
         h0->SetTitle("Signal-To-Noise Ratio");
    
    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return {NULL, NAN};
    }
 
    double dt = axis->GetBinWidth(1);  // Time resolution
    double fs = 1./dt;                 // Sample frequency
    nFFT = nFFT < 1 ? axis->GetNbins() : TMath::Min(nFFT, axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute SNR from histogram "+h1->GetName()+" and "+h2->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");

    vector<double> x    = UIUC::TFactory::GetVectorContent(h1);
    vector<double> y    = UIUC::TFactory::GetVectorContent(h2);
    vector<double> PSD  = UIUC::TFactory::GetVectorContent(hPSD);

    double sigma;
    vector<OMU::Complex> SNR;

    std::tie(SNR, sigma)  = OMU::TKFR::SNR(OMU::Complexify(x), OMU::Complexify(y), fs, PSD, OMU::TKFR::Domain::ComplexFrequency, option, nFFT, fLow, fHigh);
    
    if(opt.Contains("x")) h0 = UIUC::TFactory::SetVectorContent(h0, OMU::TKFR::Get(part, SNR));
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return {NULL, NAN};
    }

    h0->SetName((TString) h0->GetName() + ":"+opt+"-" + enum2str(part));
    return make_pair(h0, sigma);
}

std::vector<double> OMU::TKFR::Downsample(const std::vector<double> &x, double fs, double newFs, int offset) 
{ 
    // Extract subsample here..
    if (fs <= newFs) return x;

    if(!UIUC::EpsilonEqualTo(UIUC::GCD(fs, newFs), TMath::Min(fs, newFs)))
            throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

    std::vector<double> Xn = OMU::SliceXn(x, fs/newFs, offset);
                        Xn.push_back(x[x.size()-1]);

    return Xn;
}

std::vector<OMU::Complex> OMU::TKFR::Downsample(const std::vector<OMU::Complex> &x, double fs, double newFs, int offset) 
{ 
    // Extract subsample here..
    if (fs <= newFs) return x;
    
    if(!UIUC::EpsilonEqualTo(UIUC::GCD(fs, newFs), TMath::Min(fs, newFs)))
            throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

    std::vector<OMU::Complex> Xn = OMU::SliceXn(x, fs/newFs, offset);
                              Xn.push_back(x[x.size()-1]);

    return Xn;
}

TH1* OMU::TKFR::Downsample(TH1 *h, Option_t *option, double newFs, int offset)
{
    TString opt = option;
            opt.ToLower();

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h->GetXaxis();
    else if(opt.Contains("y")) axis = h->GetYaxis();
    else if(opt.Contains("z")) axis = h->GetZaxis();
    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double fs = 1./axis->GetBinWidth(1);
    if(UIUC::EpsilonEqualTo(newFs, fs)) return h;

    TString part1 = Form("%.0f", newFs); 
    TString part2 = offset ? "["+TString::Itoa(offset,10)+"]" : "";
    
    TH1* h0 = (TH1*) h->Clone((TString) h->GetName() + ":" + opt + "-" + Form("%s%s%s", "downsample", part1.Data(), part2.Data()));

    double dx = fs/newFs;
    std::vector<double> x       = OMU::TKFR::Downsample(UIUC::TFactory::Range(axis), fs, newFs, offset);
    std::vector<double> content = OMU::TKFR::Downsample(UIUC::TFactory::GetVectorContent(h0), fs, newFs, offset);

    h0->SetBins(x.size()-1, &x[0]);
    axis->SetRangeUser(x[0], x[x.size()-1]);
    
    if(opt.Contains("x")) {
        
        h0 = UIUC::TFactory::SetVectorContent(h0, content);
        
    } else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

std::vector<double> OMU::TKFR::Upsample(const std::vector<double> &x, double fs, double newFs, int offset) 
{ 
    // Extract subsample here..
    if(fs >= newFs) return x;
    if(!UIUC::EpsilonEqualTo(UIUC::GCD(fs, newFs), TMath::Min(fs, newFs)))
          throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

    return OMU::InsetXn(x, newFs/fs, offset);
}

std::vector<OMU::Complex> OMU::TKFR::Upsample(const std::vector<OMU::Complex> &x, double fs, double newFs, int offset) 
{ 
    // Extract subsample here..
    if(fs >= newFs) return x;
    if(!UIUC::EpsilonEqualTo(UIUC::GCD(fs, newFs), TMath::Min(fs, newFs)))
          throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

    return OMU::InsetXn(x, newFs/fs, offset);
}

TH1* OMU::TKFR::Upsample(TH1 *h, Option_t *option, double newFs, int offset)
{
    TString opt = option;
            opt.ToLower();

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h->GetXaxis();
    else if(opt.Contains("y")) axis = h->GetYaxis();
    else if(opt.Contains("z")) axis = h->GetZaxis();
    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double fs = 1./axis->GetBinWidth(1);
    if(UIUC::EpsilonEqualTo(newFs, fs)) return h;

    TString part1 = Form("%.0f", newFs); 
    TString part2 = offset ? "["+TString::Itoa(offset,10)+"]" : "";
    
    TH1* h0 = (TH1*) h->Clone((TString) h->GetName() + ":" + opt + "-" + Form("%s%s%s", "downsample", part1.Data(), part2.Data()));

    double dx = newFs / fs;
    int nbins = axis->GetNbins()*dx;
    std::vector<double> x       = OMU::TKFR::Interpolate(nbins, UIUC::TFactory::Range(axis));
    std::vector<double> content = OMU::TKFR::Upsample(UIUC::TFactory::GetVectorContent(h0), fs, newFs, offset);

    h0->SetBins(x.size()-1, &x[0]);
    axis->SetRangeUser(x[0], x[x.size()-1]);
    
    if(opt.Contains("x")) {
        
        h0 = UIUC::TFactory::SetVectorContent(h0, content);
        
    } else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

std::vector<double> OMU::TKFR::Decimate(const std::vector<double> &x, double fs, double fdown, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<double, double> &filterRpRs) 
{
    if(fs <= fdown) return x;

    double M = fs/fdown;
    OMU::SOS filter = OMU::TKFR::Biquad(filterOrder, filterDesign, filterRpRs, OMU::TKFR::FilterType::LowPass, {fs/(2*M)}, fs);
    
    switch(method) {

        case Method::Polyphase: 
            throw std::invalid_argument("Polyphase filter not implemented yet..");

        default:
        case Method::FFT:
            return Downsample(OMU::TKFR::Filtering(x, filter), fs, fdown, offset);
    }
}

std::vector<OMU::Complex> OMU::TKFR::Decimate(const std::vector<OMU::Complex> &x, double fs, double fdown, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<double, double> &filterRpRs) 
{
    if(fs <= fdown) return x;

    double M = fs/fdown;
    OMU::SOS filter = OMU::TKFR::Biquad(filterOrder, filterDesign, filterRpRs, OMU::TKFR::FilterType::LowPass, {fs/(2*M)}, fs);
    
    switch(method) {

        case Method::Polyphase: 
            throw std::invalid_argument("Polyphase filter not implemented yet..");

        default:
        case Method::FFT:
            return Downsample(OMU::TKFR::Filtering(x, filter), fs, fdown, offset);
    }
};

TH1* OMU::TKFR::Decimate(TH1 *h, Option_t *option, double fdown, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<double, double> &filterRpRs)
{
    if(h == NULL) return NULL;
    
    TString opt = option;
            opt.ToLower();

    TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":" + opt + "-decimate["+TString::Itoa(fdown,10)+","+TString::Itoa(offset,10)+"]");

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();
    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double fs = 1./axis->GetBinWidth(1);
    if(UIUC::EpsilonEqualTo(fdown, fs)) return h;

    double dx = fdown / fs;
    
    std::vector<double> x       = OMU::TKFR::Downsample(UIUC::TFactory::Range(axis), fs, fdown);
    std::vector<double> content = OMU::TKFR::Decimate  (UIUC::TFactory::GetVectorContent(h0), fs, fdown, offset, method, filterOrder, filterDesign);

    h0->SetBins(x.size()-1, &x[0]);
    axis->SetRangeUser(x[0], x[x.size()-1]);
    
    if(opt.Contains("x")) {
        
        h0 = UIUC::TFactory::SetVectorContent(h0, content);
        
    } else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

std::vector<double> OMU::TKFR::Interpolate(const std::vector<double> &x, double fs, double fup, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<double, double> &filterRpRs) 
{
    if(fs >= fup) return x;

    double L = fup/fs;
    OMU::SOS filter = OMU::TKFR::Biquad(filterOrder, filterDesign, filterRpRs, OMU::TKFR::FilterType::LowPass, {fs/(2*L)}, fs);

    switch(method) {

        case Method::Polyphase: 
            throw std::invalid_argument("Polyphase filter not implemented yet..");

        default:
        case Method::FFT:
            return OMU::Multiply(OMU::TKFR::Filtering(Upsample(x, fs, fup, offset), filter), L);
    }
}

std::vector<OMU::Complex> OMU::TKFR::Interpolate(const std::vector<OMU::Complex> &x, double fs, double fup, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<double, double> &filterRpRs) 
{
    if(fs >= fup) return x;

    double L = fup/fs;
    OMU::SOS filter = OMU::TKFR::Biquad(filterOrder, filterDesign, filterRpRs, OMU::TKFR::FilterType::LowPass, {fs/(2*L)}, fs);

    switch(method) {

        case Method::Polyphase: 
            throw std::invalid_argument("Polyphase filter not implemented yet..");

        default:
        case Method::FFT:
            
            return OMU::Multiply(OMU::TKFR::Filtering(Upsample(x, fs, fup, offset), filter), L);
    }
};

TH1* OMU::TKFR::Interpolate(TH1 *h, Option_t *option, double fup, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<double, double> &filterRpRs)
{
    if(h == NULL) return NULL;
    
    TString opt = option;
            opt.ToLower();

    TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":" + opt + "-interpolate["+TString::Itoa(fup,10)+"]");

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();
    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double fs = 1./axis->GetBinWidth(1);
    if(UIUC::EpsilonEqualTo(fup, fs)) return h;

    double dx = fup / fs;
    int nbins = axis->GetNbins()*dx;
    std::vector<double> x       = OMU::TKFR::Interpolate(nbins, UIUC::TFactory::Range(axis));
    std::vector<double> content = OMU::TKFR::Interpolate(UIUC::TFactory::GetVectorContent(h0), fs, fup, offset, method, filterOrder, filterDesign, filterRpRs);

    h0->SetBins(x.size()-1, &x[0]);
    axis->SetRangeUser(x[0], x[x.size()-1]);
    
    if(opt.Contains("x")) {
        
        h0 = UIUC::TFactory::SetVectorContent(h0, content);
        
    } else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

std::vector<OMU::Complex> OMU::TKFR::Resample(const std::vector<OMU::Complex> &x, double fs, double fc, double bw, Method method) 
{

    Complex dc = OMU::Mean(x);
    std::vector<OMU::Complex> y = OMU::Subtract(x, dc);

    double df = fs/x.size();
    if(bw < fs) {

        if(fc-bw/2 < 0 || fc+bw/2 > fs) {
            UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Selected window [%.2f, %.2f] is out of range [0,%.2f]", (fc-bw/2.), (fc+bw/2.), fs);
        }

        y = OMU::FrequencyShift(y, fs, -fc);
        y = OMU::TKFR::Resample(y, fs, bw, OMU::TKFR::Domain::ComplexFrequency, method);
        y = OMU::FrequencyShift(y, bw, bw/2.);

    } else if(bw > fs) {

        y = OMU::FrequencyShift(y, fs, fs/2);
        y = OMU::TKFR::Resample(y, fs, bw, OMU::TKFR::Domain::ComplexFrequency, method);
        y = OMU::FrequencyShift(y, bw, fc);
    }

    return OMU::Add(y, dc);
}

std::vector<OMU::Complex> OMU::TKFR::Resample(const std::vector<OMU::Complex> &x, double fs, double newFs, OMU::TKFR::Domain domain, Method method) 
{
    double duration = x.size()/fs;
    switch(method) {

        case Method::Polyphase: 
            throw std::invalid_argument("Polyphase filter not implemented yet..");

        default:
        case Method::FFT:
            return OMU::TKFR::iFFT(OMU::ResizeFFT(FFT(x, domain), duration*newFs), domain, "", duration*newFs);
    }
};

TH1* OMU::TKFR::Resample(TH1 *h, Option_t *option, double newFs, Method method)
{
    if(h == NULL) return NULL;
    
    TString opt = option;
            opt.ToLower();

    TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":" + opt + "-resample["+TString::Itoa(newFs,10)+"]");

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();
    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double fs = 1./axis->GetBinWidth(1);
    if(UIUC::EpsilonEqualTo(newFs, fs)) return h;

    std::vector<double> x;
    if(fs < newFs) {

        double dx = newFs / fs;
        int nbins = axis->GetNbins()*dx;
        x = OMU::TKFR::Interpolate(nbins, UIUC::TFactory::Range(axis));
    
    } else {

        double dx = fs / newFs;
        x = OMU::TKFR::Downsample(UIUC::TFactory::Range(axis), fs, newFs);
    }    
    
    std::vector<double> content = OMU::TKFR::Resample(UIUC::TFactory::GetVectorContent(h0), fs, newFs, method);
    
    h0->SetBins(x.size()-1, &x[0]);
    axis->SetRangeUser(x[0], x[x.size()-1]);
    
    if(opt.Contains("x")) {
        
        h0 = UIUC::TFactory::SetVectorContent(h0, content);
        
    } else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

std::vector<std::vector<OMU::Complex>> OMU::TKFR::Channelize(int M, const std::vector<OMU::Complex> &x, double fs, OMU::TKFR::Domain domain, Method method)
{    
    throw std::invalid_argument("Channelization is not implemented yet.");
    return {};
}

TH2* OMU::TKFR::Channelize(int M, TH1 *h1, Option_t *option, Method method)
{
    TString opt = option;
            opt.ToLower();

    throw std::invalid_argument("Channelization is not implemented yet.");
    return NULL;
}

std::vector<std::vector<OMU::Complex>> Synthesize(const std::vector<std::vector<OMU::Complex>> &x, double fs, OMU::TKFR::Domain domain, OMU::TKFR::Method method)
{
    throw std::invalid_argument("Synthesize is not implemented yet.");
    return {};
}

TH2* OMU::TKFR::Synthesize(TH2 *h, Option_t *option, Method method)
{
    TString opt = option;
            opt.ToLower();

    throw std::invalid_argument("Synthesize is not implemented yet.");
    return NULL;
}

std::vector<OMU::Complex> OMU::TKFR::MatchedFiltering(const std::vector<OMU::Complex> &x, double fs, std::vector<double> PSD, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    throw std::invalid_argument("Matched filtering is not implemented yet.");
    return {};
}

std::vector<OMU::Complex> OMU::TKFR::MatchedFiltering(TH1 *h, TH1 *hNoisePSD, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    throw std::invalid_argument("Matched filtering is not implemented yet.");
    return {};
}

std::vector<std::vector<OMU::Complex>> OMU::TKFR::VQT(std::vector<OMU::Complex> x, Option_t *option,  int nFFT)
{
    throw std::invalid_argument("VQT is not implemented yet.");
    return {};
}

TH2* OMU::TKFR::VQT(ComplexPart part, TH1 *h1, Option_t *option,  int nFFT)
{
    TString opt = option;
            opt.ToLower();

    throw std::invalid_argument("VQT is not implemented yet.");
    return NULL;    
}

std::vector<std::vector<OMU::Complex>> OMU::TKFR::CQT(std::vector<OMU::Complex> x, Option_t *option,  int nFFT)
{
    throw std::invalid_argument("CQT is not implemented yet.");
    return {};
}

TH2* OMU::TKFR::CQT(ComplexPart part, TH1 *h1, Option_t *option,  int nFFT)
{
    TString opt = option;
            opt.ToLower();

    throw std::invalid_argument("CQT is not implemented yet.");
    return NULL;
}

TH1* OMU::TKFR::Filtering(TH1 *h, Option_t *option, TString alias, vector<OMU::SOS> sos, Direction direction)
{
    TString opt = option;
            opt.ToLower();

    TString directionStr = enum2str(direction);
            directionStr = !directionStr.EqualTo("") ? "-"+directionStr : "";
    TString aliasStr = alias;
            aliasStr = !aliasStr.EqualTo("") ? "-"+aliasStr : "";

    TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":" + opt + aliasStr + directionStr);

    if(opt.Contains("x")) {

        UIUC::TFactory::SetVectorContent(h0, OMU::TKFR::Filtering(UIUC::TFactory::GetVectorContent(h), sos, direction));

    } else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

vector<double> OMU::TKFR::Filtering(const vector<double> &Xn, FilterType filter, const vector<double> &cutoff, WindowType window, double ab, WindowSymmetry  symmetry)
{
    kfr::univector<double> Yn = OMU::kfrvector(Xn);

    KFR__EXPRESSION_HANDLE<double> Wn = KFR__TO_HANDLE(OMU::kfrvector(OMU::TKFR::Window(Xn.size(), window, ab, symmetry)));

    switch(filter) {
        case FilterType::LowPass:
            fir_lowpass(Yn, cutoff[0], Wn, true);
            break;

        case FilterType::HighPass:
             fir_highpass(Yn, cutoff[0], Wn, true);
            break;

        case FilterType::BandPass:
            fir_bandpass(Yn, cutoff[0], cutoff[1], Wn, true);
            break;

        case FilterType::Notch: [[fallthrough]];
        case FilterType::BandStop:
            fir_bandstop(Yn, cutoff[0], cutoff[1], Wn, true);
            break;

        case FilterType::None:
            return Xn;

        case FilterType::LowShelf: [[fallthrough]];
        case FilterType::HighShelf: [[fallthrough]];
        case FilterType::Peak: [[fallthrough]];
        case FilterType::AllPass:
          throw std::invalid_argument("Unexpected filter implemented.");
            return vector<double>();
    }

    return OMU::stdvector(Yn);
}

TH1* OMU::TKFR::Filtering(TH1 *h, Option_t *option, FilterType filter, const vector<double> &cutoff, WindowType window, double ab, WindowSymmetry  symmetry)
{
    TString opt = option;
            opt.ToLower();

    // Apply windowing
    TH1* h0 = Windowing(h, opt, window, ab, symmetry);
         h0->SetName((TString) h->GetName()+":"+opt+"-"+enum2str(filter));

    // Apply filtering
    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Filter histogram \""+h->GetName()+"\" using \""+enum2str(filter)+"\" with options \""+opt.Data()+"\""); 

    if(opt.Contains("x")) UIUC::TFactory::SetVectorContent(h0, OMU::TKFR::Filtering(UIUC::TFactory::GetVectorContent(h0), filter, cutoff, window, ab, symmetry));
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        if(h != h0) delete h0;

        return NULL;
    }

    return h0;
}

TH1* OMU::TKFR::Windowing(TH1 *h, Option_t *option, double dutyCycle, WindowType window, double ab, WindowSymmetry  symmetry)
{
    TString opt = option;
            opt.ToLower();

    if(enum2str(window).EqualTo("")) return h;

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Windowing histogram "+h->GetName()+" using \""+enum2str(window)+"\" with options \""+opt+"\"");
    TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+opt+"-window-"+enum2str(window));
         h0 = UIUC::TFactory::Empty(h0);

    vector<double> y = OMU::TKFR::Windowing(UIUC::TFactory::GetVectorContent(h), dutyCycle, window, ab, symmetry);
    if(opt.Contains("x")) UIUC::TFactory::SetVectorContent(h0, y);
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;
        return NULL;
    }

    return h0;
}

TH1* OMU::TKFR::Detrend(TH1 *h, Trend detrend, Option_t *option)
{
    TString opt = option;
            opt.ToLower();

    TFitResult *r = NULL;

    switch(detrend) {

        case Trend::None:
            return h;

        case Trend::Constant:
        {
            UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Detrend with \"constant\" histogram "+h->GetName()+" with fitting options \""+opt+"\"");

            UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
            h->Fit("pol0", opt).Get();
            h->Add(h->GetFunction("pol0"), -1);
            UIUC::HandboxMsg::DumpTTY(__METHOD_NAME__);

            return h;
        }

        case Trend::Linear:
        {
            UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Detrend with \"linear\" histogram "+h->GetName()+" with fitting options \""+opt+"\"");

            UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
            h->Fit("pol1", opt).Get();
            h->Add(h->GetFunction("pol1"), -1);
            UIUC::HandboxMsg::DumpTTY(__METHOD_NAME__);

            return h;
        }

        default:
            throw std::invalid_argument("Unknown detrending method.");
    }
}

vector<OMU::Complex> OMU::TKFR::Impulse(double fs, double duration, const std::vector<double> &amps, const std::vector<double> &freqs, OMU::TKFR::Domain domain)
{
    int nFFT = (domain == Domain::ComplexFrequency || domain == Domain::Time) ? fs*duration : fs*duration/2+1;

    std::vector<OMU::Complex> Xk(nFFT);
    for(int i = 0, N = freqs.size(); i < N; i++) {

        double f = freqs[i]*duration;
        if(freqs[i] >= fs) {
            UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Impulse at %.2fHz is out of scope (fs = %.2fHz)", freqs[i], fs);
            continue;
        }

        Xk[f] = amps[i]*fs * Complex(TMath::Cos(TMath::Pi()/2), TMath::Sin(-TMath::Pi()/2));
        if(domain == Domain::Time || domain == Domain::ComplexFrequency)
            Xk[Xk.size()-f] = OMU::CConj(Xk[f]);
    }

    switch(domain) {

        case Domain::Time:
            return OMU::TKFR::iFFT(Xk, OMU::TKFR::Domain::ComplexFrequency, "");
        
        case Domain::Frequency: [[fallthrough]];
        case Domain::ComplexFrequency: [[fallthrough]];
        default: return Xk;
    }
}

std::vector<double> OMU::TKFR::Chirp(const double fs, const double duration, double t0, double t1, double f0, double f1, Trend method, double taper, const double phase)
{
    TString formulaStr;

    double _buf;
    if(t0 > t1) { 

        _buf = f0;
        f0 = f1;
        f1 = _buf;

        _buf = t0;
        t0 = t1;
        t1 = _buf;
    }

    if(f0 <= 0 || f1 <= 0) {

        std::cerr << Form("Frequency (`f0`,`f1`) = (%.2f,%.2f) must be greater than 0", f0, f1) << std::endl;
        throw std::exception();
    }

    TF1 *formula = NULL;

    double beta = 0;
    switch(method) {

        case Trend::Linear:
            beta       = (f1-f0) / t1;
            formula = new TF1("formula-"+UIUC::GetRandomStr(), "[1] + [0] * x", t0, t1);
            formula->SetParName  (0, "beta");
            formula->SetParameter(0, beta);
            formula->SetParName  (1, "f0");
            formula->SetParameter(1, f0);
            break;
        
        case Trend::Quadratic:
        case Trend::QuadraticConcave:
            beta       = (f1-f0)/(t1*t1);
            formula = new TF1("formula-"+UIUC::GetRandomStr(), "[2] - [0] * ([1] - x)**2", t0, t1);
            formula->SetParName  (0, "beta");
            formula->SetParameter(0, beta);
            formula->SetParName  (1, "t1");
            formula->SetParameter(1, t1-t0);
            formula->SetParName  (2, "f1");
            formula->SetParameter(2, f1);
            break;

        case Trend::QuadraticConvex:
            beta       = (f1-f0)/(t1*t1);
            formula = new TF1("formula-"+UIUC::GetRandomStr(), "[1] + [0] * x**2", t0, t1);
            formula->SetParName  (0, "beta");
            formula->SetParameter(0, beta);
            formula->SetParName  (1, "f0");
            formula->SetParameter(1, f0);
            break;

        case Trend::Logarithmic:

            if (f0*f1 <= 0.0) {

                std::cerr << Form("Logarithmic chirp, must have be same frequency sign (`f0`,`f1`) = (%.2f,%.2f)", f0, f1) << std::endl;
                throw std::exception();
            }
    
            formula = new TF1("formula-"+UIUC::GetRandomStr(), UIUC::EpsilonEqualTo(f0, f1) ? "[0]" : "[0] * ([2]/[0])**(x/[1])", t0, t1);
            formula->SetParName  (0, "f0");
            formula->SetParameter(0, f0);
            formula->SetParName  (1, "t1");
            formula->SetParameter(1, t1-t0);
            formula->SetParName  (2, "f1");
            formula->SetParameter(2, f1);
            break;

        case Trend::Hyperbolic: 
            formula = new TF1("formula-"+UIUC::GetRandomStr(), UIUC::EpsilonEqualTo(f0, f1) ? "[0]" : "[0]*[2]*[1] / (([0] - [2])*x + [2]*[1])", t0, t1);
            formula->SetParName  (0, "f0");
            formula->SetParameter(0, f0);
            formula->SetParName  (1, "t1");
            formula->SetParameter(1, t1-t0);
            formula->SetParName  (2, "f1");
            formula->SetParameter(2, f1);
            break;

        case Trend::Constant:
            std::cerr << "Chirp method `" << enum2str(method) << "` not implemented. Use TKFR::Impulse" << std::endl; 
            throw std::exception();

        default: 
            std::cerr << "Chirp method `" << enum2str(method) << "` not implemented." << std::endl; 
            throw std::exception();
    }

    std::vector<double> sweepPolynomial = SweepFormula(fs, duration, formula, {}, taper, phase);
   
    if(formula) formula->Delete();
    return sweepPolynomial;
}

std::vector<double> OMU::TKFR::SweepPolynomial(const double fs, const double duration, double t0, double t1, const std::vector<double> coeff, double taper, double phase)
{
    TF1 *formula = new TF1("formula-"+UIUC::GetRandomStr(), Form("pol%lu", TMath::Max((ULong_t) 0, coeff.size()-1)), t0, t1);
    std::vector<double> sweepPolynomial = SweepFormula(fs, duration, formula, coeff, taper, phase);

    formula->Delete();
    return sweepPolynomial;
}

std::vector<double> OMU::TKFR::SweepFormula(const double fs, const double duration, TF1 *formula, const std::vector<double> params, double taper, double phase)
{
    std::vector<double> x(fs*duration, 0);
    phase = UIUC::MathMod(phase, 2*TMath::Pi());

    for(int i = 0, N = params.size(); i < N; i++)
        formula->SetParameter(i, params[i]);

    double xMin = TMath::Max(formula->GetXmin(), 0.0);
    int iMin = xMin*fs;
    
    double xMax = TMath::Min(formula->GetXmax(), duration);
    int iMax = xMax*fs;

    for(int i = iMin, N = iMax; i < N; i++) {
        x[i] = TMath::Cos(2 * TMath::Pi() * formula->Integral(0, i/fs) + phase);
    }

    x = OMU::StripNaN(x);
    if(!UIUC::IsNaN(taper)) x = OMU::TKFR::Taper(x, fs, xMin, xMax, taper);

    return x;
}

std::vector<double> OMU::TKFR::Gaussian(const double fs, const double duration, double mean, double sigma)
{
    unsigned int N = fs*duration;
    std::vector<double> x(N, 0);
    for(int i = 0; i < N; i++) {
        x[i] = gRandom->Gaus(mean, sigma);
    }

    return x;
}

std::vector<double> OMU::TKFR::AR(const double fs, double c, std::vector<double> p, std::vector<double> AR0, std::vector<double> w)
{
    unsigned int N = fs;
    if(N < 1) return {};

    std::vector<double> AR(N, 0);
    if(w.size() != N) {
        std::cerr << "Wrong random noise length (= " << w.size() <<  ") provided. N must be " << N << std::endl;
        throw std::exception();
    }

    if(p.size() != AR0.size()) {
        std::cerr << "Unexpected initial conditions provided (N = " << w.size() <<  "). N must be " << p.size() << std::endl;
        throw std::exception();
    }

    for(int i = 0, I = p.size(); i < N; i++) {

        if(i < I) { 
            AR[i] = AR0[i]; // Initial conditions
            continue;
        }

        AR[i] = c + w[i];
        for(int k = 0, K = p.size(); k < K; k++) {  // AR model
            AR[i] += p[k]*AR[(i-1)-k];
        }
    }

    return AR;
}

std::vector<double> OMU::TKFR::MA(const double fs, std::vector<double> q, std::vector<double> w)
{
    unsigned int N = fs;
    if(N < 1) return {};

    std::vector<double> MA(N, 0);
    if(w.size() != N) {
        std::cerr << "Wrong random noise length (= " << w.size() <<  ") provided. N must be " << N << std::endl;
        throw std::exception();
    }

    for(int i = 0, I = q.size(); i < N; i++) {

        if(i < I) {
            MA[i] = w[i]; // Initial conditions
            continue;
        }

        for(int k = 0, K = q.size(); k < K; k++) { // MA model
            MA[i] += q[k] * w[(i-1)-k];
        }
    }

    return MA;
}

std::vector<OMU::Complex> OMU::TKFR::Spectrum(double fs, OMU::TKFR::Domain domain, OMU::Complex dc, TString formulaReStr, TString formulaImStr, std::vector<double> params, std::vector<double> paramSigmas, int iterations)
{
    TString randomStr = UIUC::GetRandomStr();
    TFormula *formulaRe = new TFormula("formula-"+randomStr+"[Re]", formulaReStr);
    TFormula *formulaIm = new TFormula("formula-"+randomStr+"[Im]", formulaImStr);
    std::vector<Complex> spectrum = Spectrum(fs, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations);

    formulaRe->Delete();
    formulaIm->Delete();
    
    return spectrum;
}

std::vector<OMU::Complex> OMU::TKFR::Spectrum(double fs, OMU::TKFR::Domain domain, OMU::Complex dc, TFormula *formulaRe, TFormula *formulaIm, std::vector<double> params, std::vector<double> paramSigmas, int iterations)
{
    if(!formulaRe->IsValid()) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Invalid formula \""+formulaRe->GetName()+"\" provided for spectrum generation.");
        return {};
    }

    if(!formulaIm->IsValid()) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Invalid formula \""+formulaIm->GetName()+"\" provided for spectrum generation.");
        return {};
    }

    int nFFT;
    switch(domain) {

        case Domain::Frequency: nFFT = fs/2+1;
        break;

        case Domain::ComplexFrequency: nFFT = fs;
        break;
        default:
            throw std::invalid_argument( "Unexpected domain provided.");
    }  

    if(params.size() < 1) iterations = 1; 
    paramSigmas.resize(params.size(), 0);

    std::vector<Complex> spectrum(nFFT, 0);
    spectrum[0] = dc;

    for(int j = 0; j < iterations; j++) {

        std::vector<double> _params(params.size(), 0);
        for(int i = 0, N = _params.size(); i < N; i++) {
        
            _params[i] = params[i];
            if(!UIUC::EpsilonEqualTo(paramSigmas[i], 0))
                _params[i] = gRandom->Gaus(params[i], paramSigmas[i]);
        }

        for(int i = 0; i < nFFT; i++) {

            spectrum[i] = Complex(
                spectrum[i].real() + UIUC::TVarexp::Eval(formulaRe, _params, i/fs)[params.size()] / iterations, 
                spectrum[i].imag() + UIUC::TVarexp::Eval(formulaIm, _params, i/fs)[params.size()] / iterations
            );
        }
    }

    return spectrum;
}

std::vector<double> OMU::TKFR::Waveform(double fs, double duration, TString formulaStr, std::vector<double> params, std::vector<double> paramSigmas, int iterations)
{
    TFormula *formula = new TFormula("formula-"+UIUC::GetRandomStr(), formulaStr);
    std::vector<double> waveform = Waveform(fs, duration, formula, params, paramSigmas, iterations);
    
    formula->Delete();
    return waveform;
}

std::vector<double> OMU::TKFR::Waveform(double fs, double duration, TFormula *formula , std::vector<double> params, std::vector<double> paramSigmas, int iterations)
{
    if(!formula->IsValid()) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Invalid formula provided for waveform generation with \""+formula->GetName()+"\".");
        return {};
    }

    if(UIUC::IsNaN(duration)) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unexpected duration provided for waveform generation with \""+formula->GetName()+"\".");
        return {};
    }

    if(params.size() < 1) iterations = 1; 
    paramSigmas.resize(params.size(), 0);

    std::vector<double> waveform(duration*fs, 0);
    for(int j = 0; j < iterations; j++) {

        std::vector<double> _params(params.size(), 0);
        for(int i = 0, N = _params.size(); i < N; i++) {
        
            _params[i] = params[i];
            if(!UIUC::EpsilonEqualTo(paramSigmas[i], 0))
                _params[i] = gRandom->Gaus(params[i], paramSigmas[i]);
        }

        for(int i = 0, N = duration*fs; i < N; i++) {
            waveform[i] += UIUC::TVarexp::Eval(formula, _params, i/fs)[params.size()] / iterations;
        }
    }

    return waveform;
}

vector<double> OMU::TKFR::Waveform(double fs, double duration, double dutyCycle, WindowType window, double ab, WindowSymmetry  symmetry)
{
    double size = fs;
    double sizeOn = size * dutyCycle;
    
    vector<double> _canonical = OMU::TKFR::Window(sizeOn, dutyCycle, window, ab, symmetry);
    if(symmetry != WindowSymmetry ::symmetric) _canonical = symmetric_resize(_canonical, size);
    
    return OMU::Repeat(_canonical, duration);
}

vector<double> OMU::TKFR::Window(size_t N, double dutyCycle, WindowType window, double ab, WindowSymmetry  symmetry)
{
    dutyCycle = TMath::Min(1., dutyCycle);
    dutyCycle = TMath::Max(0., dutyCycle);
    
    int Ni = N * dutyCycle;
    if(Ni == 0) return vector<double>(N, 0);

    vector<double>            _canonical;
    kfr::univector<double> _unicanonical;

    switch(window)
    {
        case WindowType::Bartlett:
            _unicanonical = window_bartlett(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::BartlettHann:
            _unicanonical = window_bartlett_hann(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Blackman:
            _unicanonical = window_blackman(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::BlackmanHarris:
            _unicanonical = window_blackman_harris(Ni, symmetry);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Bohman:
            _unicanonical = window_bohman(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Cosine:
            _unicanonical = window_cosine(Ni);
            _canonical = stdvector(_unicanonical);
            break;

       case WindowType::Tukey:
           _unicanonical = UIUC::IsNaN(ab) ? window_tukey(Ni, WINDOW_DEFAULT_TUKEY_ALPHA) : window_tukey(Ni, ab);
           _canonical = stdvector(_unicanonical);
           break;

       case WindowType::PlanckTaper:
           _unicanonical = UIUC::IsNaN(ab) ? window_planck_taper(Ni, WINDOW_DEFAULT_PLANCK_TAPER_EPSILON) : window_planck_taper(Ni, ab);
           _canonical = stdvector(_unicanonical);

           if(!UIUC::EpsilonEqualTo(_canonical[0], 1)) _canonical[0] = 0;
           if(!UIUC::EpsilonEqualTo(_canonical[Ni-1], 1)) _canonical[Ni-1] = 0;
           break;

       case WindowType::PlanckTaperLeft:
           _unicanonical = UIUC::IsNaN(ab) ? window_planck_taper(Ni, WINDOW_DEFAULT_PLANCK_TAPER_EPSILON) : window_planck_taper(Ni, ab);
           _canonical = stdvector(_unicanonical);

           for(int i = 0; i < Ni/2; i++) 
                _canonical[Ni-i - 1] = 1;
           
           if(!UIUC::EpsilonEqualTo(_canonical[0], 1)) _canonical[0] = 0;
           break;

       case WindowType::PlanckTaperRight:
           _unicanonical = UIUC::IsNaN(ab) ? window_planck_taper(Ni, WINDOW_DEFAULT_PLANCK_TAPER_EPSILON) : window_planck_taper(Ni, ab);
           _canonical = stdvector(_unicanonical);

           for(int i = 0; i < Ni/2; i++) 
                _canonical[i] = 1;

           if(!UIUC::EpsilonEqualTo(_canonical[Ni-1], 1)) _canonical[Ni-1] = 0;
           break;

        case WindowType::Sine:
            _unicanonical = window_cosine(Ni);
            _canonical = OMU::Rotate(stdvector(_unicanonical), Ni/2);
            break;

        case WindowType::FlatTop:
            _unicanonical = window_flattop(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Gaussian:
            _unicanonical = UIUC::IsNaN(ab) ? window_gaussian(Ni) : window_gaussian(Ni, ab);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Hamming:
            _unicanonical = UIUC::IsNaN(ab) ? window_hamming(Ni)  : window_hamming(Ni, ab);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Hann:
            _unicanonical = window_hann(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Kaiser:
            _unicanonical = UIUC::IsNaN(ab) ? window_kaiser(Ni)   : window_kaiser(Ni, ab);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Lanczos:
            _unicanonical = window_lanczos(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Triangle: [[fallthrough]];
        case WindowType::Triangular:
            _unicanonical = window_triangular(Ni);
            _canonical = stdvector(_unicanonical);
            break;

        case WindowType::Sawtooth:
            _canonical.resize(Ni, 0);

            for(int i = 0; i < Ni; i++)
                _canonical[i] = i/(Ni-1.);

            break;

        case WindowType::Rectangle: [[fallthrough]];
        case WindowType::Rectangular:            
            _canonical = vector<double>(Ni, !UIUC::IsNaN(ab) ? ab : 1.);
            break;

        case WindowType::Square:
        
            _canonical = vector<double>(Ni, 0);
            for(int i = 0; i < Ni; i++)
                _canonical[i] = (i+1)%2/(Ni-1);

            break;

        case WindowType::Zeros: [[fallthrough]];
        case WindowType::ZeroPad:
            _canonical = vector<double>(Ni, 0);
            break;

        default      : [[fallthrough]];
        case WindowType::None: [[fallthrough]];
        case WindowType::Unit:
            _canonical = vector<double>(Ni, 1);
    }

    if(symmetry == WindowSymmetry::symmetric)
        _canonical = symmetric_resize(_canonical, N);

    return _canonical;
}

std::vector<OMU::Complex> OMU::TKFR::Taper(std::vector<OMU::Complex> x, double fs, double T0, double T1, double ab, WindowSymmetry symmetry)
{
    if(T0 > T1) {

        double _buf = T1;
        T1 = T0;
        T0 = T1;
    }

    std::vector<double> window = OMU::TKFR::Window(TMath::Abs(T0-T1) * fs, OMU::TKFR::WindowType::PlanckTaper, ab, symmetry);
    window.resize(x.size(),0);
    window = OMU::Roll(window, -T0*fs);

    return OMU::Multiply(x, window);
}

TH1* OMU::TKFR::Taper(TH1 *h, double T0, double T1, Option_t *option, double ab, WindowSymmetry symmetry)
{
    TString opt = option;
            opt.ToLower();

    if(h == NULL) return NULL;

    TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Taper histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (T = ["+Form("%.2fs; %.2fs", T0, T1)+"])");

    vector<double> xn = OMU::TKFR::Taper(UIUC::TFactory::GetVectorContent(h), fs, T0, T1, ab, symmetry);  
    h0->SetName (h->GetName()  + (TString) Form(":%s-taper", opt.Data()));
    h0->SetTitle(h->GetTitle() + (TString) Form(" / Tapered [%.4f, %.4f]", T0, T1));

    return h0;
}

vector<double> OMU::TKFR::CSD(
    ComplexPart part,
    const vector<OMU::Complex> &x,
    const vector<OMU::Complex> &y,
    double fs,
    Domain domain,
    Option_t *option,
    int nFFT,
    Estimator estimator,
    double noverlap,
    Average average, Trend detrend,
    WindowType window, double ab, WindowSymmetry  symmetry)
{
    if(x.size() != y.size())
        throw std::invalid_argument( "vectors are not same size: x("+TString::Itoa(x.size(),10)+") != y("+TString::Itoa(y.size(),10)+")");

    nFFT = nFFT < 1 ? x.size() : TMath::Min(nFFT, (int) x.size());

    switch(estimator) {

        case Estimator::Periodogram: // NB: This estimator is not using Windowing, nor overlap
        {
            return OMU::TKFR::CSD(part, x, y, fs, domain, option, nFFT, Estimator::ModifiedPeriodogram, noverlap, average, detrend, WindowType::None, ab, symmetry);
        }

        case Estimator::ModifiedPeriodogram: // NB: This estimator is not using overlap
        {
            vector<OMU::Complex> X = OMU::TKFR::FFT(Windowing(Detrend(x, detrend), window, ab, symmetry), domain, "o", nFFT);
            vector<OMU::Complex> Y = OMU::TKFR::FFT(Windowing(Detrend(y, detrend), window, ab, symmetry), domain, "o", nFFT);

            vector<double> ESD = OMU::TKFR::Get(part, OMU::Multiply(OMU::CConj(X), Y));
            if(part == ComplexPart::Phase) return ESD;
            nFFT = ESD.size();

            // Apply window correction factor: This is squared sum of bins, rescaled by 1/nFFT
            vector<double> _window = OMU::TKFR::Window(nFFT, window, ab, symmetry);
            double w2 = UIUC::SumX2(_window)/nFFT;

            // Scaling is applied to correct for the histogram binning
            return OMU::Multiply(ESD, 2. / (w2 * fs));  
        }

        case Estimator::LombScargle:
        {
            // https://jakevdp.github.io/blog/2015/06/13/lomb-scargle-in-python/
            throw std::invalid_argument( "Lomb-Scargle PSD not implemented.");
            return {};
        }

        case Estimator::BlackmanTukey:
        {   
            // These lines below are an attempt..
            throw std::invalid_argument( "Blackman-Tukey PSD not implemented.");

            // // Compute FFT with orthonormal scaling: 
            // // - For real frequencies: 1./sqrt(nFFT/2)
            // // - For complex frequencies: 1./sqrt(nFFT)
            // vector<double> Rxy = OMU::TKFR::Windowing(
            //     OMU::Mag(OMU::TKFR::CorrelateFFT(Detrend(x, detrend), Detrend(y, detrend), domain, "b", nFFT)),
            //     window, ab, symmetry
            // );

            // // Energy spectral density is scaled by a 2/N factor as a consequence of the FFT's orthonormal scaling
            // vector<double> ESD = OMU::TKFR::Get(part, OMU::TKFR::FFT(Rxy, domain, option, nFFT));

            // double w2 = 1;
            // if(part != ComplexPart::Phase) {

            //     // Energy spectral density is scaled by a 2/N factor as a consequence of the FFT's orthonormal scaling
            //     nFFT = 2*(ESD.size() - 1);

            //     // Apply window correction factor: This is squared sum of bins, rescaled by 1/nFFT
            //     vector<double> _window = OMU::TKFR::Window(nFFT, window, ab, symmetry);
            //     double w2 = UIUC::SumX2(_window)/nFFT;
            // }

            // // NB: WindowType and ESD scalings are partially compensated
            // return OMU::Multiply(ESD, 1. / w2);
        }

        case Estimator::Bartlett:
        {
            return OMU::TKFR::CSD(part, x, y, fs, domain, option, nFFT, Estimator::Welch, 0.0, average, detrend, window, ab, symmetry);
        }

        case Estimator::Welch:
        {
            vector<vector<double>> spectrogram = Spectrogram(part, x, y, fs, domain, option, nFFT, Estimator::ModifiedPeriodogram, noverlap, average, detrend, window, ab, symmetry);
            for(int index = 0, N = spectrogram.size(); index < N; index++) {

                if (spectrogram[index].size() == 0) // Compute average without holes..
                    spectrogram.erase(spectrogram.begin() + index);
            }

            switch(average) {

                case Average::Mean  : return OMU::Mean(spectrogram);
                case Average::Median: return OMU::Median(spectrogram);
                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected average method passed as argument..");
            }

            return {};
        }

        default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
    }

    return {};
}

vector<double> OMU::TKFR::CSD(
    EstimatorProperty property,
    const vector<OMU::Complex> &x,
    const vector<OMU::Complex> &y,
    double fs,
    Domain domain,
    Option_t *option,
    int nFFT,
    Estimator estimator,
    double noverlap,
    Average average, Trend detrend,
    WindowType window, double ab, WindowSymmetry  symmetry)
{
    if(x.size() != y.size())
        throw std::invalid_argument( "vectors are not same size: x("+TString::Itoa(x.size(),10)+") != y("+TString::Itoa(y.size(),10)+")");

    switch(property)
    {
        case EstimatorProperty::Decibel  : return Mag2dB(OMU::TKFR::CSD(EstimatorProperty::Spectrum, x, y, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry));
        case EstimatorProperty::Density  : return OMU::TKFR::CSD(ComplexPart::Magnitude, x, y, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);

        case EstimatorProperty::Spectrum : return OMU::Sqrt(OMU::TKFR::CSD(EstimatorProperty::Density, x, y, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry));

        case EstimatorProperty::Variance : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];

                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::Bias : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];

                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::Variability : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];
                
                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::Resolution : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];
                
                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::FigureOfMerit : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];
                
                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }

            break;
    }

    return {};
}

TH1* OMU::TKFR::CSD(ComplexPart part, TH1 *h1, TH1 *h2, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;
    if(h2 == NULL) return NULL;

    TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency

    nFFT = nFFT < 1 ? axis->GetNbins() : TMath::Min(nFFT, axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute CSD from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    vector<double> Sxy = OMU::TKFR::CSD(part, xn, (h1 == h2 ? xn : UIUC::TFactory::GetVectorContent(h2)), fs, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);

    if(opt.Contains((char) Operator::Concatenate)) nFFT = axis->GetNbins();
    double df = fs/nFFT;              // Spectral resolution

    TString windowing = "";
    switch(estimator) {
        case Estimator::Periodogram: windowing = "";
            break;

        default:
            windowing = enum2str(window);
            windowing = !windowing.EqualTo("") ? "-"+windowing : "";
    }

    h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":csd-"+enum2str(estimator)+windowing);
    h0->SetTitle(Form("Cross Power Spectrum Density (f_{s} = %.2f Hz, nFFT = %d, d_{f} = %.2f); Frequency (Hz)", fs, nFFT, df));

    // Make sure outgoing histogram ranges between [0;fNyquist]
    h0->SetBins(Sxy.size(), 0, fs/2+1);
    axis->SetRangeUser(0, fs/2+1);

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, Sxy);
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

TH1* OMU::TKFR::CSD(EstimatorProperty property, TH1 *h1, TH1 *h2, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;
    if(h2 == NULL) return NULL;

    TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? axis->GetNbins() : TMath::Min(nFFT, axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute CSD from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    vector<double> Sxy = OMU::TKFR::CSD(property, xn, (h1 == h2 ? xn : UIUC::TFactory::GetVectorContent(h2)), fs, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);

    if(opt.Contains((char) Operator::Concatenate)) nFFT = axis->GetNbins();
    double df = fs/nFFT;              // Spectral resolution
    
    TString windowing = "";
    switch(estimator) {
        case Estimator::Periodogram: windowing = "";
            break;

        default:
            windowing = enum2str(window);
            windowing = !windowing.EqualTo("") ? "-"+windowing : "";
    }

    h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":csd-"+enum2str(estimator)+windowing);
    h0->SetTitle(Form("Cross Power Spectrum Density (f_{s} = %.2f Hz, nFFT = %d, d_{f} = %.2f); Frequency (Hz)", fs, nFFT, df));

    // Make sure outgoing histogram ranges between [0;fNyquist]
    h0->SetBins(Sxy.size(), 0, fs/2+1);
    axis->SetRangeUser(0, fs/2+1);

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, Sxy);
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

TH1* OMU::TKFR::PSD(EstimatorProperty property, TH1 *h0, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{
    TH1 *h = OMU::TKFR::CSD(property, h0, h0, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);

    TString hname = (TString) h->GetName();
            hname = hname.ReplaceAll((TString) h0->GetName()+"_%_"+h0->GetName()+":csd-", (TString) h0->GetName()+":psd-");

    TString htitle  = (TString) h->GetTitle();
            htitle  = htitle.ReplaceAll("Cross Power Spectrum Density", "Power Spectrum Density");
            htitle += "; Frequency (Hz)";
            htitle += "; PSD (Strain / Hz)";

    h->SetName((TString) hname);
    h->SetTitle((TString) htitle);

    return h;
}

TH1* OMU::TKFR::ASD(EstimatorProperty property, TH1 *h0, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{
    TH1 *h = UIUC::TFactory::Sqrt(OMU::TKFR::CSD(property, h0, h0, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry));

    TString hname = (TString) h->GetName();
            hname = hname.ReplaceAll((TString) h0->GetName()+"_%_"+h0->GetName()+":csd-", (TString) h0->GetName()+":asd-");

    TString htitle  = (TString) h->GetTitle();
            htitle  = htitle.ReplaceAll("Cross Power Spectrum Density", "Amplitude Spectrum Density");
            htitle += "; Frequency (Hz)";
            htitle += "; ASD (Strain / #sqrt{Hz})";

    h->SetName((TString) hname);
    h->SetTitle((TString) htitle);
    return h;
}

vector<double> OMU::TKFR::Coherence(const vector<OMU::Complex> &x, const vector<OMU::Complex> &y, double fs, OMU::TKFR::Domain domain, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry)
{   
    vector<double> Pxy = OMU::TKFR::CSD(x, y, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    vector<double> Pxx = OMU::TKFR::PSD(x,    fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    vector<double> Pyy = OMU::TKFR::PSD(y,    fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);

    vector<double> Coh;
    for(int i = 0, N = Pxy.size(); i < N; i++)
        Coh.push_back((Pxy[i]*Pxy[i])/(Pxx[i]*Pyy[i]));

    return Coh;
}

TH1* OMU::TKFR::Coherence(TH1 *h1, TH1 *h2, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry)
{
    TH1* Pxy = OMU::TKFR::CSD(h1, h2, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    TH1* Pxx = OMU::TKFR::PSD(h1,     option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    TH1* Pyy = OMU::TKFR::PSD(h2,     option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);

    TString hname  = (TString) h1->GetName()+"_%_"+h2->GetName()+":coherence";
    TString htitle = "Coherence distribution";

    TH1 *h = (TH1*) Pxy->Clone(hname);
         h->SetTitle(htitle);

    for(int i = 0; i <= h->GetNcells(); i++) {

        if(UIUC::EpsilonEqualTo(Pxx->GetBinContent(i)*Pyy->GetBinContent(i), 0)) h->SetBinContent(i, 0);
        else h->SetBinContent(i, h->GetBinContent(i)*h->GetBinContent(i) / (Pxx->GetBinContent(i)*Pyy->GetBinContent(i)));
        
        h->SetBinError  (i, 0 /* to be computed, if needed */);
    }   
    
    return h;
}

vector<OMU::Complex> OMU::TKFR::Inject(const std::vector<OMU::Complex> &x, const std::vector<OMU::Complex> &y, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    return  OMU::TKFR::iFFT(OMU::Add(
        OMU::TKFR::FFT(x, domain, option, nFFT),
        OMU::TKFR::FFT(y, domain, option, nFFT))
    );
}

TH1* OMU::TKFR::Inject(TH1 *h1, TH1 *h2, Option_t *option, int nFFT) 
{ 
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;
    if(h2 == NULL) return NULL;

    TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? axis->GetNbins() : TMath::Min(nFFT, axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute convolution from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    vector<OMU::Complex> z = OMU::TKFR::Inject(xn, (h1 == h2 ? xn : UIUC::TFactory::GetVectorContent(h2)), option, nFFT);

    double df = fs/nFFT;              // Spectral resolution

    h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":inject");
    h0->SetTitle(Form("Convolution distribution (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+"); #tau", fs, nFFT, df));
    h0->SetBins(z.size(), 0, fs/2+1);
    axis->SetRangeUser(0, fs/2+1);

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, OMU::TKFR::Get(ComplexPart::Real, z));
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

vector<OMU::Complex> OMU::TKFR::Hilbert(vector<OMU::Complex> x, Option_t *option, int nFFT)
{
    std::vector<OMU::Complex> X = OMU::TKFR::FFT(x, OMU::TKFR::Domain::ComplexFrequency, option, nFFT);
    for(int i = 1, N = X.size(); i < N; i++)
        X[i] = i < N/2 ? 2. * X[i] : 0;
    
    return OMU::TKFR::iFFT(X, OMU::TKFR::Domain::ComplexFrequency, option, nFFT);
}

TH1* OMU::TKFR::Hilbert(ComplexPart part, TH1 *h1, Option_t *option, int nFFT) 
{ 
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;

    TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? 2*axis->GetNbins() : TMath::Min(nFFT, 2*axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute Hilbert transform from histograms \"" + h1->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    vector<OMU::Complex> Rxy = OMU::TKFR::Hilbert(OMU::Complexify(xn), option, nFFT);
    double df = fs/nFFT;              // Spectral resolution

    h0->SetName((TString) h1->GetName()+":hilbert");
    h0->SetTitle(Form("Hilbert transform (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+"); #tau", fs, nFFT, df));

    double length = (axis->GetXmax()-axis->GetXmin());
    h0->SetBins(Rxy.size(), -length, length);
    axis->SetRangeUser(-length, length);

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, OMU::TKFR::Get(part, Rxy));
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

TH1* OMU::TKFR::Envelope(TH1 *h0, Option_t *option, int nFFT) 
{ 
    TH1 *h = UIUC::TFactory::Abs(OMU::TKFR::Hilbert(h0, option, nFFT));

    TString hname = (TString) h->GetName();
            hname = hname.ReplaceAll((TString) h0->GetName()+":hilbert", (TString) h0->GetName()+":envelope");

    TString htitle  = (TString) h->GetTitle();
            htitle  = htitle.ReplaceAll("Hilbert transform", "Envelope distribution");

    h->SetName((TString) hname);
    h->SetTitle((TString) htitle);
    return h;
}

vector<OMU::Complex> OMU::TKFR::ConvolveFFT(vector<OMU::Complex> x, vector<OMU::Complex> y, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    int max = TMath::Max(x.size(), y.size());

    x.resize(2*max, 0);
    y.resize(2*max, 0);

    return  OMU::TKFR::iFFT(OMU::Multiply(
                OMU::TKFR::FFT(x, domain, option, nFFT),
                OMU::TKFR::FFT(y, domain, option, nFFT)), domain, option, nFFT
            );
}

TH1* OMU::TKFR::ConvolveFFT(ComplexPart part, TH1 *h1, TH1 *h2, Option_t *option, int nFFT) 
{ 
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;
    if(h2 == NULL) return NULL;

    TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? 2*axis->GetNbins() : TMath::Min(nFFT, 2*axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute convolution from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    vector<OMU::Complex> Rxy = OMU::TKFR::ConvolveFFT(xn, (h1 == h2 ? xn : UIUC::TFactory::GetVectorContent(h2)), option, nFFT);
    double df = fs/nFFT;              // Spectral resolution

    h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":convolve");
    h0->SetTitle(Form("Convolution distribution (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+"); #tau", fs, nFFT, df));

    double length = (axis->GetXmax()-axis->GetXmin());
    h0->SetBins(Rxy.size(), -length, length);
    axis->SetRangeUser(-length, length);

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, OMU::TKFR::Get(part, Rxy));
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

vector<OMU::Complex> OMU::TKFR::CorrelateFFT(vector<OMU::Complex> x, vector<OMU::Complex> y, OMU::TKFR::Domain domain, Option_t *option, int nFFT)
{
    int max = TMath::Max(x.size(), y.size());

    x.resize(2*max, 0);
    y.resize(2*max, 0);

    return  OMU::TKFR::iFFT(OMU::Multiply(
                OMU::CConj(OMU::TKFR::FFT(OMU::Rotate(x,max), domain, option, nFFT)),
                OMU::TKFR::FFT(y, domain, option, nFFT)), domain, option, nFFT
            );
}

TH1* OMU::TKFR::CorrelateFFT(ComplexPart part, TH1 *h1, TH1 *h2, Option_t *option, int nFFT) 
{ 
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;
    if(h2 == NULL) return NULL;

    TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? 2*axis->GetNbins() : TMath::Min(nFFT, 2*axis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute correlation from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    vector<OMU::Complex> Rxy = OMU::TKFR::CorrelateFFT(xn, (h1 == h2 ? xn : UIUC::TFactory::GetVectorContent(h2)), option, nFFT);

    double df = fs/nFFT;              // Spectral resolution

    h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":correlation");
    h0->SetTitle(Form("Correlation distribution (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+"); #tau", fs, nFFT, df));

    double length = (axis->GetXmax()-axis->GetXmin());
    h0->SetBins(Rxy.size(), -length, length);
    axis->SetRangeUser(-length, length);

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, OMU::TKFR::Get(part, Rxy));
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

TH2* OMU::TKFR::Spectrogram(ComplexPart part, TH1 *h0, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{      
    TH2 *h = (TH2*) OMU::TKFR::Spectrogram(part, h0, h0, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);

    TString hname = (TString) h->GetName();
            hname = hname.ReplaceAll((TString) h0->GetName()+"_%_"+h0->GetName()+":xspectrogram-", (TString) h0->GetName()+":spectrogram-");

    TString htitle  = (TString) h->GetTitle();
            htitle  = htitle.ReplaceAll("Cross Spectrogram", "Spectrogram");
            htitle += "; Time (s)";
            htitle += "; Frequency (Hz)";
            htitle += "; ASD (Strain / #sqrt{Hz})";

    h->SetName((TString) hname);
    h->SetTitle((TString) htitle);

    return h;
}

TH2* OMU::TKFR::Spectrogram(EstimatorProperty property, TH1 *h0, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{
    TH2 *h = (TH2*) OMU::TKFR::Spectrogram(property, h0, h0, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    if(h == NULL) return NULL;
    TString hname = (TString) h->GetName();
            hname = hname.ReplaceAll((TString) h0->GetName()+"_%_"+h0->GetName()+":xspectrogram-", (TString) h0->GetName()+":spectrogram-");

    TString htitle  = (TString) h->GetTitle();
            htitle  = htitle.ReplaceAll("Cross Spectrogram", "Spectrogram");
            htitle += "; Time (s)";
            htitle += "; Frequency (Hz)";
            htitle += "; ASD (Strain / #sqrt{Hz})";

    h->SetName((TString) hname);
    h->SetTitle((TString) htitle);

    return h;
}

TH2* OMU::TKFR::Spectrogram(ComplexPart part, TH1 *h1, TH1 *h2, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;
    if(h2 == NULL) return NULL;

    TAxis *timeAxis = h1->GetXaxis();
    
    double dt = timeAxis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? timeAxis->GetNbins() : TMath::Min(nFFT, timeAxis->GetNbins());
 
    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute CSD from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    vector<vector<double>> spectrogram = OMU::TKFR::Spectrogram(part, xn, (h1 == h2 ? xn : UIUC::TFactory::GetVectorContent(h2)), fs, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    double df = fs/nFFT; // Spectral resolution
    
    TString windowing = "";
    switch(estimator) {
        case Estimator::Periodogram: windowing = "";
            break;

        default:
            windowing = enum2str(window);
            windowing = !windowing.EqualTo("") ? "-"+windowing : "";
    }

    TH2 *h0 = new TH2D(
         h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4), "", 
         spectrogram.size(), timeAxis->GetXmin(), timeAxis->GetXmax(),
         spectrogram[0].size(), 1, spectrogram[0].size()*df
    );

    TAxis *freqAxis = NULL;
           freqAxis = h0->GetYaxis();

    h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":xspectrogram-"+enum2str(estimator)+windowing);
    h0->SetTitle(Form("Cross Spectrogram (f_{s} = %.2f Hz, nFFT = %d, d_{f} = %.2f); Time (s); Frequency (Hz)", fs, nFFT, df));

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, spectrogram);
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

TH2* OMU::TKFR::Spectrogram(EstimatorProperty property, TH1 *h1, TH1 *h2, Option_t *option, int nFFT, Estimator estimator, double noverlap, Average average, Trend detrend, WindowType window, double ab, WindowSymmetry  symmetry) 
{
    TString opt = option;
            opt.ToLower();

    if(h1 == NULL) return NULL;
    if(h2 == NULL) return NULL;

    TAxis *timeAxis = h1->GetXaxis();
    
    double dt = timeAxis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    nFFT = nFFT < 1 ? timeAxis->GetNbins() : TMath::Min(nFFT, timeAxis->GetNbins());

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute CSD from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nFFT = "+TString::Itoa(nFFT,10)+")");
    vector<double> xn  = UIUC::TFactory::GetVectorContent(h1);
    
    vector<vector<double>> spectrogram = OMU::TKFR::Spectrogram(property, xn, (h1 == h2 ? xn : UIUC::TFactory::GetVectorContent(h2)), fs, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    double df = fs/nFFT; // Spectral resolution
    
    TString windowing = "";
    switch(estimator) {
        case Estimator::Periodogram: windowing = "";
            break;

        default:
            windowing = enum2str(window);
            windowing = !windowing.EqualTo("") ? "-"+windowing : "";
    }

    TH2 *h0 = new TH2D(
         h1->GetName() + (TString) ":"+UIUC::GetRandomStr(4), "", 
         spectrogram.size(), timeAxis->GetXmin(), timeAxis->GetXmax(),
         spectrogram[0].size(), 1, spectrogram[0].size()*df
    );

    TAxis *freqAxis = NULL;
           freqAxis = h0->GetYaxis();

    h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":xspectrogram-"+enum2str(estimator)+windowing);
    h0->SetTitle(Form("Cross Spectrogram (f_{s} = %.2f Hz, nFFT = %d, d_{f} = %.2f); Time (s); Frequency (Hz)", fs, nFFT, df));

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, spectrogram);
    else { //y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }

    return h0;
}

void OMU::TKFR::SpectrogramSlice(
    int i, int K, int N, std::vector<double> &slice,
    ComplexPart part,
    const vector<OMU::Complex> xi,
    const vector<OMU::Complex> yi,
    double fs,
    Domain domain,
    Option_t *option,
    int nFFT,
    Estimator estimator,
    double noverlap,
    Average average, Trend detrend,
    WindowType window, double ab, WindowSymmetry  symmetry)
{
    slice = OMU::TKFR::CSD(part, xi, yi, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
    if(i < 0) finishedWorkers++;

    if(!UIUC::HandboxMsg::IsSuperQuiet() && K > 1) {
    
        UIUC::HandboxMsg::SetSingleCarriageReturn();
        UIUC::HandboxMsg::PrintProgressBar(Form("Computing periodograms.." + UIUC::HandboxMsg::kNoColor + " (fs = %.2fHz; nFFT = %d; t = %.2fs)", fs, nFFT, N/fs), i < 0 ? (int) finishedWorkers : i+1, K);
    }
}

vector<vector<double>> OMU::TKFR::Spectrogram(
    ComplexPart part,
    const vector<OMU::Complex> &x,
    const vector<OMU::Complex> &y,
    double fs,
    Domain domain,
    Option_t *option,
    int nFFT,
    Estimator estimator,
    double noverlap,
    Average average, Trend detrend,
    WindowType window, double ab, WindowSymmetry  symmetry)
{    
    if(noverlap >= 1 && noverlap < 0)
        throw std::invalid_argument("Unexpected number of overlap requested (0 <= noverlap < 1).");
    
    std::vector<std::vector<double>> spectrogram;
    std::vector<thread *> workers;

    int N = x.size();
    int L = nFFT;
    int D = (1.-noverlap)*L;
    int K = (N-L)/D+1;

    unsigned int numThreads = std::thread::hardware_concurrency();
    for(int i = 0; i < K; i++) {

        if(spectrogram.size() == 0) spectrogram.resize(K);
        vector<OMU::Complex> xi = OMU::Slice(x, i*D, nFFT);
        vector<OMU::Complex> yi = OMU::Slice(y, i*D, nFFT);

        if(numThreads < 2 || !ROOT::IsImplicitMTEnabled() || !OMU::TKFR::IsAlreadyRunningMT) {

            OMU::TKFR::SpectrogramSlice(i, K, N, spectrogram[i], part, xi, yi, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry); 

        } else { 

            OMU::TKFR::IsAlreadyRunningMT = false;
            finishedWorkers = 0;

            std::thread *t = new std::thread(&OMU::TKFR::SpectrogramSlice, -1, K, N, std::ref(spectrogram[i]), part, xi, yi, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
            workers.push_back(t);

            int runningWorkers = workers.size() - finishedWorkers + 1;
            while(runningWorkers >= numThreads) {
                runningWorkers = workers.size() - finishedWorkers + 1;
                gSystem->Sleep(10);
            }
        }
    }

    if(workers.size()) {

        for(int i = 0, N = workers.size(); i < N; i++)
            workers[i]->join();

        OMU::TKFR::IsAlreadyRunningMT = true;
    }

    return spectrogram;
}

vector<vector<double>> OMU::TKFR::Spectrogram(
    EstimatorProperty property,
    const vector<OMU::Complex> &x,
    const vector<OMU::Complex> &y,
    double fs,
    Domain domain,
    Option_t *option,
    int nFFT,
    Estimator estimator,
    double noverlap,
    Average average, Trend detrend,
    WindowType window, double ab, WindowSymmetry  symmetry)
{
    if(x.size() != y.size())
        throw std::invalid_argument( "vectors are not same size: x("+TString::Itoa(x.size(),10)+") != y("+TString::Itoa(y.size(),10)+")");

    std::vector<std::vector<double>> spectrogram;
    switch(property)
    {
        case EstimatorProperty::Decibel  :
            spectrogram = OMU::TKFR::Spectrogram(EstimatorProperty::Spectrum, x, y, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
            for(int i = 0, N = spectrogram.size(); i < N; i++)
                spectrogram[i] = Mag2dB(spectrogram[i]);

            return spectrogram;
    
        case EstimatorProperty::Density  : 
            return OMU::TKFR::Spectrogram(ComplexPart::Magnitude, x, y, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
        
        case EstimatorProperty::Spectrum :

            spectrogram = OMU::TKFR::Spectrogram(EstimatorProperty::Density, x, y, fs, domain, option, nFFT, estimator, noverlap, average, detrend, window, ab, symmetry);
            for(int i = 0, N = spectrogram.size(); i < N; i++)
                spectrogram[i] = OMU::Sqrt(spectrogram[i]);
 
            return spectrogram;

        case EstimatorProperty::Variance : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];

                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::Bias : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];

                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::Variability : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];
                
                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::Resolution : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];
                
                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }
            
            break;

        case EstimatorProperty::FigureOfMerit : 
            
            switch(estimator) {

                case Estimator::Periodogram:         [[fallthrough]];
                case Estimator::ModifiedPeriodogram: [[fallthrough]];
                case Estimator::LombScargle:         [[fallthrough]];
                case Estimator::BlackmanTukey:       [[fallthrough]];
                case Estimator::Bartlett:            [[fallthrough]];
                case Estimator::Welch:               [[fallthrough]];
                
                default: UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected estimator passed as argument..");
            }

            break;
    }

    return {};
}

std::vector<OMU::Complex> OMU::TKFR::Adjust(const std::vector<OMU::Complex> &x, double scaling_factor, double phase, double offset, double fs, OMU::TKFR::Domain domain, Option_t *option)
{
    std::vector<Complex> xn = OMU::Multiply(x, scaling_factor);
    if(UIUC::IsNaN(phase) && UIUC::IsNaN(offset)) return xn;

    std::vector<OMU::Complex> Xn = OMU::TKFR::FFT(xn, domain, option);
    if(!UIUC::IsNaN(phase) )  Xn = OMU::PhaseShift(Xn, phase);
    if(!UIUC::IsNaN(offset))  Xn = OMU::TimeShift (Xn, fs, offset);

    return OMU::TKFR::iFFT(Xn, domain, option);
}

std::vector<OMU::Complex> OMU::TKFR::Adjust(TH1 *h, double scaling_factor, double phase, double offset, OMU::TKFR::Domain domain, Option_t *option)
{
    TString opt = option;
            opt.ToLower();

    if(h == NULL) return {};

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h->GetXaxis();
    else if(opt.Contains("y")) axis = h->GetYaxis();
    else if(opt.Contains("z")) axis = h->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return {};
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    
    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Adjusting (scaling_factor, phase,offset)="+Form("(%.4f; %.4f; %.4f)", scaling_factor, phase, offset)+" for histogram "+h->GetName()+" with options \""+opt+"\"");

    vector<double> xn = UIUC::TFactory::GetVectorContent(h);
    return OMU::TKFR::Adjust(xn, scaling_factor, phase, offset, fs, domain, option);
}


TH1* OMU::TKFR::Adjust(ComplexPart part, TH1 *h, double scaling_factor, double phase, double offset, OMU::TKFR::Domain domain, Option_t *option)
{
    TString opt = option;
            opt.ToLower();

    if(h == NULL) return NULL;

    TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+UIUC::GetRandomStr(4));
         h0 = UIUC::TFactory::Empty(h0);

    TAxis *axis = NULL;
    if(opt.Contains("x")) axis = h0->GetXaxis();
    else if(opt.Contains("y")) axis = h0->GetYaxis();
    else if(opt.Contains("z")) axis = h0->GetZaxis();

    if(axis == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        return NULL;
    }

    double dt = axis->GetBinWidth(1); // Time resolution
    double fs = 1./dt;                // Sample frequency
    
    vector<double> xn = UIUC::TFactory::GetVectorContent(h);
    vector<OMU::Complex> x = OMU::TKFR::Adjust(xn, scaling_factor, phase, offset, fs, domain, option);
    
    h0->SetName(h->GetName() + (TString) ":"+opt+"-adjust:"+OMU::TKFR::enum2str(part));
    h0->SetTitle(Form("Corrected Time Distribution (A_{0} = %.2f, #phi = %.2f, t_{0} = %.2f)", scaling_factor, phase, offset));

    if(opt.Contains("x")) UIUC::TFactory::AddVectorContent(h0, OMU::TKFR::Get(part, x));
    else { // y,z ..

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
        delete h0;

        return NULL;
    }
    
    return h0;
}

TCanvas* OMU::TKFR::BodePlot(TString name, TString title, Option_t *opt, double fs, vector<OMU::SOS> sos)
{
    TVirtualPad *gParentPad = gPad;
    TCanvas* bodePlot = new TCanvas(name, title, 500, 1000);
             bodePlot->Divide(1,2);

    bodePlot->cd(1);
    bodePlot->cd(1)->SetLogx(1);
    bodePlot->cd(1)->SetGridx(1);
    bodePlot->cd(1)->SetGridy(1);
    
    TH1 *hMag = OMU::TKFR::MagnitudeResponse(name+"[m"+enum2str(OMU::TKFR::ComplexPart::Magnitude)+"]", "Magnitude response", fs, sos);
    if (hMag) {
        hMag = UIUC::TFactory::ScaleX(hMag, 2*TMath::Pi());
        hMag->GetXaxis()->SetRangeUser(10, hMag->GetXaxis()->GetXmax());
        hMag->GetXaxis()->SetTitle("Angular frequency (rad/s)");
        hMag->Draw(opt);
    }

    bodePlot->cd(2);
    bodePlot->cd(2)->SetLogx(1);
    bodePlot->cd(2)->SetGridx(1);
    bodePlot->cd(2)->SetGridy(1);

    TH1 *hPhase = OMU::TKFR::PhaseResponse(name+"["+enum2str(OMU::TKFR::ComplexPart::Phase)+"]", "Phase response", fs, sos);
    if (hPhase) {

        hPhase = UIUC::TFactory::ScaleX(hPhase, 2*TMath::Pi());
        hPhase->GetYaxis()->SetRangeUser(-180, 180);
        hPhase->GetXaxis()->SetRangeUser(10, hPhase->GetXaxis()->GetXmax());
        hPhase->GetXaxis()->SetTitle("Angular frequency (rad/s)");
        hPhase->Draw(opt);
    }

    gParentPad->cd();    
    return bodePlot;
}


TH1* OMU::TKFR::MagnitudeResponse(TString name, TString title, double fs, const std::vector<OMU::SOS> &sos)
{
    TH1 *h = FrequencyResponse(name, title, ComplexPart::Decibel, fs, sos, OMU::TKFR::Domain::Frequency);
         h->GetXaxis()->SetTitle("Frequency [Hz]");
         h->GetYaxis()->SetTitle("Amplitude [dB]");

    return h;
}

TH1* OMU::TKFR::PhaseResponse(TString name, TString title, double fs, const std::vector<OMU::SOS> &sos)
{
    TH1 *h = FrequencyResponse(name, title, ComplexPart::Argument, fs, sos, OMU::TKFR::Domain::Frequency);
         h->GetXaxis()->SetTitle("Frequency [Hz]");
         h->GetYaxis()->SetTitle("Phase (deg)");

    return h;
}

std::vector<OMU::Complex> OMU::TKFR::FrequencyResponse(double fs, const std::vector<OMU::SOS> &sos, OMU::TKFR::Domain domain)
{
    int nFFT = fs;
    switch(domain) {

        case Domain::Frequency: nFFT = fs/2+1;
            break;
        case Domain::ComplexFrequency: nFFT = fs;
            break;
        default:
            throw std::invalid_argument("Unexpected domain provided.");
    }  

    std::vector<OMU::Complex> y;
    for(int f = 0; f < nFFT; f++)
        y.push_back(Complex(TMath::Cos(2*TMath::Pi()*f/fs), TMath::Sin(2*TMath::Pi()*f/fs)));

    for(int k = 0, K = sos.size(); k < K; k++) {

        OMU::TF tf = OMU::tf(sos[k]);

        std::vector<double> params = {static_cast<double>(tf.first.size()), static_cast<double>(tf.second.size())};
        for(int i = 0, N = tf.first.size(); i < N; i++)
            params.push_back(tf.first[i]);
        for(int i = 0, N = tf.second.size(); i < N; i++)
            params.push_back(tf.second[i]);

        for(int f = 0, N = fs; f < (int) fs; f++) {
            TComplex z = OMU::tfuncc(&std::vector<double>({y[f].real(), y[f].imag()})[0], &params[0]);
            y[f] = OMU::Complex(z.Re(), z.Im());
        }
    }

    return y;
}

TH1* OMU::TKFR::FrequencyResponse(TString name, TString title, ComplexPart part, double fs, const std::vector<OMU::SOS> &sos, OMU::TKFR::Domain domain)
{
    int nFFT = fs;
    switch(domain) {

        case Domain::Frequency: nFFT = fs/2+1;
            break;
        case Domain::ComplexFrequency: nFFT = fs;
            break;
        default:
            throw std::invalid_argument("Unexpected domain provided.");
    }

    TH1D *hTransfer = new TH1D(name, title, nFFT, 1, nFFT);
          hTransfer->SetEntries(nFFT);
    
    return (TH1*) UIUC::TFactory::AddVectorContent((TH1*) hTransfer, OMU::TKFR::FrequencyResponse(part, fs, sos, domain));
}

OMU::SS OMU::TKFR::Analog(const OMU::SS &digital, double Ts, OMU::TKFR::FilterTransform method, double alpha)  
{
    OMU::SS analog;

    switch(method) {

        case OMU::TKFR::FilterTransform::Euler: [[fallthrough]];
        case OMU::TKFR::FilterTransform::ForwardEuler:
            return OMU::TKFR::Analog(digital, Ts, OMU::TKFR::FilterTransform::GBT, 0.0);

        case OMU::TKFR::FilterTransform::Tustin: [[fallthrough]];
        case OMU::TKFR::FilterTransform::Bilinear: 
            return OMU::TKFR::Analog(digital, Ts, OMU::TKFR::FilterTransform::GBT, 0.5);

        case OMU::TKFR::FilterTransform::Backward: [[fallthrough]];
        case OMU::TKFR::FilterTransform::BackwardEuler:
            return OMU::TKFR::Analog(digital, Ts, OMU::TKFR::FilterTransform::GBT, 1.0);

        case OMU::TKFR::FilterTransform::GBT: [[fallthrough]];
        case OMU::TKFR::FilterTransform::GeneralizedBilinear:
        {
            if(!UIUC::IsNaN(alpha)) {
                alpha = TMath::Max(1.0, TMath::Min(0.0, alpha));
            }

            throw std::invalid_argument("Digital-to-Analog conversion with `generalized bilinear transform` method is not implemented yet.");
            return analog;
        }

        default:
        case OMU::TKFR::FilterTransform::ZOH: [[fallthrough]];
        case OMU::TKFR::FilterTransform::ZeroOrderHold:
        {
            throw std::invalid_argument("Digital-to-Analog conversion with `zero order hold` method is not implemented yet.");
            return analog;
        }

        case OMU::TKFR::FilterTransform::FOH: [[fallthrough]];
        case OMU::TKFR::FilterTransform::FirstOrderHold:
        {
            throw std::invalid_argument("Digital-to-Analog conversion with `first order hold` method is not implemented yet.");
            return analog;
        }

        case OMU::TKFR::FilterTransform::Impulse: [[fallthrough]];
        case OMU::TKFR::FilterTransform::ImpulseResponse:
        {
            throw std::invalid_argument("Digital-to-Analog conversion with `impulse response` method is not implemented yet.");
            return analog;
        }
    }
}

OMU::SS OMU::TKFR::Digital(const OMU::SS &analog, double Ts, OMU::TKFR::FilterTransform method, double alpha)
{
    OMU::SS digital;

    const TMatrixD &A = std::get<0>(analog);
    const TMatrixD &B = std::get<1>(analog);
    const TMatrixD &C = std::get<2>(analog);
    const TMatrixD &D = std::get<3>(analog);

    switch(method) {

        case OMU::TKFR::FilterTransform::Euler: [[fallthrough]];
        case OMU::TKFR::FilterTransform::ForwardEuler:
            return OMU::TKFR::Digital(analog, Ts, OMU::TKFR::FilterTransform::GBT, 0.0);

        case OMU::TKFR::FilterTransform::Tustin: [[fallthrough]];
        case OMU::TKFR::FilterTransform::Bilinear: 
            return OMU::TKFR::Digital(analog, Ts, OMU::TKFR::FilterTransform::GBT, 0.5);

        case OMU::TKFR::FilterTransform::Backward: [[fallthrough]];
        case OMU::TKFR::FilterTransform::BackwardEuler:
            return OMU::TKFR::Digital(analog, Ts, OMU::TKFR::FilterTransform::GBT, 1.0);

        case OMU::TKFR::FilterTransform::GBT: [[fallthrough]];
        case OMU::TKFR::FilterTransform::GeneralizedBilinear:
        {
            if(!UIUC::IsNaN(alpha)) {
                alpha = TMath::Min(1.0, TMath::Max(0.0, alpha));
            }

            TMatrixD M = OMU::Eye<double>(A.GetNrows()) - alpha * Ts * A;
            TMatrixD Mt = M;
                     Mt.T();

            TDecompLU LU(M);
            TDecompLU LUt(Mt);

            TMatrixD AA = OMU::Eye<double>(A.GetNrows()) + (1.0-alpha) * Ts * A;
            TMatrixD BB = Ts*B;
            TMatrixD CC = C;
                     CC.T();
            
            LU.MultiSolve(AA);
            LU.MultiSolve(BB);       
            LUt.MultiSolve(CC);

            CC = CC.T();
            TMatrixD DD = D + alpha * C * BB;

            return OMU::ss(AA,BB,CC,DD);
        }

        default:
        case OMU::TKFR::FilterTransform::ZOH: [[fallthrough]];
        case OMU::TKFR::FilterTransform::ZeroOrderHold:
        {
            TMatrixD M = OMU::GetHorizontalBlockMatrix(std::get<0>(analog), std::get<1>(analog));
                     M.ResizeTo(M.GetNcols(), M.GetNcols());

            TMatrixD MM = OMU::Exp(Ts * M);
            TMatrixD AA = MM.GetSub(0, A.GetNrows()-1, 0,            A.GetNcols()-1);
            TMatrixD BB = MM.GetSub(0, B.GetNrows()-1, A.GetNcols(), A.GetNcols()+B.GetNcols()-1);
            TMatrixD CC = C;
            TMatrixD DD = D;

            return OMU::ss(AA,BB,CC,DD);
        }

        case OMU::TKFR::FilterTransform::FOH: [[fallthrough]];
        case OMU::TKFR::FilterTransform::FirstOrderHold:
        {   
            int a = A.GetNrows();
            int b = B.GetNcols();

            TMatrixD M = OMU::GetDiagonalBlockMatrix(Ts * OMU::GetBlockMatrix(A, B), OMU::Eye<double>(b));
            int m = TMath::Max(M.GetNrows(), M.GetNcols());
            M.ResizeTo(m,m);

            TMatrixD MM = OMU::Exp(M);
            TMatrixD MA = MM.GetSub(0, a-1, 0,   a-1);
            TMatrixD MB = MM.GetSub(0, a-1, a,   a+b-1);
            TMatrixD MC = MM.GetSub(0, a-1, a+b, m-1);
            
            TMatrixD AA = MA;
            TMatrixD BB = MB - MC + MA * MC;
            TMatrixD CC = C;
            TMatrixD DD = D + C * MC;
            
            return OMU::ss(AA,BB,CC,DD);
        }

        case OMU::TKFR::FilterTransform::Impulse: [[fallthrough]];
        case OMU::TKFR::FilterTransform::ImpulseResponse:
        {
            if(!UIUC::EpsilonEqualTo(D, 0.0)) 
                throw std::invalid_argument("Impulse method is only applicable to strictly proper systems");

            TMatrixD AA = OMU::Exp(Ts * A);
            TMatrixD BB = Ts * AA * B;
            TMatrixD CC = C;
            TMatrixD DD = Ts * C * B;

            return OMU::ss(AA,BB,CC,DD);
        }
    }
}
