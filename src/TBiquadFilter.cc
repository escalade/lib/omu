/**
 **********************************************
 *
 * \file TBiquadFilter.cc
 * \brief Source code of the TBiquadFilter class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TBiquadFilter.h>
ClassImp(OMU::TBiquadFilter)

OMU::TBiquadFilter::TBiquadFilter(Filter filter, double k, TQuad B, TQuad A)
: TComplexFilter(
    filter, k,
    std::vector<double>({std::get<0>(B),std::get<1>(B),std::get<2>(B)}), 
    std::vector<double>({std::get<0>(A),std::get<1>(A),std::get<2>(A)})) { }