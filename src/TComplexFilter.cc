/**
 **********************************************
 *
 * \file TComplexFilter.cc
 * \brief Source code of the TComplexFilter class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <OMU/TComplexFilter.h>
ClassImp(OMU::TComplexFilter)

#include <OMU/TKFR.h>
#include <UIUC/TFactory.h>
#include <OMU/MathUtils.h>
#include <TSpectrum.h>
#include <TPolyMarker.h>
#include <TArrow.h>

#include <TFitResultPtr.h>
#include <TFitResult.h>

#include <kfr/all.hpp>
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

using namespace std;
using namespace kfr;

OMU::TComplexFilter::TComplexFilter(Filter type, double k, std::vector<double> B, std::vector<double> A): type(type)
{
    int order = TMath::Max(B.size(), A.size());
    
    if(IsDigital()) std::reverse(B.begin(), B.end()); // Rearrange coefficients orders such that it matches polynomial (n>0)
    if(IsDigital()) std::reverse(A.begin(), A.end());
    
    B.resize(order, 0);
    for(int i = 0; i < order; i++)
        B[i] *= k;

    this->B = new ROOT::Math::Polynomial(order-1);
    this->B->SetParameters(&B[0]);

    A.resize(order, 0);
    this->A = new ROOT::Math::Polynomial(order-1);
    this->A->SetParameters(&A[0]);
}

void OMU::TComplexFilter::SetGain(double k)
{
    std::vector<double> B = std::vector<double>(this->B->Parameters(), this->B->Parameters() + this->B->NPar());

    double k0 = B[0];
    for(int i = 0, N = this->B->NPar(); i < N; i++) {
        B[i] *= k/k0;
    }

    this->B->SetParameters(&B[0]);
}

OMU::TComplexFilter *OMU::TComplexFilter::Cast(Filter type, double Ts, OMU::TKFR::FilterTransform method, double alpha)
{
        if(IsDigital()) {

                if(type == Filter::Digital) return this;

                TF tf = OMU::TKFR::Analog(this->GetTF(), Ts, method, alpha);
                return new OMU::TComplexFilter(Filter::Analog, tf.first, tf.second);

        } else {

                if(type == Filter::Analog) return this;

                TF tf = OMU::TKFR::Digital(this->GetTF(), Ts, method, alpha);
                return new OMU::TComplexFilter(Filter::Digital, tf.first, tf.second);
        }
}

std::vector<double> OMU::TComplexFilter::Response(std::vector<double> x)
{
    return OMU::TKFR::Filtering(x, this->GetSOS());
}

std::vector<std::complex<double>> OMU::TComplexFilter::Response(std::vector<std::complex<double>> x)
{
    OMU::ZPK zpk;
             zpk.z = kfrvector(this->GetZeros());
             zpk.p = kfrvector(this->GetPoles());
             zpk.k = this->GetGain();

    OMU::TF tf = OMU::tf(zpk);

    std::vector<double> params = {static_cast<double>(tf.first.size()), static_cast<double>(tf.second.size())};
    for(int i = 0, I = tf.first.size(); i < I; i++)
        params.push_back(tf.first[i]);
    for(int i = 0, I = tf.second.size(); i < I; i++)
        params.push_back(tf.second[i]);

    std::vector<std::complex<double>> H(x.size(), 0);
    for(int i = 0, N = x.size(); i < N; i++) {
        
        TComplex y = OMU::tfuncc(&std::vector<double>({x[i].real(), x[i].imag()})[0], &params[0]);
        H[i] = std::complex<double>(y.Re(), y.Im());
    }

    return H;
}

void OMU::TComplexFilter::Print(Option_t *opt) const
{
    TString x = this->IsDigital() ? "z" : "s";
    std::cout << "H(" << x << ") = B(" << x << ") / A(" << x << ")" << std::endl;
    
    std::vector<double> b = OMU::polvector(*this->B);
                        b = RemoveTrailing(b, 0.0);

    std::cout << "B(" << x << ") = ";
    for(int i = 0, N = b.size(), first = true; i < N; i++) {
        
        if( UIUC::EpsilonEqualTo(b[i],0)) continue;
        if(!UIUC::EpsilonEqualTo(b[i],1)) std::cout << b[i];
        if(i > 1) std::cout << x << "^" << i;
        else if (i > 0) std::cout << x;

        if((!UIUC::EpsilonEqualTo(b[i],0) && !UIUC::EpsilonEqualTo(b[i],1)) && i < N-1) std::cout << " + ";
        else std::cout << " ";
        first = false;
    }
    std::cout << std::endl;

    std::vector<double> a = OMU::polvector(*this->A);
                        a = RemoveTrailing(a, 0.0);

    std::cout << "B(" << x << ") = ";
    for(int i = 0, N = a.size(); i < N; i++) {

        if( UIUC::EpsilonEqualTo(a[i],0)) continue;

        if(!UIUC::EpsilonEqualTo(a[i],1)) std::cout << a[i];
        if(i > 1) std::cout << x << "^" << i;
        else if (i > 0) std::cout << x;

        if((!UIUC::EpsilonEqualTo(a[i],0) && !UIUC::EpsilonEqualTo(a[i],1)) && i < N-1) std::cout << " + ";
    }

    std::cout << std::endl;
}
